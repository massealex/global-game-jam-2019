﻿using Outerminds.UIAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ANewStartAppears : AnimatedContent
{
	static ANewStartAppears _instance;
	public static ANewStartAppears Instance { get { if (_instance == null) _instance = FindObjectOfType<ANewStartAppears>(); return _instance; } }
}
