﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightController : MonoBehaviour
{
	public Transform LightContainerTransform;
	public float CycleFastSpeed;
	public float CycleLessFastSpeed;

	public DayNightCycleState CurrentState;

	static DayNightController _instance;
	public static DayNightController Instance { get { if (_instance == null) _instance = FindObjectOfType<DayNightController>(); return _instance; } }

	void Start()
	{
		if (CurrentState == DayNightCycleState.Day)
		{
			LightContainerTransform.localEulerAngles = new Vector3(0f, 0f, 360f);
		}
		else if (CurrentState == DayNightCycleState.Night)
		{
			LightContainerTransform.localEulerAngles = new Vector3(0f, 0f, 180f);
		}
	}

	void Update()
	{
		if (CurrentState == DayNightCycleState.Day)
		{
			LightContainerTransform.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(LightContainerTransform.localEulerAngles.z, 360f, Time.deltaTime * 1.6f));
		}
		else if (CurrentState == DayNightCycleState.Night)
		{
			LightContainerTransform.localEulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(LightContainerTransform.localEulerAngles.z, 180f, Time.deltaTime * 1.6f));
		}
		else if (CurrentState == DayNightCycleState.Fast || CurrentState == DayNightCycleState.LessFast)
		{
			float speed = CurrentState == DayNightCycleState.Fast ? CycleFastSpeed : CycleLessFastSpeed;
			LightContainerTransform.localEulerAngles = new Vector3(0f, 0f, LightContainerTransform.localEulerAngles.z + Time.deltaTime * speed);
		}

		if (MusicManager.Instance != null)
		{
			bool isDay = LightContainerTransform.localEulerAngles.z < 90f || LightContainerTransform.localEulerAngles.z > 270f;
			MusicManager.Instance.DayAmbiance.IsPlaying = isDay;
			MusicManager.Instance.NightAmbiance.IsPlaying = !isDay;
		}
	}
}

public enum DayNightCycleState
{
	Day, Night, Fast, LessFast
}
