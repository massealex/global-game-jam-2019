﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceTrack : MonoBehaviour
{
	public GameObject Content;
	public Transform PlayerStartPosition;
	public Transform[] NPCStartPosition;
	public Transform[] Checkpoints;

	static RaceTrack _instance;
	public static RaceTrack Instance { get { if (_instance == null) _instance = FindObjectOfType<RaceTrack>(); return _instance; } }

	void Start()
	{
		Content.SetActive(false);
	}

	public void Show(Vector3 snailPosition)
	{
		Content.SetActive(true);

		var randomOffset = Random.insideUnitCircle.normalized;
		var spawnPosition = snailPosition + new Vector3(randomOffset.x, 0f, randomOffset.y);
		transform.position = spawnPosition;
		transform.localEulerAngles = new Vector3(0f, 250f - (spawnPosition - snailPosition).ToDegreeAngleXZ(), 0f);
	}
}
