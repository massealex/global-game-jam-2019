﻿using Outerminds.UIAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Herb : MonoBehaviour
{
	public AnimationScale AnimationScale;

	public bool IsEaten { get; set; }
	
	public bool Eat()
	{
		if (IsEaten)
			return false;

		IsEaten = true;
		GetComponent<Collider>().enabled = false;

		AnimationScale.Play(false);

		return true;
	}
}
