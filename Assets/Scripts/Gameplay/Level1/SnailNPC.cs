﻿using UnityEngine;

public class SnailNPC : MonoBehaviour
{
	public Animator Animator;
	public Transform ShellContainer;
	public Transform BoneTransform;
	public Rigidbody Rigidbody;
	public AnimationCurve SpeedVariationCurve;
	[Localized]
	public string DialogText;
	public Texture2D OverrideMaterialTexture;

	static SnailNPC[] _instances;
	public static SnailNPC[] Instances { get { if (_instances == null) _instances = FindObjectsOfType<SnailNPC>(); return _instances; } }

	bool _isWatchingPlayer;
	float _defaultYAngle;
	int _currentCheckPointIndex;

	public bool IsHeadingTowardRace => RaceTrack.Instance.Content.activeInHierarchy;
	public Transform StartRacePosition => RaceTrack.Instance.NPCStartPosition[System.Array.IndexOf(Instances, this)];
	public bool ReadyToRace { get; set; }
	public bool RaceCompleted => _currentCheckPointIndex >= RaceTrack.Instance.Checkpoints.Length;

	void Awake()
	{
		_defaultYAngle = transform.localEulerAngles.y;

		var meshRenderer = ShellContainer.GetComponentInChildren<MeshRenderer>();
		meshRenderer.material.mainTexture = OverrideMaterialTexture;
	}

	public bool IsWatchingPlayer
	{
		get
		{
			return _isWatchingPlayer;
		}
		set
		{
			if (value != _isWatchingPlayer)
			{
				_isWatchingPlayer = value;
				if (_isWatchingPlayer && !IsHeadingTowardRace)
				{
					// TODO: trigger dialog
					UISubtitle.Instance.ShowByID(DialogText);
				}
			}
		}
	}

	void Start()
	{
		ShellContainer.parent = BoneTransform;
	}

	Vector3 _randomCheckPointOffset;
	void FixedUpdate()
	{
		if (SnailCharacter.Instance.IsRaceStarted)
		{
			if (!RaceCompleted)
			{
				Transform currentCheckPoint = RaceTrack.Instance.Checkpoints[_currentCheckPointIndex];
				var targetPosition = currentCheckPoint.position + _randomCheckPointOffset;

				Vector3 direction = (targetPosition - transform.position);
				direction.y = 0f;
				var test = new System.Random(gameObject.GetInstanceID());
				var randomMultiplier = 1f + 0.55f * SpeedVariationCurve.Evaluate(((Time.time * 0.2f) + (float)test.NextDouble()) % 1f);
				Rigidbody.velocity = 7f * direction.normalized * randomMultiplier;
				transform.localEulerAngles = new Vector3(0f, 450f - direction.ToDegreeAngleXZ(), 0f);
				Rigidbody.angularVelocity = Vector3.zero;
				Animator.SetBool(SnailCharacter._walkAnimatorParam, true);

				bool reachedCheckPoint = Vector2.Distance(new Vector2(targetPosition.x, targetPosition.z), new Vector2(transform.position.x, transform.position.z)) < 0.5f;
				if (reachedCheckPoint)
				{
					_currentCheckPointIndex++;
					var temp = Random.insideUnitCircle * 2f;
					_randomCheckPointOffset = new Vector3(temp.x, 0f, temp.y);
				}
			}
			else
			{
				Rigidbody.velocity = Vector3.zero;
				IsWatchingPlayer = true;
			}
		}
		else if (IsHeadingTowardRace)
		{
			Vector3 direction = (StartRacePosition.position - transform.position);
			ReadyToRace = direction.magnitude < 0.2f;
			Animator.SetBool(SnailCharacter._walkAnimatorParam, !ReadyToRace);
			if (ReadyToRace)
			{
				Rigidbody.velocity = Vector3.zero;
				transform.localEulerAngles = new Vector3(0f, 450f - StartRacePosition.forward.ToDegreeAngleXZ(), 0f);
			}
			else
			{
				Rigidbody.velocity = 3f * direction.normalized;
				transform.localEulerAngles = new Vector3(0f, 450f - direction.ToDegreeAngleXZ(), 0f);
			}
			Rigidbody.angularVelocity = Vector3.zero;
		}
	}

	void LateUpdate()
	{
		if ((!IsHeadingTowardRace && !SnailCharacter.Instance.IsRaceStarted) || RaceCompleted)
		{
			var targetAngle = IsWatchingPlayer ?
				450f - (SnailCharacter.Instance.transform.position - transform.position).ToDegreeAngleXZ() :
				_defaultYAngle;
			transform.localEulerAngles = new Vector3(0f, Mathf.LerpAngle(transform.localEulerAngles.y, targetAngle, Time.deltaTime * 0.5f), 0f);
		}
	}
}
