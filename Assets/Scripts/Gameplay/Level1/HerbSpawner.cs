﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbSpawner : MonoBehaviour
{
	public GameObject[] HerbPrefabs;
	public float FieldDiameter;
	public float DistanceBetweenHerb;

	void Start()
	{
		var count = FieldDiameter / DistanceBetweenHerb;
		for (int i = 0; i < count; i++)
		{
			for (int j = 0; j < count; j++)
			{
				var randomOffset = Random.insideUnitCircle * DistanceBetweenHerb * 5f;
				var position = new Vector3(-FieldDiameter / 2f + i * DistanceBetweenHerb, 0f, -FieldDiameter / 2f + j * DistanceBetweenHerb) + new Vector3(randomOffset.x, 0f, randomOffset.y);

				bool isCloseToSnail = Vector3.Distance(SnailCharacter.Instance.transform.position - new Vector3(0f, 0f, 2.5f), position) < 3f;
				if (isCloseToSnail)
					continue;

				bool isCloseToNPC = false;
				foreach (var snailNPC in SnailNPC.Instances)
					if (Vector3.Distance(snailNPC.transform.position, position) < 1.1f)
						isCloseToNPC = true;

				if (isCloseToNPC)
					continue;

				var newHerb = Instantiate(HerbPrefabs[Random.Range(0, HerbPrefabs.Length)]);
				newHerb.transform.parent = transform;
				newHerb.transform.localPosition = position;

				newHerb.transform.localEulerAngles = new Vector3(0f, Random.Range(0f, 360f), 0f);
				var baseScale = newHerb.transform.localScale.x;
				baseScale *= Random.Range(0.75f, 1.5f);
				newHerb.transform.localScale = Vector3.one * baseScale;
			}
		}
	}
}
