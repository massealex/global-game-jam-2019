﻿using Outerminds.UIAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailCharacter : MonoBehaviour
{
	public Animator Animator;
	public Rigidbody Rigidbody;
	public float MoveSpeed;
	public float RaceMoveSpeed;
	public float RotationSpeed;
	public float StartScale;
	public float ScaleIncrementPercentLevel1;
	public float ScaleTargetLevel1;
	public float ScaleTargetLevel2;
	public FadeTransition FadeTransitionPrefab;
	public ParticleSystem EatParticleSystem;
	public AnimationUnit AnimationEat;
	public GameObject SnailCanPrefab;

	[Header("Shells")]
	public ShellState CurrentShellState;
	public Transform BoneTransform;
	public Transform ShellContainer;
	public GameObject ShellLevel1;
	public GameObject ShellLevel2;
	public GameObject ShellLevel3;

	bool _raceStarted;
	float _raceStartTimer;
	float _currentScale;
	float _lastTimeEat;
	bool _raceStartDialogSeen;

	public static string _walkAnimatorParam = "Walk";
	public static string _recoilAnimatorParam = "Recoil";
	public static string _idleRandomAnimatorParam = "IdleRandom";

	public bool IsRecoiling => Animator.GetBool(_recoilAnimatorParam);
	public bool InLevel1 => _currentScale < ScaleTargetLevel1;
	public bool InLevel2 => _currentScale >= ScaleTargetLevel1 && !InLevel3;
	public bool InLevel3 => _currentScale >= ScaleTargetLevel2;
	public bool IsRaceStarted => _raceStarted;

    public List<string> _firstLevelAudio = new List<string> { "MunchMunch", "RadulaWikipedia", "LivingHereSnailyLife-01", "LivingHereSnailyLife-02" };
    public List<bool>   _firstLevelAudioPlayed = new List<bool> { false, false, false, false };

    static SnailCharacter _instance;
	public static SnailCharacter Instance { get { if (_instance == null) _instance = FindObjectOfType<SnailCharacter>(); return _instance; } }

	void Start()
	{
		_currentScale = StartScale;
		transform.localScale = Vector3.one * _currentScale;

		ShellContainer.parent = BoneTransform;
	}

	void Update()
	{
		// Keep shell the same size:
		ShellContainer.parent = null;
		ShellContainer.localScale = Vector3.one * 6f;
		ShellContainer.parent = BoneTransform;

		ShellLevel1.SetActive(CurrentShellState == ShellState.Level1);
		ShellLevel2.SetActive(CurrentShellState == ShellState.Level2);
		ShellLevel3.SetActive(CurrentShellState == ShellState.Level3);

		if (Input.GetKeyDown(KeyCode.F1))
		{
			if (InLevel1)
			{
				_currentScale = ScaleTargetLevel1;
				SpawnCan();
			}
			else if (InLevel2)
			{
				_currentScale = ScaleTargetLevel2;
				SpawnRaceTrack();
			}
		}

		if (!_raceStarted && CurrentShellState == ShellState.Level3)
		{
			_raceStartTimer += Time.deltaTime;
			CameraManager.Instance.CinemachineCamRaceStart1.SetActivePriority(100);

			if (_raceStartTimer > 2f)
			{
				CameraManager.Instance.CinemachineCamRaceStart1.SetActivePriority(0);
				CameraManager.Instance.CinemachineCamRaceStart1.IsActive = false;
			}

			if (_raceStartTimer > 1.5f && !_raceStartDialogSeen)
			{
				_raceStartDialogSeen = true;
				UISubtitle.Instance.ShowByName("Es Car Go!");

				UISubtitle.Instance.ShowByName("RaceBoxUchi-01");
				UISubtitle.Instance.ShowByName("RaceBoxUchi-02");
				UISubtitle.Instance.ShowByName("RaceBoxUchi-03");
				UISubtitle.Instance.ShowByName("RaceBoxUchi-04");
				UISubtitle.Instance.ShowByName("RaceBoxUchi-05");
			}

			if (_raceStartTimer > 4f)
			{
				_raceStarted = true;
				MusicManager.Instance.RaceMusic.IsPlaying = true;
				DayNightController.Instance.CurrentState = DayNightCycleState.Fast;
			}
		}
	}

	void FixedUpdate()
	{
		if (!_raceStarted && CurrentShellState == ShellState.Level3)
		{
			Rigidbody.velocity = Vector3.zero;
			Rigidbody.angularVelocity = Vector3.zero;
			transform.localEulerAngles = new Vector3(0f, 450f - RaceTrack.Instance.PlayerStartPosition.forward.ToDegreeAngleXZ(), 0f);
			transform.position = RaceTrack.Instance.PlayerStartPosition.position;
			Animator.SetBool(_walkAnimatorParam, false);
		}
		else if (!CameraManager.Instance.IntroCamera.IsActive && !CameraManager.Instance.MainMenuCamera.IsActive)
		{
			var horizontalAxis = Input.GetAxis("Horizontal");
			var verticalAxis = Input.GetAxis("Vertical");

			float rotationY = horizontalAxis * RotationSpeed * Time.deltaTime;
			transform.localEulerAngles = new Vector3(0f, rotationY, 0f) + transform.localEulerAngles;

			var currentPosition = transform.position;
			var move = Mathf.Max(0f, verticalAxis) * transform.forward;
			float currentMoveSpeed = _currentScale * (_raceStarted ? RaceMoveSpeed : MoveSpeed) / StartScale;
			var moveVelocity = Time.deltaTime * currentMoveSpeed * move;
			Vector3 newPosition = currentPosition + moveVelocity;
			Rigidbody.MovePosition(newPosition);

			bool isMoving = !(horizontalAxis == 0f && verticalAxis == 0f);
			bool recoil = CurrentShellState == ShellState.Level2 && (!InLevel3 || Foot.Instance.Content.activeInHierarchy) && !isMoving;
			Animator.SetBool(_recoilAnimatorParam, recoil);
			Animator.SetBool(_walkAnimatorParam, isMoving);
			var targetScale = recoil ? 0.5f : 1f;
			transform.localScale = Vector3.Lerp(transform.localScale, targetScale * Vector3.one * _currentScale, Time.deltaTime * 2f);
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider == null || collider.gameObject == null)
			return;

		if (collider.gameObject.GetComponent<Herb>() != null)
		{
			bool didEat = collider.gameObject.GetComponent<Herb>().Eat();
			if (didEat)
			{
				//AnimationEat.Play();
				EatParticleSystem.Play();

				// Do not grow if level 1 is finished but did not collect level 3 shell yet
				if (!InLevel3 && (!InLevel2 || CurrentShellState == ShellState.Level2))
				{
					var wasInLevel2 = InLevel2;
					var wasInLevel3 = InLevel3;

                    if (Time.time - _lastTimeEat > 0.3f)
                    {
                       
                        _currentScale += ScaleIncrementPercentLevel1 * transform.localScale.x;
                        if (InLevel1)
                        {
                            if (_currentScale >= 6.5f && !_firstLevelAudioPlayed[0])
                            {
                                UISubtitle.Instance.ShowByName(_firstLevelAudio[0]);
                                _firstLevelAudioPlayed[0] = true;
                            }
                            else if (_currentScale >= 7 && !_firstLevelAudioPlayed[1])
                            {
                                UISubtitle.Instance.ShowByName(_firstLevelAudio[1]);
                                _firstLevelAudioPlayed[1] = true;
                            }
                            else if (_currentScale >= 9.5 && !_firstLevelAudioPlayed[2])
                            {
                                UISubtitle.Instance.ShowByName(_firstLevelAudio[2]);
                                _firstLevelAudioPlayed[2] = true;
                            }
                            else if (_currentScale >= 10 && !_firstLevelAudioPlayed[3])
                            {
                                UISubtitle.Instance.ShowByName(_firstLevelAudio[3]);
                                _firstLevelAudioPlayed[3] = true;
                            }
                        }
                    }

					_lastTimeEat = Time.time;

					if (!wasInLevel2 && InLevel2)
					{
						SpawnCan();

						UISubtitle.Instance.ShowByName("MediumUchi-01");
						UISubtitle.Instance.ShowByName("MediumUchi-02");
						UISubtitle.Instance.ShowByName("MediumUchi-03");
					}
					else if (!wasInLevel3 && InLevel3)
					{
						SpawnRaceTrack();
					}
				}
			}
		}
		else if (collider.gameObject.GetComponentInParent<SnailNPC>() != null)
		{
			var npcSnail = collider.gameObject.GetComponentInParent<SnailNPC>();
			npcSnail.IsWatchingPlayer = true;
		}
		else if (collider.gameObject.name.ToLower().Contains("RaceFinished".ToLower()))
		{
			StartCoroutine(EndRaceRoutine());
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider == null || collider.gameObject == null)
			return;

		if (collider.gameObject.GetComponentInParent<SnailNPC>() != null)
		{
			var npcSnail = collider.gameObject.GetComponentInParent<SnailNPC>();
			npcSnail.IsWatchingPlayer = false;
		}
	}

	IEnumerator EndRaceRoutine()
	{
		DayNightController.Instance.CurrentState = DayNightCycleState.Day;
		UISubtitle.Instance.ShowByName("SnailedIt");
		UISubtitle.Instance.ShowByName("FinallyHappyButNotReally-01");
		UISubtitle.Instance.ShowByName("FinallyHappyButNotReally-02");

		yield return new WaitForSeconds(17.5f);
		var fadeTransition = Instantiate(FadeTransitionPrefab);
		fadeTransition.Init(GoToFewYearsLater);
	}

	void GoToFewYearsLater()
	{
		UIYearsLater.Instance.Show();
	}

	void SpawnCan()
	{
		// Spawn snail can
		var can = Instantiate(SnailCanPrefab);
		var randomCircle = (Random.insideUnitCircle * 2.5f).normalized;
		can.transform.position = new Vector3(randomCircle.x, 10f, randomCircle.y);

		float angularVelocity = 20f;
		can.GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(-angularVelocity, angularVelocity), Random.Range(-angularVelocity, angularVelocity), Random.Range(-angularVelocity, angularVelocity));
	}

	void SpawnRaceTrack()
	{
		RaceTrack.Instance.Show(transform.position);
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision == null || collision.gameObject == null)
			return;

		if (collision.gameObject.name.ToLower().Contains("SnailCan".ToLower()))
		{
			Destroy(collision.gameObject);
			CurrentShellState = ShellState.Level2;
			DayNightController.Instance.CurrentState = DayNightCycleState.Day;
		}
		else if (collision.gameObject.name.ToLower().Contains("SnailBox".ToLower()))
		{
			Destroy(collision.gameObject);
			CurrentShellState = ShellState.Level3;
		}
	}
}

public enum ShellState
{
	Level1,
	Level2,
	Level3
}
