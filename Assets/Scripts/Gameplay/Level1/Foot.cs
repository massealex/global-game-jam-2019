﻿using Outerminds.UIAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foot : MonoBehaviour
{
	public SnailCharacter SnailCharacter;
	public Animator Animator;
	public FadeTransition FadeTransitionPrefab;
	public GameObject Content;

	float _timerStomp;
	bool SeenFootDialog1 = false;
	int SeenFootDialog2Count = 0;

	static Foot _instance;
	public static Foot Instance { get { if (_instance == null) _instance = FindObjectOfType<Foot>(); return _instance; } }

	void Update()
	{
		transform.position = SnailCharacter.transform.position;

		if (SnailCharacter.CurrentShellState == ShellState.Level2 && SnailCharacter.InLevel2)
		{
			_timerStomp += Time.deltaTime;
			if (_timerStomp > 9f)
			{
				_timerStomp = 0f;
				Animator.SetTrigger("Stomp");

				if (!SeenFootDialog1)
				{
					UISubtitle.Instance.ShowByName("WhenYouTryToGrow-01");
					SeenFootDialog1 = true;
				}
			}
		}
	}

	
	public void Damage()
	{
		if (!SnailCharacter.IsRecoiling)
		{
            SoundManager.Instance.PlaySound("SFX/Squished");
            var fadeTransition = Instantiate(FadeTransitionPrefab);
			fadeTransition.Init(OnGameReset);
		}
        else
        {
            SoundManager.Instance.PlaySound("SFX/BlockedFoot");
        }

		if (SeenFootDialog2Count == 0)
		{
			UISubtitle.Instance.ShowByName("WhenYouTryToGrow-02");
			SeenFootDialog2Count++;
		}
		else if (SeenFootDialog2Count == 1 && !SnailCharacter.IsRecoiling)
		{
			UISubtitle.Instance.ShowByName("AlreadyCanned");
			SeenFootDialog2Count++;
		}
		else if (SeenFootDialog2Count == 2 && !SnailCharacter.IsRecoiling)
		{
			UISubtitle.Instance.ShowByName("StayedSmaller");
			SeenFootDialog2Count++;
		}
		else if (SeenFootDialog2Count == 3 && !SnailCharacter.IsRecoiling)
		{
			UISubtitle.Instance.ShowByName("StillBetterWithSalt");
			SeenFootDialog2Count++;
		}
		
	}

	void OnGameReset()
	{

	}
}
