﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private bool spinning = false;
    private float spinTimer = 0f;
    private bool canRespin = true;
    private bool dead = false;

    public Vector2 force = Vector2.zero;
    public Transform sun;
    public DistanceJoint2D d2d;
    public GameObject fadePrefab;
    public PlanetGameController gameController;
    public GameObject[] constellationToggles;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Planet>())
        {
            StartCoroutine(Death());
        }
        if(collision.GetComponent<Constellation>())
        {
            StartCoroutine(Win());
        }
    }

    private IEnumerator Win()
    {
        dead = true;
        rb2d.velocity = Vector2.zero;
        force = Vector2.zero;

        UISubtitle.Instance.ShowByName("Ending-01");
        yield return new WaitForSeconds(8f);

        while (transform.position != constellationToggles[0].transform.position)
        {
            transform.position = Vector3.MoveTowards(transform.position,constellationToggles[0].transform.position,Time.deltaTime * 5f);
            yield return new WaitForEndOfFrame();
        }

        UISubtitle.Instance.ShowByName("Ending-02");
        yield return new WaitForSeconds(8f);

        for (int i=0;i<100;i++)
        {
            transform.localScale -= Vector3.one * 0.0005f;
            if (transform.localScale.x < 0f) transform.localScale = Vector3.zero;
            yield return new WaitForEndOfFrame();
        }

        UISubtitle.Instance.ShowByName("Ending-03");
        yield return new WaitForSeconds(5f);

        foreach (GameObject toggle in constellationToggles)
        {
            toggle.SetActive(true);
        }

        yield return new WaitForSeconds(3f);
        if (FindObjectOfType<FadeTransition>() == null)
        {
            GameObject fadeOut = GameObject.Instantiate(fadePrefab);
            fadeOut.GetComponent<FadeTransition>().Init(WinScene);
        }
    }

    public void WinScene()
    {
        UIMainMenu.AchievementUnlocked = true;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Alex");
    }

    private IEnumerator Death()
    {
        if (!dead)
        {
            SoundManager.Instance.PlaySound("SFX/ExplosionCrash-0" + Random.Range(1, 3), 0.1f);
            rb2d.velocity = Vector2.zero;
            force = Vector2.zero;
            d2d.enabled = false;
            dead = true;
            canRespin = true;
            spinTimer = 0f;
            for (int i = 0; i < 33; i++)
            {
                transform.localScale -= Vector3.one * 0.0016f;
                if (transform.localScale.x < 0f) transform.localScale = Vector3.zero;
                yield return new WaitForEndOfFrame();
            }

            if (FindObjectOfType<FadeTransition>() == null)
            {
                GameObject fadeOut = GameObject.Instantiate(fadePrefab);
                fadeOut.GetComponent<FadeTransition>().Init(Reset);
            }
        }
    }

    private void Reset()
    {
        dead = false;
        gameController.ResetGame();
    }

    private void Update()
    {
        if (dead) return;

        if(force != Vector2.zero)
        {
            rb2d.AddForce(force * 2f, ForceMode2D.Force);
            if (!spinning && canRespin && force.y != 0f)
            {
                spinTimer += Time.deltaTime;
                if (spinTimer > 2f)
                {
                    d2d.enabled = true;
                    spinning = true;
                    spinTimer = 0f;
                }
            }
            if (spinning)
            {
                spinTimer += Time.deltaTime;
                if(spinTimer > 2.5f)
                {
                    force = (constellationToggles[0].transform.position - transform.position) / 2f;
                    spinTimer = 0f;
                    spinning = false;
                    d2d.enabled = false;
                    canRespin = false;
                }
            }
        }
    }
}
