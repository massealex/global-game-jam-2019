﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGameController : MonoBehaviour
{
    public GameObject blimp;
    public ParticleSystem lava;
    public ParticleSystem stars;
    public Transform arrow;
    public LineRenderer line;
    public EarthController earth;

    private GameObject snail;
    private bool canAct = false;
    private float charge = 0f;
    private Vector3 earthPos;

    private IEnumerator Start()
    {
        earthPos = earth.transform.position;
        yield return new WaitForSeconds(2f);
        snail = blimp.transform.GetChild(0).gameObject;
        for (int i=0;i<30;i++)
        {
            blimp.transform.Translate(new Vector3(0f, -0.015f, 0f));
            blimp.transform.localScale *= 0.95f;
            yield return new WaitForEndOfFrame();
        }
        SoundManager.Instance.PlayPitchAudio("SFX/Squished");
        lava.Play();
        yield return new WaitForSeconds(1f);
        snail.transform.SetParent(transform);
        snail.transform.localEulerAngles = new Vector3(0f, 0f, 45f);
        snail.transform.localPosition = new Vector3(5.955f, 0.348f, 0f);
        snail.transform.localScale = Vector3.zero;
        for (int i = 0; i < 25; i++)
        {
            snail.transform.localScale = Vector3.one * i * 0.003f;
            yield return new WaitForEndOfFrame();
        }
        SoundManager.Instance.PlayPitchAudio("SFX/Squished");
        yield return new WaitForSeconds(1f);
        MusicManager.Instance.SpaceAmbiance.Play();
        Color c = Camera.main.backgroundColor;
        while (Camera.main.transform.position != new Vector3(0f, 0f, -10f))
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(0f, 0f, -10f), Time.deltaTime * 1.2f);
            Camera.main.transform.rotation = Quaternion.RotateTowards(Camera.main.transform.rotation, Quaternion.identity, Time.deltaTime * 15f);
            Camera.main.orthographicSize = Mathf.Min(10f, Camera.main.orthographicSize + Time.deltaTime * 2f);
            Camera.main.backgroundColor = Color.Lerp(c, Color.black, Camera.main.orthographicSize);
            yield return new WaitForEndOfFrame();
        }
        snail.transform.SetParent(earth.transform);
        stars.Play();
        canAct = true;
    }

    private void FixedUpdate()
    {
        if (!canAct) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        
        if (charge > 1f)
        {
            if (h != 0 || v != 0)
            {
                SoundManager.Instance.PlaySound("SFX/Swingshot");
                earth.force = new Vector3(h, v, 0f);
                canAct = false;
                ResetLine();
            }
        }
        else if(h != 0f || v != 0f)
        {
            arrow.gameObject.SetActive(true);
            arrow.right = (arrow.position-earth.transform.position);
            line.gameObject.SetActive(true);
            arrow.transform.localPosition = new Vector3(h, v, 0f) * 30f;
            line.SetPosition(0, transform.localPosition);
            line.SetPosition(1, arrow.transform.localPosition);

            charge += Time.deltaTime;
            if (charge <= 0.5f) line.SetColors(Color.Lerp(Color.white,Color.yellow,charge/2), Color.Lerp(Color.white, Color.yellow, charge/2));
            else line.SetColors(Color.Lerp(Color.yellow, Color.red, (charge-0.5f)/2), Color.Lerp(Color.yellow, Color.red, (charge- 0.5f) /2));
        }
        else
        {
            ResetLine();
        }
    }

    private void ResetLine()
    {
        charge = 0f;
        arrow.gameObject.SetActive(false);
        line.gameObject.SetActive(false);
        line.SetColors(Color.white, Color.white);
    }

    public void ResetGame()
    {
        charge = 0f;
        canAct = true;
        earth.transform.localPosition = earthPos;
        earth.transform.localScale = Vector3.one * 0.05f;
        earth.force = Vector2.zero;
        earth.transform.localEulerAngles = Vector3.zero;
    }
}
