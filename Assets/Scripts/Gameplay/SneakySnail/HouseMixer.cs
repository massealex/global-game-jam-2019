﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseMixer : MonoBehaviour
{
    public Material[] mats;

    // Start is called before the first frame update
    void Start()
    {
        int rnd = Random.Range(0, 3);
        for (int i = 0; i < 3; i++) transform.GetChild(i).gameObject.SetActive(i == rnd);
        GetComponentInChildren<Renderer>().material = mats[Random.Range(0, 3)];
        transform.localScale = new Vector3(Random.Range(1f, 2f), Random.Range(1f, 2f), 1f);

        transform.localPosition = new Vector3(-840f + 100f * transform.GetSiblingIndex(), -100f, 85f);
    }
}
