﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakyTree : MonoBehaviour
{
    public GameObject[] models;
    void Start()
    {
        int rnd = Random.Range(0, 3);
        for(int i=0;i<3;i++)
        {
            models[i].SetActive(i == rnd);
            if (i != rnd) Destroy(models[i]);
        }
    }
}
