﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlimpGame : MonoBehaviour
{
    public GameObject cloudPrefab;
    public GameObject planePrefab;
    public Transform snail;
    public Transform farTarget;
    public GameObject fadePrefab;

    private int deaths = -1;
    private float spawnTimer = 0f;
    private int cloudsDrank = 0;
    private List<GameObject> toDestroy = new List<GameObject>();

    private void Start()
    {
        Reload();
        SpawnObject(true,true);
        MusicManager.Instance.StopAllMusic();
        MusicManager.Instance.StartMusic(MusicManager.Instance.BlimpRide);
    }

    public void DrankCloud()
    {
        cloudsDrank++;
        if(cloudsDrank == 20)
        {
            SoundManager.Instance.PlaySound("SFX/FallingBlimp");
            snail.GetComponent<Blimp>().enabled = false;
            StartCoroutine(Win());
        }
    }

    public void Reload()
    {
        cloudsDrank = 0;
        foreach (GameObject td in toDestroy)
        {
            if (td != null)
            {
                Destroy(td);
            }
        }
        toDestroy = new List<GameObject>();
        deaths++;
    }

    private void Update()
    {
        if (cloudsDrank >= 20) return;

        spawnTimer += Time.deltaTime;
        if(spawnTimer > Mathf.Max(0.2f,2f - cloudsDrank * 0.1f))
        {
            spawnTimer = 0f;
            bool rnd = Random.Range(0, 100) < 50 + cloudsDrank + deaths * 3;
            SpawnObject(rnd);
            SpawnObject(true);
        }
    }

    private void SpawnObject(bool type, bool isTutorialCloudFromFinalFantasy7 = false)
    {
        GameObject pref = GameObject.Instantiate(type ? cloudPrefab : planePrefab);
        toDestroy.Add(pref);
        pref.transform.eulerAngles = new Vector3(Random.Range(35f, 50f), -90f, 0f);

        pref.transform.position = farTarget.position + new Vector3(0f, Random.Range(-300f, 300f), Random.Range(-500f, 500f));

        Destroy(pref, 10f);

        if(isTutorialCloudFromFinalFantasy7)
        {
            pref.transform.position = snail.position + snail.forward * 500f;
            StartCoroutine(NudgeCloud(pref));
        }
    }

    private IEnumerator NudgeCloud(GameObject cloud)
    {
        while(cloud != null)
        {
            cloud.transform.position = Vector3.MoveTowards(cloud.transform.position, snail.transform.position, 10f);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator Win()
    {
        for(int i=0;i<60;i++)
        {
            snail.Translate(0f, -500f * Time.deltaTime, 0f);
            yield return new WaitForEndOfFrame();
        }
        if (FindObjectOfType<FadeTransition>() == null)
        {
            GameObject fadeOut = GameObject.Instantiate(fadePrefab);
            fadeOut.GetComponent<FadeTransition>().Init(NextScene);
        }
    }

    private void NextScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("SolarSystem");
    }
}
