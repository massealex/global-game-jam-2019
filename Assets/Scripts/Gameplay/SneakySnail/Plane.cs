﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    public Material[] mats;

    private GameObject aud;

    private void Start()
    {
        SoundManager.Instance.PlayPitchAudio("SFX/Plane-01");
        aud = SoundManager.Instance.LastOneShotAudioGameObject;
        Material mat = mats[Random.Range(0, mats.Length)];
        foreach (Renderer r in GetComponentsInChildren<Renderer>()) r.material = mat;
    }

    private void OnDestroy()
    {
        Destroy(aud);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * 1000f * Time.deltaTime);
    }
}
