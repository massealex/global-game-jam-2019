﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakySnailGame : MonoBehaviour
{
    private float passerbyTimer = 0f;
    private bool win = false;
    private bool started = false;

    public static int deaths = 0;
    public GameObject pedestrian;
    public Transform snail;
    public Transform blimp;
    public Rigidbody snailHouse;
    public Blimp blimpControls;
    public BlimpGame blimpGame;
    public Transform ground;

    IEnumerator Start()
    {
        snail.GetComponent<SneakySnail>().enabled = false;
        if (deaths == 0)
        {
            UISubtitle.Instance.ShowByName("HouseUchi_01");
            yield return new WaitForSeconds(8f);
            UISubtitle.Instance.ShowByName("HouseUchi_02");
            yield return new WaitForSeconds(9.2f);
        }
        started = true;
        snail.GetComponent<SneakySnail>().enabled = true;
        Physics.gravity = new Vector3(0f,-500f);
    }

    void Update()
    {
        if (!started) return;
        if (win) return;

        passerbyTimer += Time.deltaTime;
        if (passerbyTimer > Random.Range((float)(11-transform.childCount) * 1.75f, (float)(11- transform.childCount) * 3.25f) + 0.5f * deaths)
        {
            passerbyTimer = 0f;
            SoundManager.Instance.PlaySound("SFX/HumanEntrance");
            StartCoroutine(SpawnPedestrian());
        }

        if(transform.childCount == 0)
        {
            win = true;
            StartCoroutine(WinSequence());
        }
    }

    private IEnumerator WinSequence()
    {
        snail.GetComponent<SneakySnail>().enabled = false;
        for (int i = 1; i < 4; i++)
        {
            UISubtitle.Instance.ShowByName("GettingToTheBlimp_0" + i);
            yield return new WaitForSeconds(4.5f);
        }
        while(snail.position.x > blimp.position.x + 100f)
        {
            snail.Translate(new Vector3(0f, 0f, 1f));
            yield return new WaitForEndOfFrame();
        }
        Camera.main.transform.SetParent(null);
        for(int i=0;i<30;i++)
        {
            snail.localEulerAngles += new Vector3(0f, 6f, 0f);
            yield return new WaitForEndOfFrame();
        }
        blimp.transform.SetParent(snail);
        blimp.transform.localPosition = new Vector3(0f, 0.253f, -0.261f);
        snailHouse.constraints = RigidbodyConstraints.None;
        snailHouse.AddForce(new Vector3(-20f,20f,20f), ForceMode.Impulse);
        snail.GetComponent<Animator>().enabled = false;
        for(int i=0;i<33;i++)
        {
            snail.Translate(new Vector3(0f, 2f, 0f));
            snail.eulerAngles -= new Vector3(1f, 0f, 0f);
            yield return new WaitForEndOfFrame();
        }
        float f = 0f;
        while (f < 3f)
        {
            snail.Translate(Vector3.forward * 5f);
            Camera.main.transform.LookAt(snail);
            f += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Camera.main.transform.SetParent(snail);
        Vector3 v = new Vector3(0f, 0.35f, -4.096795f);
        while(Camera.main.transform.localPosition != v)
        {
            snail.Translate(Vector3.forward * 5f);
            Camera.main.transform.localPosition = Vector3.MoveTowards(Camera.main.transform.localPosition, v, Time.deltaTime * 3f);
            Camera.main.fieldOfView = Mathf.Max(Camera.main.fieldOfView - 1f, 40f);
            Camera.main.transform.localRotation = Quaternion.RotateTowards(Camera.main.transform.localRotation, Quaternion.identity, Time.deltaTime * 50f);
            //ground.Translate(new Vector3(0f, -5f, 0f));
            yield return new WaitForEndOfFrame();
        }
		Destroy(ground.gameObject);

        Camera.main.fieldOfView = 40f;
        Camera.main.farClipPlane = 10000;
        blimpControls.enabled = true;
        blimpGame.enabled = true;
        Camera.main.transform.SetParent(null);
    }

    private IEnumerator SpawnPedestrian()
    {
        GameObject newPedestrian = GameObject.Instantiate(pedestrian);
        newPedestrian.transform.position = new Vector3(snail.transform.position.x - 250f, 34f, -6f);
        newPedestrian.GetComponent<SpriteRenderer>().flipX = true;
        newPedestrian.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0.9f, 1f), Random.Range(0.9f, 1f), Random.Range(0.9f, 1f));
        float speed = Random.Range(0.5f, 0.9f);
        do
        {
            if (newPedestrian.transform.GetChild(0).gameObject.activeInHierarchy == false)
            {
                newPedestrian.transform.Translate(new Vector3(speed * Time.deltaTime * 33f, 0f, 0f));
            }
            if (Mathf.Abs(newPedestrian.transform.position.x) > 1500f) Destroy(newPedestrian);
            if (newPedestrian.GetComponent<SneakySnailPedestrian>() == null) speed *= 1.025f;
            yield return new WaitForEndOfFrame();
        }
        while (newPedestrian != null && !win);
    }
}
