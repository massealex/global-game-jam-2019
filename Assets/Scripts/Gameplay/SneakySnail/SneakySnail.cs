﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakySnail : MonoBehaviour
{
    private int snailState = 0; //in shell, transition, moving, eating
    private float scaleSize = 1f;
    private List<GameObject> charactersInSnail = new List<GameObject>();
    private bool dead = false;
    private static bool[] treeAudio = new bool[2] { true, true };
    private static int seenAudioIndex;
    private static List<string> seenAudio = new List<string>() { "TheWorldWasNotSoFondOfSeeingHim", "PeopleNowFearedHim", "NeverWishToBeFeared", "SnailSnailSNAIL", "PoorUchi"};
    private int ateTrees = 0;

    public GameObject snailModel;
    public Rigidbody snailShell;
    public GameObject fadePrefab;
    public List<AudioClip> slime;

    //Fixed update is called once per fixed frame
    private void FixedUpdate()
    {
        if(dead && GetComponent<AudioSource>().volume > 0f ) GetComponent<AudioSource>().volume = 0f;

        if (dead) return;
        float h = Input.GetAxis("Horizontal");

        GetComponent<AudioSource>().volume = (h == 0) ? 0f : 1f;

        if (h < 0f)
        {
            if(snailState == 0)
            {
                if (charactersInSnail.Count > 0)
                {
                    foreach (GameObject character in charactersInSnail)
                    {
                        StartCoroutine(SurprisedPedestrian(character.transform));
                    }
                    charactersInSnail.Clear();
                    StartCoroutine(SnailToggle(false));
                }
                else
                {
                    StartCoroutine(SnailToggle(true));
                }
            }
            if (snailState == 2)
            {
                transform.Translate(0f,0f,-h * Time.deltaTime * 33f);
            }
        }
        else
        {
            if(snailState == 2)
            {
                StartCoroutine(SnailToggle(false));
            }
        }

        if(GetComponent<AudioSource>().isPlaying == false)
        {
            GetComponent<AudioSource>().clip = slime[Random.Range(0, slime.Count)];
            GetComponent<AudioSource>().Play();
        }
    }

    private IEnumerator SnailToggle(bool snailVisible)
    {
        snailState = 1;
        //anim instead
        for(int i=0;i<30;i++)
        {
            snailModel.transform.localScale += Vector3.one * (scaleSize / 30f) * (snailVisible ? 1f : -1f);
            if (snailModel.transform.localScale.x < 0f) snailModel.transform.localScale = Vector3.zero;
            if (snailVisible) snailShell.MovePosition(snailShell.position + new Vector3(0f, 0.1f, 0f));
            yield return new WaitForEndOfFrame();
        }

        snailState = snailVisible ? 2 : 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<SneakyTree>())
        {
            Destroy(other.GetComponent<SneakyTree>());
            StartCoroutine(EatTree(other.gameObject));
        }
        if(other.GetComponent<SneakySnailPedestrian>())
        {
            Destroy(other.GetComponent<SneakySnailPedestrian>());
            if (snailState > 1)
            {
                StartCoroutine(SurprisedPedestrian(other.transform));
                snailModel.transform.localScale = Vector3.zero;
            }
            else
            {
                charactersInSnail.Add(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(charactersInSnail.Contains(other.gameObject))
        {
            charactersInSnail.Remove(other.gameObject);
        }
    }

    private IEnumerator EatTree(GameObject tree)
    {
        int rnd = Random.Range(1, 4);
        SoundManager.Instance.PlaySound("SFX/LongEating-0" + rnd);
        yield return new WaitForEndOfFrame();
        GameObject go = SoundManager.Instance.LastOneShotAudioGameObject;
        ateTrees++;
        snailState = 3;
        tree.GetComponentInChildren<ParticleSystem>().Play();
        for(int i=0;i<25;i++)
        {
            tree.transform.GetChild(0).GetChild(0).localScale -= new Vector3(4f,4f,4f);
            scaleSize += 0.005f;
            snailModel.transform.localScale = Vector3.one * scaleSize;
            Camera.main.fieldOfView += 0.1f;
            yield return new WaitForEndOfFrame();
        }

        if (treeAudio[0] && Random.Range(0, 3) < ateTrees)
        {
            treeAudio[0] = false;
            UISubtitle.Instance.ShowByName("UchiEnjoyedNewGrass");
        }
        if(treeAudio[1] && Random.Range(3,6) < ateTrees)
        {
            treeAudio[1] = false;
            UISubtitle.Instance.ShowByName("TheyWereALotCrunchier");
        }
        tree.transform.SetParent(null);
        snailState = 2;

        yield return new WaitForSeconds(1f);
        Destroy(go);
    }

    private IEnumerator SurprisedPedestrian(Transform pedestrian)
    {
        dead = true;
        SoundManager.Instance.PlaySound("SFX/SurprisedGuy-0" + Random.Range(1, 6));
        pedestrian.GetComponent<Animator>().SetBool("Scared", true);
        pedestrian.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        pedestrian.GetChild(0).gameObject.SetActive(false);
        pedestrian.GetComponent<Animator>().SetBool("Scared", false);
        if(seenAudio.Count > 0)
        {
            seenAudioIndex = Mathf.Clamp(seenAudioIndex, 0, seenAudio.Count-1);
            string aud = seenAudio[seenAudioIndex];
            seenAudioIndex++;
            //seenAudio.Remove(aud);
            UISubtitle.Instance.ShowByName(aud);
            yield return new WaitForSeconds(Resources.Load<AudioClip>("HouseUchi/"+aud).length);
        }
        if(FindObjectOfType<FadeTransition>() == null)
        {
            GameObject fadeOut = GameObject.Instantiate(fadePrefab);
            fadeOut.GetComponent<FadeTransition>().Init(Reload);
        }
    }

    private void Reload()
    {
        SneakySnailGame.deaths++;
        UnityEngine.SceneManagement.SceneManager.LoadScene("SneakySnailAndBlimp");
    }
}
