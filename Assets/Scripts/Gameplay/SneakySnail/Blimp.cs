﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blimp : MonoBehaviour
{
    private float tH = 0f;
    private float tV = 0f;
    private int weight = 0;
    private bool dead = false;
    private Vector3 snailSize;
    private Vector3 snailPosition;
    private static int maxWeight = 0;
    private static int flavorText = 0;
    private static int deaths = 0;
    private float flavorTimer = 0f;
    private float barrelRollTimer = 0f;
    private bool canBarrelRoll = true;

    public BlimpGame game;
    public Transform snailModel;
    public GameObject fadePrefab;
    public Collider blimpCollider;
    public Transform blimpModel;
    public GameObject cameraClouds;

    private void Start()
    {
        snailSize = snailModel.localScale;
        snailPosition = transform.position;
        blimpCollider.enabled = true;
        cameraClouds.SetActive(true);
    }

    private void FixedUpdate()
    {
        if (dead)
        {
            transform.Translate(0f, -7.5f, 0f);
            return;
        }

        if (canBarrelRoll)
        {
            if (Input.GetKeyUp(KeyCode.Z) || Input.GetKeyUp(KeyCode.R))
            {
                if (barrelRollTimer == 0f)
                {
                    barrelRollTimer = Time.fixedTime;
                }
                else
                {
                    if (Time.fixedTime - barrelRollTimer < 0.25f)
                    {
                        StartCoroutine(BarrelRoll());
                    }
                    barrelRollTimer = Time.fixedTime;
                }
            }
        }

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        v -= weight * 0.03f;

        if (Mathf.Abs(tH + h) > 75f) { h = 0f; } else { tH += h; }
        if (Mathf.Abs(tV + v) > 50f) { v = 0f; } else { tV += v; }

        transform.Translate(h * 220f * Time.deltaTime, v * 220f * Time.deltaTime, 0f);
        flavorTimer += Time.deltaTime;

        if(flavorTimer > 15f)
        {
            flavorTimer = 0f;
            if (flavorText == 0) UISubtitle.Instance.ShowByName("HeBelieveHeCouldFly");
            if (flavorText == 1) UISubtitle.Instance.ShowByName("TouchSky");
            flavorText++;
        }
    }

    private IEnumerator BarrelRoll()
    {
        UISubtitle.Instance.ShowByName("DoABarrelRoll");
        int dir = Random.Range(0, 100) < 50 ? -1 : 1;
        canBarrelRoll = false;
        for(int i=0;i<90;i++)
        {
            blimpModel.localEulerAngles += new Vector3(0f, 0f, 20f * dir);
            yield return new WaitForEndOfFrame();
        }
        canBarrelRoll = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Cloud>())
        {
            Destroy(other.GetComponent<Cloud>());
            StartCoroutine(DrinkCloud(other.gameObject));
        }
        if (other.GetComponent<Plane>())
        {
            Destroy(other.GetComponent<Plane>());
            StartCoroutine(HitPlane(other.gameObject));
        }
    }

    private IEnumerator DrinkCloud(GameObject cloud)
    {
        SoundManager.Instance.PlaySound("SFX/Slurp-0" + Random.Range(1, 4));
        for (int i = 0; i < 25; i++)
        {
            cloud.transform.localScale -= new Vector3(1f, 1f, 1f);
            snailModel.localScale += Vector3.one * 0.005f;
            yield return new WaitForEndOfFrame();
        }
        weight++;
        game.DrankCloud();
        Destroy(cloud);

        maxWeight = Mathf.Max(weight, maxWeight);

        flavorTimer /= 2f;

        if (maxWeight == 1) UISubtitle.Instance.ShowByName("CloudsDense");
        if (maxWeight == 4) UISubtitle.Instance.ShowByName("CondensationThirst");
        if (maxWeight == 8) UISubtitle.Instance.ShowByName("StayingHydrated");
        if (maxWeight == 12) UISubtitle.Instance.ShowByName("ActuallyBloated");
        if (maxWeight == 16) UISubtitle.Instance.ShowByName("TooHeavyEven");
    }

    private IEnumerator HitPlane(GameObject plane)
    {
        SoundManager.Instance.PlaySound("SFX/ExplosionCrash-0" + Random.Range(1, 3));
        flavorTimer = 0f;
        dead = true;
        plane.GetComponent<Rigidbody>().isKinematic = false;
        plane.GetComponent<Rigidbody>().AddForce(Random.insideUnitSphere * 5000f, ForceMode.Impulse);
        deaths++;
        if (deaths == 1) UISubtitle.Instance.ShowByName("GiantMetalBirds");
        if (deaths == 3) UISubtitle.Instance.ShowByName("NotUnderstandAirController");
        if (deaths == 5) UISubtitle.Instance.ShowByName("NoSurvivors");
        yield return new WaitForSeconds(4f);
        if (FindObjectOfType<FadeTransition>() == null)
        {
            GameObject fadeOut = GameObject.Instantiate(fadePrefab);
            fadeOut.GetComponent<FadeTransition>().Init(Reload);
        }
    }

    private void Reload()
    {
        dead = false;
        snailModel.localScale = snailSize;
        transform.position = snailPosition;
        game.Reload();
        weight = 0;
        tH = 0f;
        tV = 0f;
    }
}
