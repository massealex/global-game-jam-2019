﻿using UnityEngine;

public class Settings : MonoBehaviour
{
	public static float Sound
	{
		get { return PlayerPrefs.GetFloat("Sound", 1f); }
		set { PlayerPrefs.SetFloat("Sound", value); }
	}

	public static float Music
	{
		get { return PlayerPrefs.GetFloat("Music", 1f); }
		set { PlayerPrefs.SetFloat("Music", value); }
	}

	public static string Language
	{
		get { return PlayerPrefs.GetString("Language", ""); }
		set { PlayerPrefs.SetString("Language", value); }
	}

	public static float[] GameSpeedArray = new float[] { 1f, 2f, 3f };
	public static int GameSpeed
	{
		get { return PlayerPrefs.GetInt("GameSpeed", 0); }
		set { PlayerPrefs.SetInt("GameSpeed", value); }
	}
}
