﻿using System.Collections;
using UnityEngine;

public class UIMainMenu : UIWindow
{
	[Header("References")]
	[Localized]
	public string IntroSubtitle;

	public static bool AchievementUnlocked { get; set; } = false;

	static UIMainMenu _instance;
	public static UIMainMenu Instance { get { if (_instance == null) _instance = FindObjectOfType<UIMainMenu>(); return _instance; } }

	void Awake()
	{
		// Init game
		{
			if (!string.IsNullOrEmpty(Settings.Language))
				LocalizationManager.Instance.SetCurrentLanguage(Settings.Language);
			Time.timeScale = Settings.GameSpeedArray[Settings.GameSpeed];

			DayNightController.Instance.CurrentState = DayNightCycleState.Night;
		}
	}

	void Start()
	{
		StartCoroutine(ShowAfterDelay());
	}

	IEnumerator ShowAfterDelay()
	{
		yield return null;

		if (AchievementUnlocked)
		{
			AchievementUnlocked = false;

			yield return new WaitForSeconds(2f);
			CameraManager.Instance.EndingCamera.SetActivePriority(20f);

			yield return new WaitForSeconds(3f);
			ANewStartAppears.Instance.Show();

			yield return new WaitForSeconds(3f);
			UICredits.Instance.Show();
		}
		else
		{
			Show();
		}
	}

	protected override void OnShow()
	{
		base.OnShow();

		CameraManager.Instance.MainMenuCamera.IsActive = true;
	}

	protected override void OnHide()
	{
		base.OnHide();

		CameraManager.Instance.MainMenuCamera.IsActive = false;
	}

	void Update()
	{
		MusicManager.Instance.MenuMusic.IsPlaying = IsVisible || UICredits.Instance.IsVisible;

		if (!IsVisible)
			return;

		if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) && !Input.GetMouseButtonDown(0) && !Input.GetKeyDown(KeyCode.F12))
		{
			IsVisible = false;
			StartCoroutine(Intro());
		}

		if (Input.GetKeyDown(KeyCode.F12))
		{
			UICredits.Instance.Show();
		}
	}

	IEnumerator Intro()
	{
		if (!Input.GetKeyDown(KeyCode.F11))
		{
			yield return null;

			CameraManager.Instance.IntroCamera.IsActive = true;
			yield return new WaitForSeconds(2f);

			UISubtitle.Instance.ShowByName("LookingAtTheNightSky");
			UISubtitle.Instance.ShowByName("FeelSluggish");
			UISubtitle.Instance.ShowByName("ComingOutOfYourShell");
			UISubtitle.Instance.ShowByName("JustHungry-01");
			UISubtitle.Instance.ShowByName("JustHungry-02");

			yield return new WaitForSeconds(9f);

		}
		CameraManager.Instance.IntroCamera.IsActive = false;

		//UISubtitle.Instance.ShowByID(IntroSubtitle);
		DayNightController.Instance.CurrentState = DayNightCycleState.LessFast;
		CameraManager.Instance.Level1Camera.IsActive = true;
	}

	public override void Back()
	{
		base.Back();

		UISettings.Instance.Show();
	}
}
