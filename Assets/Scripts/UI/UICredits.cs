﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICredits : UIWindow
{
	static UICredits _instance;
	public static UICredits Instance { get { if (_instance == null) _instance = FindObjectOfType<UICredits>(); return _instance; } }

	protected override void OnShow()
	{
		base.OnShow();

		if (UIMainMenu.Instance.IsVisible)
			UIMainMenu.Instance.Hide();
	}

	public override void Back()
	{
		base.Back();

		Hide();
	}

	protected override void OnHide()
	{
		base.OnHide();

		CameraManager.Instance.EndingCamera.IsActive = false;
		UIMainMenu.Instance.Show();
	}
}
