﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIYearsLater : UIWindow
{
	static UIYearsLater _instance;
	public static UIYearsLater Instance { get { if (_instance == null) _instance = FindObjectOfType<UIYearsLater>(); return _instance; } }

	protected override void OnShow()
	{
		base.OnShow();

		StartCoroutine(Timer());
	}

	IEnumerator Timer()
	{
		yield return new WaitForSeconds(0.25f);

		SoundManager.Instance.PlaySound("RaceBoxUchi/AFewYearsLaterSpongebobImitation");

		yield return new WaitForSeconds(3.5f);

		var fadeTransition = Instantiate(SnailCharacter.Instance.FadeTransitionPrefab);
		fadeTransition.Init(GoToJeremie);
	}

	void GoToJeremie()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("SneakySnailAndBlimp");
	}
}
