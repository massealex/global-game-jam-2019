﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISettings : UIWindow
{
	[Header("References")]
	public TMPro.TextMeshProUGUI LabelLanguage;
	public TMPro.TextMeshProUGUI LabelSoundValue;
	public TMPro.TextMeshProUGUI LabelMusicValue;
	public TMPro.TextMeshProUGUI LabelSpeedValue;

	[Localized]
	public string TextIDOn;
	[Localized]
	public string TextIDOff;

	static UISettings _instance;
	public static UISettings Instance { get { if (_instance == null) _instance = FindObjectOfType<UISettings>(); return _instance; } }

	protected override void OnShow()
	{
		base.OnShow();

		RefreshValues();
	}

	void RefreshValues()
	{
		LabelLanguage.text = LocalizationManager.Instance.GetLanguageData(LocalizationManager.Instance.CurrentLanguage).NativeName;
		LabelSoundValue.text = LocalizationManager.Instance.GetString(Settings.Sound == 1f ? TextIDOn : TextIDOff);
		LabelMusicValue.text = LocalizationManager.Instance.GetString(Settings.Music == 1f ? TextIDOn : TextIDOff);
		LabelSpeedValue.text = "x" + Settings.GameSpeedArray[Settings.GameSpeed];
	}

	public void ToggleCurrentLanguage()
	{
		// todo set language   LocalizationManager.Instance.CurrentLanguage
		if (Settings.Language == "English")
			Settings.Language = "French";
		else
			Settings.Language = "English";
		LocalizationManager.Instance.SetCurrentLanguage(Settings.Language);

		RefreshValues();
	}

	public void ToggleSound()
	{
		Settings.Sound = Settings.Sound == 1f ? 0f : 1f;
		RefreshValues();
	}

	public void ToggleMusic()
	{
		Settings.Music = Settings.Music == 1f ? 0f : 1f;
		RefreshValues();
	}

	public void ToggleGameSpeed()
	{
		Settings.GameSpeed = (Settings.GameSpeed + 1) % Settings.GameSpeedArray.Length;
		Time.timeScale = Settings.GameSpeedArray[Settings.GameSpeed];
		RefreshValues();
	}

	public override void Back()
	{
		base.Back();

		Hide();
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
