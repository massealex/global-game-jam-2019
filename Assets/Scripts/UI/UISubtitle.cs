﻿using Outerminds.UIAnimations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISubtitle : UIWindow
{
	[Header("References")]
	public AnimationTextAppear TextAppearAnimation;
	public LocalizedText LabelDialog;
	public Transform SpeechBubbleContainer;
	public TMPro.TextMeshProUGUI LabelText;
	public Color DialogColor;
	public Color SubtitleColor;

	List<SubtitleItem> _subtitleQueue = new List<SubtitleItem>();
	float _currentItemTimer;
	float _pauseTimer = 0f;

	public bool IsPlaying => _currentItemTimer > 0f || TextAppearAnimation.IsPlaying();

	static UISubtitle _instance;
	public static UISubtitle Instance { get { if (_instance == null) _instance = FindObjectOfType<UISubtitle>(); return _instance; } }

	public void Show(string category, string name, float volume = 1f)
	{
		ShowByName(LocalizationManager.Instance.GetTextIDByCategoryAndName(category, name), volume);
	}

	public void ShowByName(string name, float volume = 1f)
	{
		var textItem = LocalizationManager.Instance.GetTextItemByName(name);
		Show(textItem);
	}

	public void ShowByID(string textID, float volume = 1f)
	{
		var textItem = LocalizationManager.Instance.GetTextItemByID(textID);
		Show(textItem);
	}

	void Show(LocalizationItem textItem, float volume = 1f)
	{
		if (textItem != null)
		{
			_subtitleQueue.Add(new SubtitleItem() { LocalizationItem = textItem, Volume = volume });
		}
	}

	void Update()
	{
		if (_currentItemTimer > 0f)
			_currentItemTimer -= Time.unscaledDeltaTime;

		if (!IsPlaying)
		{
			_pauseTimer += Time.unscaledDeltaTime;

			if (_subtitleQueue.Count > 0 && _pauseTimer >= 1f)
			{
				var subtitleItem = _subtitleQueue[0];
				_subtitleQueue.RemoveAt(0);

				LabelDialog.TextID = subtitleItem.LocalizationItem.ID;
				Show();

				var textItem = subtitleItem.LocalizationItem;
				bool isDialog = textItem.Name == "EatGrassTilDeath" || textItem.Name == "FeelSluggish" || textItem.Name == "MunchMunch" || textItem.Name == "DoABarrelRoll" ||
					textItem.Name == "LookingAtTheNightSky" || textItem.Name == "FeelSluggish" || textItem.Name == "ComingOutOfYourShell" || textItem.Name == "Meow" || textItem.Name == "SnailMail";
				LabelText.color = isDialog ? DialogColor : SubtitleColor;
				SpeechBubbleContainer.gameObject.SetActive(isDialog);

				//TextAppearAnimation.ResetToDefaultState();
				TextAppearAnimation.Duration = 0.05f * LocalizationManager.Instance.GetString(textItem.ID).Length;
				TextAppearAnimation.Play();
				SoundManager.Instance.PlaySound(subtitleItem.LocalizationItem.Category + "/" + subtitleItem.LocalizationItem.Name, subtitleItem.Volume);
				var duration = Mathf.Max(SoundManager.Instance.LastOneShotAudio.clip.length + 1f, 3f);
				_currentItemTimer = duration;

				_pauseTimer = 0f;
			}
			else
			{
				Hide();
			}
		}
	}
}

public struct SubtitleItem
{
	public LocalizationItem LocalizationItem;
	public float Volume;
}
