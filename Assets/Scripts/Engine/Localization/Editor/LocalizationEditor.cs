﻿using System.IO;
using UnityEditor;
using UnityEngine;

public static class LocalizationEditor
{
	public static string CategoryDefault = "General";
	public static string CategoryFilterAll = "All";
	public static string ResourcesFolder = "Resources";
	public static string FileFormat = ".txt";
	public static string DefaultNewTextItemName = "NewText";
	public static string SearchNotFilledTag = "empty:";

	static TextAsset _textAsset;
	public static TextAsset TextAsset
	{
		get
		{
			if (_textAsset == null)
				_textAsset = string.IsNullOrEmpty(LocalizationManager.DataPath) ? null : (TextAsset)Resources.Load(LocalizationManager.DataPath, typeof(TextAsset));

			return _textAsset;
		}
		set { _textAsset = value; }
	}

	public static void Save()
	{
		if (TextAsset != null)
		{
			string guid;
			long file;
			if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(TextAsset, out guid, out file))
			{
				string json = JsonUtility.ToJson(LocalizationManager.Instance.Data, true);
				File.WriteAllText(Application.dataPath.Replace("Assets", "") + AssetDatabase.GUIDToAssetPath(guid), json);
				AssetDatabase.Refresh();

				//Debug.Log("Saved! " + guid);
			}
		}
	}

	public static LocalizationItem CreateNewItem(string categoryFilter = "")
	{
		int count = 0;
		foreach (var localizationItem in LocalizationManager.Instance.Data.Items)
			if (localizationItem.Name.StartsWith(DefaultNewTextItemName))
				count++;

		var item = new LocalizationItem();
		item.ID = System.Guid.NewGuid().ToString();
		item.Category = string.IsNullOrEmpty(categoryFilter) || categoryFilter == CategoryFilterAll ? CategoryDefault : categoryFilter;
		item.Name = DefaultNewTextItemName + (count > 0 ? count.ToString() : "");
		RefreshTextItemLanguage(item);
		LocalizationManager.Instance.Data.Items.Add(item);

		return item;
	}

	public static void RefreshTextItemLanguages()
	{
		foreach (var textItem in LocalizationManager.Instance.Data.Items)
			RefreshTextItemLanguage(textItem);
	}

	public static void RefreshTextItemLanguage(LocalizationItem textItem)
	{
		LocalizationKeyValue[] existingItems = textItem.Translations;
		LocalizationKeyValue[] newItems = new LocalizationKeyValue[LocalizationManager.Instance.Data.SupportedLanguages.Count];
		for (int i = 0; i < LocalizationManager.Instance.Data.SupportedLanguages.Count; i++)
		{
			string languageName = LocalizationManager.Instance.Data.SupportedLanguages[i].Name;
			var newItem = new LocalizationKeyValue() { Language = languageName, Text = "" };

			if (existingItems != null)
			{
				foreach (var existingItem in existingItems)
				{
					if (existingItem.Language == languageName)
					{
						newItem.Text = existingItem.Text;
						break;
					}
				}
			}

			newItems[i] = newItem;
		}

		textItem.Translations = newItems;
	}

	public static void SaveDataPath()
	{
		// Auto generate code to store the localization data file path in a static property in LocalizationManager.cs
		string[] guids = AssetDatabase.FindAssets(typeof(LocalizationManager).ToString(), null);
		if (guids.Length > 0)
		{
			string resourcePath = "";

			string guid;
			long file;
			if (TextAsset != null && AssetDatabase.TryGetGUIDAndLocalFileIdentifier(TextAsset, out guid, out file))
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(guid);

				if (assetPath.IndexOf(ResourcesFolder) > -1 && assetPath.IndexOf(FileFormat) > -1)
				{
					resourcePath = assetPath.Substring(assetPath.IndexOf(ResourcesFolder) + ResourcesFolder.Length + 1);
					resourcePath = resourcePath.Substring(0, resourcePath.IndexOf(FileFormat));
				}
			}

			if (resourcePath != LocalizationManager.DataPath)
			{
				var localizationManagerPath = AssetDatabase.GUIDToAssetPath(guids[0]);
				var textAsset = AssetDatabase.LoadAssetAtPath(localizationManagerPath, typeof(TextAsset)) as TextAsset;
				string startString = "DataPath => ";
				int startIndex = textAsset.text.IndexOf(startString) + startString.Length;
				int endIndex = startIndex + textAsset.text.Substring(startIndex).IndexOf(";");

				string newContent = textAsset.text.Substring(0, startIndex) + "\"" + resourcePath + "\"" + textAsset.text.Substring(endIndex);
				File.WriteAllText(Application.dataPath.Replace("Assets", "") + localizationManagerPath, newContent);
				AssetDatabase.Refresh();
			}
		}
	}

	public static float CalculateFillRate(string language)
	{
		int filledCount = 0;
		int notFilledCount = 0;
		foreach (var item in LocalizationManager.Instance.Data.Items)
		{
			foreach (var translationItem in item.Translations)
			{
				if (translationItem.Language == language)
				{
					if (string.IsNullOrEmpty(translationItem.Text))
						notFilledCount++;
					else
						filledCount++;
					break;
				}
			}
		}

		if (filledCount + notFilledCount == 0)
			return 0;

		return (float)filledCount / (filledCount + notFilledCount);
	}
}
