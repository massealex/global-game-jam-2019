﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class SelectLanguageWindow : EditorWindow
{
	Vector2 _scrollPos;
	Color _transparentColor = new Color(1f, 1f, 1f, 0f);
	List<LanguageData> _languages = new List<LanguageData>();
	Dictionary<string, bool> _alreadySupportedLanguageData;
	GUIStyle _disabledTextStyle;

	public LocalizationEditorWindow LocalizationEditorWindow { get; set; }

	void OnEnable()
	{
		_disabledTextStyle = new GUIStyle(GUI.skin.label) { fontStyle = FontStyle.Italic };

		_alreadySupportedLanguageData = new Dictionary<string, bool>();
		foreach (var languageData in LocalizationManager.Instance.AllLanguageData)
		{
			_languages.Add(languageData);
			_alreadySupportedLanguageData.Add(languageData.Name, LocalizationManager.Instance.IsLanguageSupported(languageData.Name));
		}

		_languages = _languages.OrderBy(o => o.Name).ToList();
	}

	void OnGUI()
	{
		_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);

		Color defaultBackgroundColor = GUI.backgroundColor;
		for (int i = 0; i < _languages.Count; i++)
		{
			var languageData = _languages[i];

			bool isAlready = _alreadySupportedLanguageData[languageData.Name];
			EditorGUILayout.LabelField((isAlready ? "✔ " : "") +
				languageData.LongDisplayName,
				isAlready ? _disabledTextStyle : EditorStyles.label);

			Rect lastRect = GUILayoutUtility.GetLastRect();
			GUI.backgroundColor = _transparentColor;
			if (GUI.Button(lastRect, ""))
			{
				Close();
				LocalizationEditorWindow.AddLanguage(languageData.Name);
			}
		}
		GUI.backgroundColor = defaultBackgroundColor;

		EditorGUILayout.EndScrollView();
	}
}
