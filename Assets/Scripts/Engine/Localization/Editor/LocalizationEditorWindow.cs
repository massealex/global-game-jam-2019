﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LocalizationEditorWindow : EditorWindow
{
	GUIStyle _foldoutStyle;
	GUIStyle _titleLabelStyle;
	GUIStyle _titleButtonStyle;
	GUIStyle _lastAlignRight;
	Dictionary<string, bool> _showItemDetails = new Dictionary<string, bool>();
	bool _foldoutFile;
	bool _foldoutLanguages;
	bool _foldoutAllItems;
	string[] _categories;
	string _categoryFilter;
	ReorderableList _languageReorderableList;
	Vector2 _scrollPos;
	int _displayedItemCount;
	string _searchTerms = "";
	List<LocalizationItem> _itemsToDelete = new List<LocalizationItem>();

	void OnGUI()
	{
		DrawFile();
		DrawLanguages();

		if (LocalizationEditor.TextAsset == null)
		{
			EditorGUILayout.HelpBox("Select a file to store localization data. It must be a " + LocalizationEditor.FileFormat +
				" file inside a " + LocalizationEditor.ResourcesFolder + " folder.", MessageType.Error);
		}
		else
		{
			var assetPath = AssetDatabase.GetAssetPath(LocalizationEditor.TextAsset);
			bool isInResources = assetPath.Contains(LocalizationEditor.ResourcesFolder);
			bool isTxtFile = assetPath.Contains(LocalizationEditor.FileFormat);

			if (!isInResources)
				EditorGUILayout.HelpBox("File must be inside a " + LocalizationEditor.ResourcesFolder + " folder.", MessageType.Error);

			if (!isTxtFile)
				EditorGUILayout.HelpBox("File must be in the " + LocalizationEditor.FileFormat + " format.", MessageType.Error);

			if (isInResources && isTxtFile)
				DrawItemList();
		}
	}

	void DrawFile()
	{
		var lastTextAsset = LocalizationEditor.TextAsset;
		_foldoutFile = EditorGUILayout.Foldout(_foldoutFile, "Localization File");
		if (_foldoutFile)
		{
			LocalizationEditor.TextAsset = (TextAsset)EditorGUILayout.ObjectField(LocalizationEditor.TextAsset, typeof(TextAsset), false);
			if (LocalizationEditor.TextAsset != lastTextAsset)
			{
				LocalizationManager.ClearInstance();
				LocalizationEditor.SaveDataPath();
			}
			EditorGUILayout.Space();
		}
	}

	void DrawLanguages()
	{
		if (LocalizationEditor.TextAsset == null)
			return;

		_foldoutLanguages = EditorGUILayout.Foldout(_foldoutLanguages, "Supported Languages");
		if (_foldoutLanguages)
			_languageReorderableList.DoLayoutList();

		if (LocalizationManager.Instance.Data.SupportedLanguages.Count == 0)
			EditorGUILayout.HelpBox("You must add at least one supported language.", MessageType.Warning);

		EditorGUILayout.Space();
	}

	void DrawItemListTitleAndAddButton()
	{
		if (_titleLabelStyle == null)
		{
			_titleLabelStyle = new GUIStyle(EditorStyles.boldLabel);
			_titleLabelStyle.fontSize = 20;
			_titleLabelStyle.alignment = TextAnchor.LowerLeft;
		}

		if (_titleButtonStyle == null)
		{
			_titleButtonStyle = new GUIStyle(GUI.skin.button);
			_titleButtonStyle.fontSize = 16;
			_titleButtonStyle.fontStyle = FontStyle.Bold;
			_titleButtonStyle.alignment = TextAnchor.MiddleCenter;
		}

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Texts", _titleLabelStyle, GUILayout.Width(70), GUILayout.Height(25));
		if (GUILayout.Button("+", _titleButtonStyle, GUILayout.Width(26), GUILayout.Height(19)))
		{
			_searchTerms = "";

			LocalizationEditor.CreateNewItem(_categoryFilter);

			_scrollPos = new Vector2(_scrollPos.x, float.MaxValue);
		}
		EditorGUILayout.EndHorizontal();
	}

	void DrawItemList()
	{
		DrawItemListTitleAndAddButton();

		var itemCount = LocalizationManager.Instance.Data.Items.Count;
		if (itemCount > 0)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			DrawItemListHeader();

			_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
			EditorGUI.BeginChangeCheck();
			_displayedItemCount = 0;
			_itemsToDelete.Clear();
			int rowIndex = 0;
			for (int i = 0; i < itemCount; i++)
			{
				var localizationItem = LocalizationManager.Instance.Data.Items[i];

				// Category filter
				if (!string.IsNullOrEmpty(_categoryFilter) && _categoryFilter != LocalizationEditor.CategoryFilterAll && _categoryFilter.ToLower() != localizationItem.Category.ToLower())
					continue;

				// Search term filter
				if (!string.IsNullOrWhiteSpace(_searchTerms))
				{
					bool matches = false;

					string emptyFilter = LocalizationEditor.SearchNotFilledTag;
					if (_searchTerms.ToLower().StartsWith(emptyFilter))
					{
						string emptyLanguage = _searchTerms.ToLower().Substring(emptyFilter.Length);
						foreach (var translationItem in localizationItem.Translations)
						{
							if (string.IsNullOrWhiteSpace(translationItem.Text) &&
								(string.IsNullOrWhiteSpace(emptyLanguage) || translationItem.Language.ToLower().Contains(emptyLanguage.ToLower())))
							{
								matches = true;
								break;
							}
						}
					}
					else if (localizationItem.Category.ToLower().Contains(_searchTerms.ToLower()))
						matches = true;
					else if (localizationItem.Name.ToLower().Contains(_searchTerms.ToLower()))
						matches = true;
					else
					{
						foreach (var translationItem in localizationItem.Translations)
						{
							if (translationItem.Text.ToLower().Contains(_searchTerms.ToLower()))
							{
								matches = true;
								break;
							}
						}
					}

					if (!matches)
						continue;
				}

				bool isDeleted = DrawItem(localizationItem, rowIndex);
				if (isDeleted)
					_itemsToDelete.Add(localizationItem);
				else
					_displayedItemCount++;

				rowIndex++;
			}

			for (int i = 0; i < _itemsToDelete.Count; i++)
				LocalizationManager.Instance.Data.Items.Remove(_itemsToDelete[i]);

			if (EditorGUI.EndChangeCheck())
				LocalizationEditor.Save();

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
			DrawCategoryFilter();
		}
	}

	void DrawItemListHeader()
	{
		// Draw localization item list header
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("", GUILayout.Width(35));
		var lastRect = GUILayoutUtility.GetLastRect();

		var lastFoldout = _foldoutAllItems;
		lastRect.xMin += 2;
		_foldoutAllItems = EditorGUI.Foldout(lastRect, _foldoutAllItems, "");
		if (_foldoutAllItems != lastFoldout)
		{
			List<string> keys = new List<string>(_showItemDetails.Keys);
			foreach (var key in keys)
				_showItemDetails[key] = _foldoutAllItems;
		}

		EditorGUILayout.LabelField("Category", EditorStyles.boldLabel, GUILayout.Width(100));
		EditorGUILayout.LabelField("Name", EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal();
	}

	bool DrawItem(LocalizationItem localizationItem, int itemIndex)
	{
		if (!_showItemDetails.ContainsKey(localizationItem.ID))
			_showItemDetails[localizationItem.ID] = _foldoutAllItems;

		if (_foldoutStyle == null)
		{
			_foldoutStyle = new GUIStyle(EditorStyles.foldout);
			_foldoutStyle.fixedWidth = 5;
		}

		var guiColor = GUI.color;
		if (itemIndex % 2 == 0)
			GUI.color = Color.gray;
		EditorGUILayout.BeginVertical(EditorStyles.helpBox);
		GUI.color = guiColor;
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("", GUILayout.Width(30));
		localizationItem.Category = EditorGUILayout.DelayedTextField(localizationItem.Category, GUILayout.Width(100));

		var foldoutRect = GUILayoutUtility.GetLastRect();
		bool showDetails = _showItemDetails[localizationItem.ID];
		foldoutRect.xMin -= 30;
		foldoutRect.width = 30;
		showDetails = EditorGUI.Foldout(foldoutRect, showDetails, "");
		_showItemDetails[localizationItem.ID] = showDetails;

		localizationItem.Name = EditorGUILayout.DelayedTextField(localizationItem.Name, GUILayout.MaxWidth(200));

		if (GUILayout.Button("X", GUILayout.Width(20)))
		{
			if (localizationItem.Name.StartsWith(LocalizationEditor.DefaultNewTextItemName) || EditorUtility.DisplayDialog("Delete", "Are you sure you want to delete " + localizationItem.MenuDisplayName + "?", "Delete", "Cancel"))
				return true;
		}

		if (GUILayout.Button("Edit", GUILayout.Width(36)))
		{
			var item = LocalizationManager.Instance.GetTextItemByID(localizationItem.ID);
			if (item != null)
				EditLocalizationItemWindow.ShowWindow(item, false);
		}

		EditorGUILayout.EndHorizontal();

		if (showDetails)
		{
			foreach (var translation in localizationItem.Translations)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("", GUILayout.Width(10));
				EditorGUILayout.LabelField(translation.Language, GUILayout.Width(50));
				translation.Text = EditorGUILayout.DelayedTextField(translation.Text);
				EditorGUILayout.EndHorizontal();
			}
		}
		EditorGUILayout.EndVertical();

		return false;
	}

	void DrawCategoryFilter()
	{
		if (_lastAlignRight == null)
			_lastAlignRight = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleRight };

		// TODO: Don't refresh every time, only on data change
		{
			List<string> categories = new List<string>();
			List<string> categoriesToLower = new List<string>();
			categories.Add(LocalizationEditor.CategoryFilterAll);
			for (int i = 0; i < LocalizationManager.Instance.Data.Items.Count; i++)
			{
				var value = LocalizationManager.Instance.Data.Items[i].Category;
				if (!categoriesToLower.Contains(value.ToLower()))
				{
					categories.Add(value);
					categoriesToLower.Add(value.ToLower());
				}
			}

			categories = categories.OrderBy(o => o).ToList();
			_categories = categories.ToArray();
		}

		int categoryFilterIndex = string.IsNullOrEmpty(_categoryFilter) ? 0 : Array.IndexOf(_categories, _categoryFilter);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Filter", GUILayout.Width(34));
		categoryFilterIndex = EditorGUILayout.Popup(categoryFilterIndex, _categories, GUILayout.Width(100));
		_searchTerms = EditorGUILayout.DelayedTextField("", _searchTerms, GUILayout.Width(100));
		EditorGUILayout.LabelField(_displayedItemCount + " Entries", _lastAlignRight);
		EditorGUILayout.EndHorizontal();
		_categoryFilter = categoryFilterIndex >= 0 ? _categories[categoryFilterIndex] : "";
	}

	void OnEnable()
	{
		_foldoutFile = LocalizationEditor.TextAsset == null;
		_foldoutLanguages = LocalizationManager.Instance.Data.SupportedLanguages.Count <= 1;

		_languageReorderableList = new ReorderableList(LocalizationManager.Instance.Data.SupportedLanguages, typeof(LanguageData), true, false, true, true);
		_languageReorderableList.drawElementCallback += DrawLanguageListElement;

		_languageReorderableList.onAddCallback += AddItem;
		_languageReorderableList.onRemoveCallback += RemoveItem;
		_languageReorderableList.onReorderCallback += ReorderItem;
	}

	void OnDisable()
	{
		_languageReorderableList.drawElementCallback -= DrawLanguageListElement;

		_languageReorderableList.onAddCallback -= AddItem;
		_languageReorderableList.onRemoveCallback -= RemoveItem;
		_languageReorderableList.onReorderCallback -= ReorderItem;
	}

	void DrawLanguageListElement(Rect rect, int index, bool active, bool focused)
	{
		var supportedLanguage = LocalizationManager.Instance.Data.SupportedLanguages[index];
		var languageData = LocalizationManager.Instance.GetLanguageData(supportedLanguage.Name);
		EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, rect.height), languageData.LongDisplayName);

		string[] statuses = Enum.GetNames(typeof(SupportedLanguageStatus));
		statuses[0] = "Enabled - Default Language";
		var lastStatus = supportedLanguage.Status;
		supportedLanguage.Status = (SupportedLanguageStatus)EditorGUI.Popup(new Rect(204, rect.y + 1, 100, rect.height), (int)supportedLanguage.Status, statuses);
		if (lastStatus != supportedLanguage.Status)
			LocalizationEditor.Save();

		if (LocalizationManager.Instance.Data.Items.Count > 0)
		{
			float filledRate = LocalizationEditor.CalculateFillRate(supportedLanguage.Name);
			EditorGUI.LabelField(new Rect(314, rect.y + 1, 80, rect.height), "Fill: " + (filledRate * 100f).ToString("0.00") + "%");
			if (filledRate < 1f && GUI.Button(new Rect(400, rect.y + 1, 44, 16), "Show"))
			{
				_searchTerms = LocalizationEditor.SearchNotFilledTag + supportedLanguage.Name;
			}
		}
	}

	void AddItem(ReorderableList list)
	{
		var selectLanguageWindow = (SelectLanguageWindow)GetWindow(typeof(SelectLanguageWindow), true, "Select Language");
		selectLanguageWindow.LocalizationEditorWindow = this;
	}

	public void AddLanguage(string language)
	{
		bool isFirstLanguage = LocalizationManager.Instance.Data.SupportedLanguages.Count == 0;
		LocalizationManager.Instance.Data.SupportedLanguages.Add(new SupportedLanguage()
			{ Name = language, Status = isFirstLanguage ? SupportedLanguageStatus.DefaultLanguage : SupportedLanguageStatus.Enabled } );

		LocalizationEditor.RefreshTextItemLanguages();
		LocalizationEditor.Save();
	}

	void RemoveItem(ReorderableList list)
	{
		LocalizationManager.Instance.Data.SupportedLanguages.RemoveAt(list.index);
		LocalizationEditor.RefreshTextItemLanguages();
		LocalizationEditor.Save();
	}

	void ReorderItem(ReorderableList list)
	{
		LocalizationEditor.RefreshTextItemLanguages();
		LocalizationEditor.Save();
	}

	// Add menu item
	[MenuItem("Window/Localization Editor")]
	public static void ShowWindow()
	{
		GetWindow(typeof(LocalizationEditorWindow), false, "Localization");
	}
}
