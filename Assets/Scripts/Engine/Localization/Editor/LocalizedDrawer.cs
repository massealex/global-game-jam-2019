﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(LocalizedAttribute))]
public class LocalizedDrawer : PropertyDrawer
{
	static int EditButtonWidth = 36;
	static int AddButtonWidth = 42;
	static int AddButtonPadding = 3;
	static int InfoButtonWidth = 20;

	string[] _textIDs;
	string[] _textDisplayNames;
	int _currentTextItemIndex;

	// + ≡ ✎ ✍  ☝ ☛ ☆ ★ ☄

	void InitCurrentText(SerializedProperty property)
	{
		var textID = property.stringValue;

		 _currentTextItemIndex = -1;
		var itemCount = LocalizationManager.Instance.Data.Items.Count;
		_textIDs = new string[itemCount];
		_textDisplayNames = new string[itemCount];
		for (int i = 0; i < itemCount; i++)
		{
			var textItem = LocalizationManager.Instance.Data.Items[i];
			_textIDs[i] = textItem.ID;
			_textDisplayNames[i] = textItem.MenuDisplayName;

			if (textItem.ID == textID)
				_currentTextItemIndex = i;
		}
	}

	public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
	{
		InitCurrentText(property);

		//var localized = attribute as LocalizedAttribute;

		if (property.propertyType == SerializedPropertyType.String)
			DrawControl(rect, property);
		else
			EditorGUI.HelpBox(rect, "Use Localized on string fields only.", MessageType.Error);
	}

	void DrawControl(Rect rect, SerializedProperty property)
	{
		bool canEdit = _currentTextItemIndex > -1;

		Rect labelRect = rect;
		labelRect.width = EditorGUIUtility.labelWidth;
		EditorGUI.LabelField(labelRect, property.name);

		EditorGUI.BeginChangeCheck();
		Rect popupRect = rect;
		popupRect.width -= InfoButtonWidth + AddButtonWidth + (canEdit ? EditButtonWidth : 0);
		popupRect.xMin += EditorGUIUtility.labelWidth;
		 _currentTextItemIndex = EditorGUI.Popup(popupRect, _currentTextItemIndex, _textDisplayNames);
		if (EditorGUI.EndChangeCheck())
			property.stringValue = _textIDs[_currentTextItemIndex];

		

		if (canEdit)
		{
			Rect buttonEditRect = rect;
			buttonEditRect.xMax = buttonEditRect.xMax - InfoButtonWidth - AddButtonWidth;
			buttonEditRect.xMin = buttonEditRect.xMax - EditButtonWidth;
			buttonEditRect.height -= 1;
			if (GUI.Button(buttonEditRect, "Edit"))
			{
				var item = LocalizationManager.Instance.GetTextItemByID(_textIDs[_currentTextItemIndex]);
				if (item != null)
					EditLocalizationItemWindow.ShowWindow(item, false);
			}
		}

		Rect buttonAddRect = rect;
		buttonAddRect.xMax = buttonAddRect.xMax - InfoButtonWidth;
		buttonAddRect.xMin = buttonAddRect.xMax - AddButtonWidth;
		buttonAddRect.height -= 1;
		buttonAddRect.xMin += AddButtonPadding;
		buttonAddRect.xMax -= AddButtonPadding;
		if (GUI.Button(buttonAddRect, "Add"))
		{
			var newItem = LocalizationEditor.CreateNewItem();
			property.stringValue = newItem.ID;
			EditLocalizationItemWindow.ShowWindow(newItem, true);
		}

		Rect buttonInfoRect = rect;
		buttonInfoRect.xMin = buttonInfoRect.xMax - InfoButtonWidth;
		buttonInfoRect.height -= 1;
		if (GUI.Button(buttonInfoRect, "≡"))
			LocalizationEditorWindow.ShowWindow();
	}
}