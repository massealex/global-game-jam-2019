﻿using UnityEditor;
using UnityEngine;

public class EditLocalizationItemWindow : EditorWindow
{
	Vector2 _scrollPos;
	GUIStyle _titleLabelStyle;
	string[] _supportedLanguages;
	int _currentLanguageIndex;

	public LocalizationItem Item { get; set; }
	public LocalizationItem BackUpItem { get; set; }
	public bool IsNewItem { get; set; }

	void OnEnable()
	{
		if (_titleLabelStyle == null)
		{
			_titleLabelStyle = new GUIStyle(EditorStyles.boldLabel);
			_titleLabelStyle.fontSize = 20;
			_titleLabelStyle.alignment = TextAnchor.LowerLeft;
		}

		_supportedLanguages = new string[LocalizationManager.Instance.Data.SupportedLanguages.Count];
		for (int i = 0; i < _supportedLanguages.Length; i++)
			_supportedLanguages[i] = LocalizationManager.Instance.Data.SupportedLanguages[i].Name;
	}

	void OnGUI()
	{
		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Category", GUILayout.Width(65));
		Item.Category = EditorGUILayout.TextField(Item.Category);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Name", GUILayout.Width(65));
		Item.Name = EditorGUILayout.TextField(Item.Name);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();

		DrawTranslatedTextBox();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Text ID:", GUILayout.Width(50));
		EditorGUILayout.SelectableLabel(Item.ID);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		#if UNITY_EDITOR_OSX
			DrawCancelButton();
			DrawConfirmButton();
		#else
			DrawConfirmButton();
			DrawCancelButton();
		#endif
		EditorGUILayout.EndHorizontal();
	}

	void DrawTranslatedTextBox()
	{
		if (LocalizationManager.Instance.Data.SupportedLanguages.Count == 0)
		{
			EditorGUILayout.HelpBox("You must add at least one supported language in the Localization editor window.", MessageType.Warning);
			return;
		}

		EditorGUILayout.BeginVertical(EditorStyles.helpBox);
		_currentLanguageIndex = EditorGUILayout.Popup("Text", _currentLanguageIndex, _supportedLanguages);
		_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);

		var lastText = "";
		foreach (var item in Item.Translations)
			if (item.Language == _supportedLanguages[_currentLanguageIndex])
				lastText = item.Text;

		var newText = EditorGUILayout.TextArea(lastText, GUILayout.Height(position.height - 30));

		if (newText != lastText)
			foreach (var item in Item.Translations)
				if (item.Language == _supportedLanguages[_currentLanguageIndex])
					item.Text = newText;

		EditorGUILayout.EndScrollView();
		EditorGUILayout.EndVertical();
		EditorGUILayout.Space();
	}

	void DrawConfirmButton()
	{
		if (GUILayout.Button("✔ " + (IsNewItem ? "Create" : "Save"), GUILayout.Height(25)))
		{
			LocalizationEditor.Save();
			Close();
		}
	}

	void DrawCancelButton()
	{
		if (GUILayout.Button("☇ Cancel ", GUILayout.Height(25)))
		{
			int index = LocalizationManager.Instance.Data.Items.IndexOf(Item);
			if (index > -1)
				LocalizationManager.Instance.Data.Items.RemoveAt(index);
			if (!IsNewItem)
				LocalizationManager.Instance.Data.Items.Insert(index, BackUpItem);


			LocalizationEditor.Save();
			Close();
		}
	}

	public static void ShowWindow(LocalizationItem item, bool isNew)
	{
		if (item == null)
			return;

		var window = (EditLocalizationItemWindow)GetWindow(typeof(EditLocalizationItemWindow), true,
			isNew ? "☄ Create New Text" : "✎ Edit Localization");

		window.IsNewItem = isNew;
		window.Item = item;
		window.BackUpItem = JsonUtility.FromJson<LocalizationItem>(JsonUtility.ToJson(item));
	}
}
