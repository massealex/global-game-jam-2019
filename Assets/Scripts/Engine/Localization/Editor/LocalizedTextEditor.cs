﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LocalizedText))]
public class LocalizedTextEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		var translatedText = (LocalizedText)target;

		var textResult = translatedText == null ? "" : translatedText.GetString();
		if (textResult != null && textResult.StartsWith("["))
			textResult = "";

		if (textResult != null && textResult.Contains("\n"))
			EditorGUILayout.LabelField("Translated Text: ", textResult, GUILayout.Height(50));
		else
			EditorGUILayout.LabelField("Translated Text: ", textResult);

		if (translatedText.TextComponentType == LocalizedText.TextComponents.Null)
			EditorGUILayout.HelpBox("Please add a text component", MessageType.Error);

		if (GUI.changed)
			translatedText.Refresh();
	}
}
