using System;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager
{
	public static string DataPath => "LocalizationData"; // AUTO GENERATED VALUE - DO NOT EDIT
	public static string EnglishLanguage => "English";

	static LocalizationManager _instance = null;
	public static LocalizationManager Instance { get { if (_instance == null) { _instance = new LocalizationManager(); _instance.Init(); } return _instance; } }
	public static void ClearInstance() { _instance = null; }

	public LocalizationData Data;
	public List<LanguageData> AllLanguageData = null;
	public event Action OnLanguageChanged;

	string _currentLanguage;
	int _currentLanguageIndex;

	public string CurrentLanguage {
		get { return _currentLanguage; }
		private set
		{
			_currentLanguage = value;

			for (int i = 0; i < Data.SupportedLanguages.Count; i++)
				if (_currentLanguage == Data.SupportedLanguages[i].Name)
					_currentLanguageIndex = i;
		}
	}

	public void SetCurrentLanguage(string language)
	{
		CurrentLanguage = language;
		OnLanguageChanged?.Invoke();
	}

	public void Init()
	{
		Data = new LocalizationData();

		var textAsset = (TextAsset)Resources.Load(DataPath, typeof(TextAsset));
		if (textAsset != null)
		{
			var data = JsonUtility.FromJson<LocalizationData>(textAsset.text);
			if (data != null)
				Data = data;
		}

		LoadLanguageData();

		// Set language to detected system language or to the default language if the user's computer language is not supported
		CurrentLanguage = EnglishLanguage;
		if (Application.isPlaying)
		{
			foreach (var supportedLanguage in Data.SupportedLanguages)
			{
				var languageData = GetLanguageData(supportedLanguage.Name);
				if (supportedLanguage.IsEnabled && languageData.SystemLanguage == Application.systemLanguage)
				{
					CurrentLanguage = languageData.Name;
					break;
				}
				else if (supportedLanguage.Status == SupportedLanguageStatus.DefaultLanguage)
				{
					CurrentLanguage = languageData.Name;
				}
			}
		}
	}

	public string GetString(string textID, params string[] parameters)
	{
		var item = GetTextItemByID(textID);
		return GetString(item, parameters);
	}

	string GetString(LocalizationItem item, params string[] parameters)
	{
		if (item != null)
		{
			var result = item.Translations[_currentLanguageIndex].Text;
			if (parameters != null && parameters.Length > 0)
				result = string.Format(result, parameters);
			return result;
		}

		return string.Empty;
	}

	public string GetStringByCategoryAndName(string category, string name, params string[] parameters)
	{
		var item = GetTextItemByCategoryAndName(category, name);
		return GetString(item, parameters);
	}

	public LocalizationItem GetTextItemByID(string textID)
	{
		for (int i = 0; i < Data.Items.Count; i++)
		{
			var item = Data.Items[i];
			if (item.ID == textID)
				return item;
		}

		return null;
	}

    public LocalizationItem GetTextItemByName(string name)
    {
        for (int i = 0; i < Data.Items.Count; i++)
        {
            var item = Data.Items[i];
            if (item.Name == name)
                return item;
        }

        return null;
    }

    public LocalizationItem GetTextItemByCategoryAndName(string category, string name)
	{
		for (int i = 0; i < Data.Items.Count; i++)
		{
			var item = Data.Items[i];
			if (item.Category == category && item.Name == name)
				return item;
		}

		return null;
	}

	public string GetTextIDByCategoryAndName(string category, string name)
	{
		var textItem = GetTextItemByCategoryAndName(category, name);
		return textItem == null ? string.Empty : textItem.ID;
	}

	public LanguageData GetLanguageData(string languageName)
	{
		foreach (var data in AllLanguageData)
			if (data.Name == languageName)
				return data;

		return null;
	}

	public bool IsLanguageSupported(string languageName)
	{
		foreach (var language in Data.SupportedLanguages)
			if (language.Name == languageName)
				return true;

		return false;
	}

	void LoadLanguageData()
	{
		// Source:
		// https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers
		// https://en.wikipedia.org/wiki/List_of_language_names

		AllLanguageData = new List<LanguageData>();
		AllLanguageData.Add(new LanguageData() { Name = EnglishLanguage, NativeName = "English", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "French", NativeName = "Français", SystemLanguage = SystemLanguage.French });
		AllLanguageData.Add(new LanguageData() { Name = "Chinese - Simplified", NativeName = "简体中文", SystemLanguage = SystemLanguage.ChineseSimplified });
		AllLanguageData.Add(new LanguageData() { Name = "Chinese - Traditional", NativeName = "繁體中文", SystemLanguage = SystemLanguage.ChineseTraditional });
		AllLanguageData.Add(new LanguageData() { Name = "Spanish", NativeName = "Español", SystemLanguage = SystemLanguage.Spanish });
		AllLanguageData.Add(new LanguageData() { Name = "Hindi", NativeName = "हिन्दी", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Arabic", NativeName = "العربية", SystemLanguage = SystemLanguage.Arabic });
		AllLanguageData.Add(new LanguageData() { Name = "Portuguese", NativeName = "Português", SystemLanguage = SystemLanguage.Portuguese });
		AllLanguageData.Add(new LanguageData() { Name = "Bengali", NativeName = "বাংলা", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Russian", NativeName = "Русский", SystemLanguage = SystemLanguage.Russian });
		AllLanguageData.Add(new LanguageData() { Name = "Japanese", NativeName = "日本語", SystemLanguage = SystemLanguage.Japanese });
		AllLanguageData.Add(new LanguageData() { Name = "Punjabi", NativeName = "ਪੰਜਾਬੀ", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "German", NativeName = "Deutsch", SystemLanguage = SystemLanguage.German });
		AllLanguageData.Add(new LanguageData() { Name = "Javanese", NativeName = "Basa Jawa", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Wu", NativeName = "吴方言", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Malay", NativeName = "Bahasa Melayu", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Telugu", NativeName = "Telugu", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Vietnamese", NativeName = "tiếng Việt", SystemLanguage = SystemLanguage.Vietnamese });
		AllLanguageData.Add(new LanguageData() { Name = "Korean", NativeName = "한국어", SystemLanguage = SystemLanguage.Korean });
		AllLanguageData.Add(new LanguageData() { Name = "Turkish", NativeName = "Türkçe", SystemLanguage = SystemLanguage.Turkish });
		AllLanguageData.Add(new LanguageData() { Name = "Italian", NativeName = "Italiano", SystemLanguage = SystemLanguage.Italian });
		AllLanguageData.Add(new LanguageData() { Name = "Cantonese", NativeName = "廣東話", SystemLanguage = SystemLanguage.English });
		AllLanguageData.Add(new LanguageData() { Name = "Thai", NativeName = "ภาษาไทย", SystemLanguage = SystemLanguage.Thai });
		AllLanguageData.Add(new LanguageData() { Name = "Dutch", NativeName = "Nederlands", SystemLanguage = SystemLanguage.Dutch });
		AllLanguageData.Add(new LanguageData() { Name = "Greek", NativeName = "Ελληνικά", SystemLanguage = SystemLanguage.Greek });
		AllLanguageData.Add(new LanguageData() { Name = "Swedish", NativeName = "Svenska", SystemLanguage = SystemLanguage.Swedish });
		AllLanguageData.Add(new LanguageData() { Name = "Finnish", NativeName = "Suomi", SystemLanguage = SystemLanguage.Finnish });
		AllLanguageData.Add(new LanguageData() { Name = "Norwegian", NativeName = "Norsk", SystemLanguage = SystemLanguage.Norwegian });
		AllLanguageData.Add(new LanguageData() { Name = "Afrikaans", NativeName = "Afrikaans", SystemLanguage = SystemLanguage.Afrikaans });
		AllLanguageData.Add(new LanguageData() { Name = "Basque", NativeName = "Euskara", SystemLanguage = SystemLanguage.Basque });
		AllLanguageData.Add(new LanguageData() { Name = "Belarusian", NativeName = "Беларуская", SystemLanguage = SystemLanguage.Belarusian });
		AllLanguageData.Add(new LanguageData() { Name = "Bulgarian", NativeName = "български език", SystemLanguage = SystemLanguage.Bulgarian });
		AllLanguageData.Add(new LanguageData() { Name = "Catalan", NativeName = "Català", SystemLanguage = SystemLanguage.Catalan });
		AllLanguageData.Add(new LanguageData() { Name = "Czech", NativeName = "čeština", SystemLanguage = SystemLanguage.Czech });
		AllLanguageData.Add(new LanguageData() { Name = "Danish", NativeName = "Dansk", SystemLanguage = SystemLanguage.Danish });
		AllLanguageData.Add(new LanguageData() { Name = "Estonian", NativeName = "Eesti", SystemLanguage = SystemLanguage.Estonian });
		AllLanguageData.Add(new LanguageData() { Name = "Faroese", NativeName = "Føroyskt", SystemLanguage = SystemLanguage.Faroese });
		AllLanguageData.Add(new LanguageData() { Name = "Hebrew", NativeName = "עברית‬", SystemLanguage = SystemLanguage.Hebrew });
		AllLanguageData.Add(new LanguageData() { Name = "Hungarian", NativeName = "Magyar", SystemLanguage = SystemLanguage.Hungarian });
		AllLanguageData.Add(new LanguageData() { Name = "Icelandic", NativeName = "Íslenska", SystemLanguage = SystemLanguage.Icelandic });
		AllLanguageData.Add(new LanguageData() { Name = "Indonesian", NativeName = "Bahasa Indonesia", SystemLanguage = SystemLanguage.Indonesian });
		AllLanguageData.Add(new LanguageData() { Name = "Latvian", NativeName = "Latviešu", SystemLanguage = SystemLanguage.Latvian });
		AllLanguageData.Add(new LanguageData() { Name = "Lithuanian", NativeName = "Lietuvių", SystemLanguage = SystemLanguage.Lithuanian });
		AllLanguageData.Add(new LanguageData() { Name = "Polish", NativeName = "Polski", SystemLanguage = SystemLanguage.Polish });
		AllLanguageData.Add(new LanguageData() { Name = "Romanian", NativeName = "Română", SystemLanguage = SystemLanguage.Romanian });
		AllLanguageData.Add(new LanguageData() { Name = "Serbo Croatian", NativeName = "Srpskohrvatski", SystemLanguage = SystemLanguage.SerboCroatian });
		AllLanguageData.Add(new LanguageData() { Name = "Slovak", NativeName = "Slovenčina", SystemLanguage = SystemLanguage.Slovak });
		AllLanguageData.Add(new LanguageData() { Name = "Slovene", NativeName = "Slovenščina", SystemLanguage = SystemLanguage.Slovenian });
		AllLanguageData.Add(new LanguageData() { Name = "Ukrainian", NativeName = "Українська", SystemLanguage = SystemLanguage.Ukrainian });

		// TODO: generate text id class. Maybe not because it will recompile anytime we add a new string
		// TODO: Create Add/edit window
	}
}

[Serializable]
public class LocalizationData
{
	public List<LocalizationItem> Items = new List<LocalizationItem>();
	public List<SupportedLanguage> SupportedLanguages = new List<SupportedLanguage>();
}

[Serializable]
public class LocalizationItem
{
	public string ID;
	public string Category;
	public string Name;
	public LocalizationKeyValue[] Translations;

	public string MenuDisplayName => Category + "/" + Name;
}

[Serializable]
public class LocalizationKeyValue
{
	public string Language;
	public string Text;
}

[Serializable]
public class LanguageData
{
	public string Name;
	public string NativeName;
	public SystemLanguage SystemLanguage;

	public string LongDisplayName => Name + " (" + NativeName + ")";
}

[Serializable]
public class SupportedLanguage
{
	public string Name;
	public SupportedLanguageStatus Status;

	public bool IsEnabled => Status == SupportedLanguageStatus.DefaultLanguage || Status == SupportedLanguageStatus.Enabled;
}

public enum SupportedLanguageStatus
{
	DefaultLanguage, Enabled, Disabled
}
