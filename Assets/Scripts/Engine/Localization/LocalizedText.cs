﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
	public enum TextComponents { Null, Text, TextMesh, TextMeshProUI, TextMeshPro }
	public enum Capitalization { None, ToUpper, ToLower, FirstWordLetterToUpper }

	[Localized]
	public string TextID;
	public Capitalization CapitalizationModifier;
	public bool RemoveColonAndParams;

	bool _isTextComponentSet;
	TextComponents _textComponentType;
	Text _text;
	TextMesh _textMesh;
	TMPro.TextMeshProUGUI _uiProText;
	TMPro.TextMeshPro _proText;
	Capitalization _lastCapitalizationModifier;
	string _lastTextID;
	string[] _stringParameters;

	public TextComponents TextComponentType { get { InitTextComponentReference(); return _textComponentType; } private set { _textComponentType = value; } }

	public void SetTextAndParameters(string text, params string[] parameters)
	{
		TextID = text;

		_stringParameters = parameters;
		Refresh();
	}

	public void SetStringParameters(params string[] parameters)
	{
		_stringParameters = parameters;
		Refresh();
	}

	void Update()
	{
		if (_lastTextID != TextID || _lastCapitalizationModifier != CapitalizationModifier)
			Refresh();
	}

	void OnEnable()
	{
		LocalizationManager.Instance.OnLanguageChanged += OnLanguageChanged;
		Refresh();
	}

	void OnDisable()
	{
		LocalizationManager.Instance.OnLanguageChanged -= OnLanguageChanged;
	}

	void OnLanguageChanged()
	{
		Refresh();
	}

	public void Refresh()
	{
		_lastTextID = TextID;
		_lastCapitalizationModifier = CapitalizationModifier;

		SetLabelText(GetString());
	}

	public string GetString()
	{
		var result = LocalizationManager.Instance.GetString(TextID, _stringParameters);

		if (CapitalizationModifier == Capitalization.ToUpper)
			result = result.ToUpper();
		else if (CapitalizationModifier == Capitalization.ToLower)
			result = result.ToLower();

		if (RemoveColonAndParams)
		{
			int index = result.IndexOf(":");
			if (index > -1)
				result = result.Substring(0, index).TrimEnd();
			for (int i = 0; i < 5; i++)
				result = result.Replace("{" + i + "}", "").Trim();
		}

		return result;
	}

	void InitTextComponentReference()
	{
		if (!_isTextComponentSet)
		{
			_text = GetComponent<Text>();
			_textMesh = GetComponent<TextMesh>();
			_uiProText = GetComponent<TMPro.TextMeshProUGUI>();
			_proText = GetComponent<TMPro.TextMeshPro>();

			if (_text != null)
				TextComponentType = TextComponents.Text;
			else if (_textMesh != null)
				TextComponentType = TextComponents.TextMesh;
			else if (_uiProText != null)
				TextComponentType = TextComponents.TextMeshProUI;
			else if (_proText != null)
				TextComponentType = TextComponents.TextMeshPro;
			else
				TextComponentType = TextComponents.Null;

			_isTextComponentSet = true;
		}
	}

	void SetLabelText(string value)
	{
		if (string.IsNullOrEmpty(value))
			return;

		InitTextComponentReference();

		switch (TextComponentType)
		{
			case TextComponents.Text:
				_text.text = value;
				break;
			case TextComponents.TextMesh:
				_textMesh.text = value;
				break;
			case TextComponents.TextMeshProUI:
				_uiProText.text = value;
				break;
			case TextComponents.TextMeshPro:
				_proText.text = value;
				break;
			default:
				break;
		}
	}

	public void SetColor(Color value)
	{
		switch (TextComponentType)
		{
			case TextComponents.Text:
				_text.color = value;
				break;
			case TextComponents.TextMesh:
				_textMesh.color = value;
				break;
			case TextComponents.TextMeshProUI:
				_uiProText.color = value;
				break;
			case TextComponents.TextMeshPro:
				_proText.color = value;
				break;
			default:
				break;
		}
	}
}
