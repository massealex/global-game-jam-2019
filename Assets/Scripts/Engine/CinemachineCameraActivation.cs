﻿using UnityEngine;

[RequireComponent(typeof(Cinemachine.CinemachineVirtualCamera))]
public class CinemachineCameraActivation : MonoBehaviour
{
	public bool IsActive;

	Cinemachine.CinemachineVirtualCamera _camera;
	float _priority;
	float _activePriority = 10f;

	// Start is called before the first frame update
	void Awake()
    {
		_camera = GetComponent<Cinemachine.CinemachineVirtualCamera>();
		_priority = _camera.Priority;
	}

    // Update is called once per frame
    void Update()
    {
		float targetValue = IsActive ? _activePriority : 0f;
		_priority = Mathf.Lerp(_priority, targetValue, Time.deltaTime);
		_camera.Priority = (int)_priority;
	}

	public void SetActivePriority(float activePriority)
	{
		_activePriority = activePriority;
		IsActive = true;
	}
}
