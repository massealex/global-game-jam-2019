﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ClassExtensions
{

	
	public static void Shuffle<T>(this List<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = UnityEngine.Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}
	
	public static Transform FindGrandChild(this Transform subject, string name)
	{
		Transform result = subject.Find (name);
		
		if(result == null)
		{
			foreach(Transform child in subject)
			{
				result = child.FindGrandChild(name);
				if(result != null) return result;
			}
		}
		
		return result;
	}
	
	public static Transform[] GetChildren(this Transform subject)
	{
		List<Transform> lstChildren = new List<Transform>();
		foreach(Transform child in subject)
		{
			lstChildren.Add(child);
		}
		
		
		return lstChildren.ToArray();
	}

	public static Vector2 GetClosestPointToLine(this Vector2 subject, Vector2 pA, Vector2 pB)
	{
		Vector2 AP = subject - (Vector2)pA;       //Vector from A to P   
		Vector2 AB = (Vector2)pB - (Vector2)pA;       //Vector from A to B  

		float magnitudeAB = AB.sqrMagnitude;     //Magnitude of AB vector (it's length squared)     
		float ABAPproduct = Vector2.Dot(AP, AB);    //The DOT product of a_to_p and a_to_b     
		float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

		if (distance < 0) return pA;    //Check if P projection is over vectorAB     
		else if (distance > 1) return pB;
		else return (Vector2)pA + AB * distance;
	}

	public static Vector3 GetClosestPointToSegment(this Vector3 subject, Vector3 pA, Vector3 pB)
	{
		return new Vector2(subject.x, subject.y).GetClosestPointToLine((Vector2)pA, (Vector2)pB);
	}

	public static Rect Merge(this Rect thisRect, Rect otherRect)
	{
		Rect rect = new Rect();

		rect.xMin = Mathf.Min(thisRect.xMin, otherRect.xMin);
		rect.xMax = Mathf.Max(thisRect.xMax, otherRect.xMax);
		rect.yMin = Mathf.Min(thisRect.yMin, otherRect.yMin);
		rect.yMax = Mathf.Max(thisRect.yMax, otherRect.yMax);

		return rect;
	}

	public static Transform Clear(this Transform transform)
	{
		foreach (Transform child in transform)
		{
			GameObject.Destroy(child.gameObject);
		}
		return transform;
	}

	public static float Sample(this float[] fArr, float t)
	{
		int count = fArr.Length;
		if (count == 0)
		{
			Debug.LogError("Unable to sample array - it has no elements");
			return 0;
		}
		if (count == 1)
			return fArr[0];
		float iFloat = t * (count - 1);
		int idLower = Mathf.FloorToInt(iFloat);
		int idUpper = Mathf.FloorToInt(iFloat + 1);
		if (idUpper >= count)
			return fArr[count - 1];
		if (idLower < 0)
			return fArr[0];
		return Mathf.Lerp(fArr[idLower], fArr[idUpper], iFloat - idLower);
	}

	public static Vector2 ToVector2(this float angle, bool isDegreeAngle = true)
	{
		float radianAngle = angle * (isDegreeAngle ? Mathf.Deg2Rad : 1f);
		return new Vector2(Mathf.Cos(radianAngle), Mathf.Sin(radianAngle));
	}

	public static float ToDegreeAngle(this Vector3 diff)
	{
		return ((Vector2)diff).ToDegreeAngle();
	}

	public static float ToDegreeAngleXZ(this Vector3 diff)
	{
		return new Vector2(diff.x, diff.z).ToDegreeAngle();
	}

	public static float ToDegreeAngle(this Vector2 diff)
	{
		return Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
	}

	public static void ForceRefresh(this Behaviour behaviour)
	{
		behaviour.enabled = false;
		behaviour.enabled = true;
	}
}
