﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GraphUI : MonoBehaviour
{
	[Header("Title Panel")]
	public GameObject PanelTitle;
	public TMPro.TextMeshProUGUI LabelTitle;

	[Header("Graph Panel")]
	public RectTransform PanelGraph;
	public RectTransform LineModel;

	[Header("Legend Panel")]
	public GameObject PanelLegend;
	public TMPro.TextMeshProUGUI LabelLegend;

	[Header("Axis")]
	public TMPro.TextMeshProUGUI LabelAxisModel;

	List<GraphData> _graphData = new List<GraphData>();
	List<RectTransform> _lines = new List<RectTransform>();
	List<TMPro.TextMeshProUGUI> _axisTexts = new List<TMPro.TextMeshProUGUI>();
	bool _isDirty;
	Vector2 _lastViewRectSize;

	public delegate string NumberFormatter(float value);
	event NumberFormatter OnFormat = null;
	public void SetNumberFormatter(NumberFormatter methode) { OnFormat = methode; }

	public float MinX { get; set; } = 0;
	public float MaxX { get; set; } = float.MaxValue;
	public int MaxRecentDataCount { get; set; } = -1;
	public float MaxY { get; set; } = 0;
	public int WantedLabelCount => 4;

	void Start()
	{
		LineModel.gameObject.SetActive(false);
		LabelAxisModel.gameObject.SetActive(false);
	}

	public void ClearData()
	{
		_graphData.Clear();
		_isDirty = true;
	}

	public void AddData(GraphData data)
	{
		_graphData.Add(data);
		_isDirty = true;
	}

	void LateUpdate()
	{
		RefreshGraph();
	}

	void RefreshGraph()
	{
		var viewRectSize = GetViewRectSize();
		if (viewRectSize != _lastViewRectSize)
			_isDirty = true;

		if (_isDirty)
		{
			foreach (var line in _lines)
				line.gameObject.SetActive(false);
			foreach (var text in _axisTexts)
				text.gameObject.SetActive(false);

			var viewPositionRect = new Rect(-viewRectSize.x / 2f, -viewRectSize.y / 2f, viewRectSize.x, viewRectSize.y);

			// Organize data by channel
			Dictionary<int, List<GraphData>> elements = new Dictionary<int, List<GraphData>>();
			foreach (var data in _graphData)
			{
				if (!elements.ContainsKey(data.Channel))
					elements.Add(data.Channel, new List<GraphData>());

				elements[data.Channel].Add(data);
			}

			foreach (var entry in elements.Keys)
			{
				// Filter data
				var orderedGraphData = elements[entry].OrderBy(o => o.Coords.x).ToList();

				Rect valueRange = new Rect();
				valueRange.xMin = float.MaxValue;
				valueRange.yMin = float.MaxValue;
				valueRange.xMax = float.MinValue;
				valueRange.yMax = float.MinValue;

				int count = 0;
				for (int i = orderedGraphData.Count - 1; i >= 0; i--)
				{
					var x = orderedGraphData[i].Coords.x;
					var y = orderedGraphData[i].Coords.y;
					if (x < MinX || x > MaxX || (MaxRecentDataCount > 0 && count >= MaxRecentDataCount))
					{
						orderedGraphData.RemoveAt(i);
						continue;
					}

					if (x < valueRange.xMin)
						valueRange.xMin = x;
					if (x > valueRange.xMax)
						valueRange.xMax = x;

					if (y < valueRange.yMin)
						valueRange.yMin = y;
					if (y > valueRange.yMax)
						valueRange.yMax = y;

					count++;
				}

				// For now, force y axis to never cut
				valueRange.yMin = Mathf.Min(valueRange.yMin, 0f);

				if (MaxY > 0)
					valueRange.yMax = Mathf.Max(valueRange.yMax, MaxY);

				var yAxisInterval = GetAppropriateYAxisInterval(valueRange.yMin, valueRange.yMax);
				valueRange.yMax = Mathf.Max(valueRange.yMax, yAxisInterval * WantedLabelCount);

				// Create lines
				for (int i = 1; i < orderedGraphData.Count; i++)
				{
					var previous = orderedGraphData[i - 1];
					var current = orderedGraphData[i];
					var previousPosition = ValuePositionToRectPosition(previous.Coords, valueRange, viewPositionRect);
					var currentPosition = ValuePositionToRectPosition(current.Coords, valueRange, viewPositionRect);
					var diff = currentPosition - previousPosition;

					var line = GetLine();
					line.sizeDelta = new Vector2(Vector2.Distance(previousPosition, currentPosition), line.sizeDelta.y);
					line.localPosition = Vector3.Lerp(previousPosition, currentPosition, 0.5f);
					line.localEulerAngles = new Vector3(0f, 0f, Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg);
				}

				// Create X axis labels
				foreach (var data in orderedGraphData)
				{
					var text = GetAxisText();
					text.alignment = TMPro.TextAlignmentOptions.Midline;
					text.text = OnFormat == null ? data.Coords.x.ToString() : OnFormat(data.Coords.x);
					text.transform.localPosition = ValuePositionToRectPosition(new Vector2(data.Coords.x, valueRange.yMin), valueRange, viewPositionRect) + new Vector2(0f, -25f);
				}

				// Create Y axis labels
				
				int labelCount = Mathf.FloorToInt((valueRange.yMax - valueRange.yMin) / yAxisInterval) + 1;
				for (int i = 0; i < labelCount; i++)
				{
					float amount = valueRange.yMin + i * yAxisInterval;

					var text = GetAxisText();
					text.alignment = TMPro.TextAlignmentOptions.MidlineRight;
					text.text = OnFormat == null ? amount.ToString() : OnFormat(amount);
					text.transform.localPosition = ValuePositionToRectPosition(new Vector2(valueRange.xMin, amount), valueRange, viewPositionRect) + new Vector2(-52f, 0f);
				}
			}

			_isDirty = false;
			_lastViewRectSize = viewRectSize;
		}
	}

	Vector2 GetViewRectSize()
	{
		// Unity has no easy way to get the size of a rect transform if it is set to stretch to its parent:
		Vector3[] corners = new Vector3[4];
		PanelGraph.GetLocalCorners(corners);
		return corners[2] - corners[0];
	}

	Vector2 ValuePositionToRectPosition(Vector2 coords, Rect valueRect, Rect positionRect)
	{
		float xRatio = (coords.x - valueRect.xMin) / (valueRect.width);
		float yRatio = (coords.y - valueRect.yMin) / (valueRect.height);

		return new Vector2(positionRect.xMin + positionRect.width * xRatio, positionRect.yMin + positionRect.height * yRatio);
	}

	// https://stackoverflow.com/questions/326679/choosing-an-attractive-linear-scale-for-a-graphs-y-axis
	float GetAppropriateYAxisInterval(float minY, float maxY)
	{
		float valueRange = maxY - minY;
		float tickRange = valueRange / WantedLabelCount;

		float x = Mathf.Ceil(Mathf.Log10(tickRange));
		float r = tickRange / Mathf.Pow(10f, x);
		if (r <= 0.1f)
			r = 0.1f;
		else if (r <= 0.2f)
			r = 0.2f;
		else if (r <= 0.25f)
			r = 0.25f;
		else if (r <= 0.3f)
			r = 0.3f;
		else if (r <= 0.4f)
			r = 0.4f;
		else if (r <= 0.5f)
			r = 0.5f;
		else if (r <= 0.6f)
			r = 0.6f;
		else if (r <= 0.7f)
			r = 0.7f;
		else if (r <= 0.75f)
			r = 0.75f;
		else if (r <= 0.8f)
			r = 0.8f;
		else if (r <= 0.9f)
			r = 0.9f;
		else if (r <= 1.0f)
			r = 1.0f;

		float interval = r * Mathf.Pow(10f, x);
		return interval;

		//return Mathf.Floor(valueRange / wantedLabelCount);
	}

	RectTransform GetLine()
	{
		foreach (var line in _lines)
		{
			if (!line.gameObject.activeInHierarchy)
			{
				line.gameObject.SetActive(true);
				return line;
			}
		}

		var newLine = Instantiate(LineModel);
		newLine.gameObject.SetActive(true);
		newLine.SetParent(LineModel.parent);
		newLine.localScale = Vector3.one;
		newLine.localPosition = Vector3.zero;
		_lines.Add(newLine);

		return newLine;
	}

	TMPro.TextMeshProUGUI GetAxisText()
	{
		foreach (var text in _axisTexts)
		{
			if (!text.gameObject.activeInHierarchy)
			{
				text.gameObject.SetActive(true);
				return text;
			}
		}

		var newText = Instantiate(LabelAxisModel).GetComponent<TMPro.TextMeshProUGUI>();
		newText.gameObject.SetActive(true);
		newText.transform.SetParent(LineModel.parent);
		newText.transform.localScale = Vector3.one;
		newText.transform.localPosition = Vector3.zero;
		_axisTexts.Add(newText);

		return newText;
	}
}

public struct GraphData
{
	public Vector2 Coords;
	public int Channel;
}
