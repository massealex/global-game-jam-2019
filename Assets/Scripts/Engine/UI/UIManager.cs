﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public Camera UICamera;

	//[Header("Screens")]
	//public UIMainMenu UIMainMenu;
	//public UILevelTransition UILevelTransition;
	//public UIGame UIGame;
	//public UIPause UIPause;
	//public UIDialog UIDialog;

	List<UIWindow> _activeWindows = new List<UIWindow>();

	static UIManager _instance;
	public static UIManager Instance { get { if (_instance == null) { _instance = FindObjectOfType<UIManager>(); } return _instance; } }

	public UIWindow CurrentWindow { get { return _activeWindows.Count > 0 ? _activeWindows[_activeWindows.Count - 1] : null; } }

	void Awake()
	{
		// Adjust canvas scaler for resolution taller than 16/9 (iPad and other tablets)
		// Reference: https://forum.unity.com/threads/canvas-scaler-issues-with-iphone-x.505711/
		//if (((float)Screen.width / (float)Screen.height) < 1.76f)
		//	GetComponent<CanvasScaler>().matchWidthOrHeight = 0f;
	}

	void Update()
	{
		ManageBackButton();
	}

	void ManageBackButton()
	{
		if (!Input.GetKeyDown(KeyCode.Escape) || _activeWindows.Count == 0)
			return;

		CurrentWindow.Back();
	}

	public void RegisterActiveWindow(UIWindow window)
	{
		if (!_activeWindows.Contains(window))
			_activeWindows.Add(window);

		// Manage window groups (only one window can be displayed at any time for a given window group)
		foreach (var activeWindow in new List<UIWindow>(_activeWindows))
			if (activeWindow != window && activeWindow.UIWindowGroup != null && activeWindow.UIWindowGroup == window.UIWindowGroup)
				activeWindow.Hide();
	}

	public void UnregisterActiveWindow(UIWindow window)
	{
		_activeWindows.Remove(window);
	}

	public bool IsCurrentWindow(UIWindow window)
	{
		return _activeWindows.Count > 0 && _activeWindows.IndexOf(window) == _activeWindows.Count - 1;
	}

	public static bool IsPointerOverUIObject()
	{
		// Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
		// the ray cast appears to require only eventData.position.
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
