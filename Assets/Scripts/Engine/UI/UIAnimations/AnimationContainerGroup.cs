﻿using System.Linq;

namespace Outerminds.UIAnimations
{
	/// <summary>
	/// Play all referenced Animations simultaneously.
	/// </summary>
	public class AnimationContainerGroup : AnimationUnit
	{
		public AnimationUnit[] Animations;

		public bool IsIn { get; private set; }

		public override bool IsPlaying()
		{
			foreach (var animation in Animations)
			{
				if (animation.IsPlaying())
					return true;
			}

			return false;
		}

		public override void Play(bool isIn = true)
		{
			IsIn = isIn;
			foreach (var anim in Animations)
				anim.Play(isIn);
		}

		public override void Stop()
		{
			foreach (var anim in Animations)
				anim.Stop();
		}
	}
}
