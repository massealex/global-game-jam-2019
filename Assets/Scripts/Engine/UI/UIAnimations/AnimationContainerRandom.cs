﻿namespace Outerminds.UIAnimations
{
	/// <summary>
	/// Play one animation at random among all referenced Animations.
	/// </summary>
	public class AnimationContainerRandom : AnimationUnit
	{
		public AnimationUnit[] Animations;

		AnimationUnit _currentlyPlayingAnimation;

		public override bool IsPlaying()
		{
			return (_currentlyPlayingAnimation != null && _currentlyPlayingAnimation.IsPlaying());
		}

		public override void Play(bool isIn = true)
		{
			if (Animations.Length == 0)
				return;

			_currentlyPlayingAnimation = Animations[UnityEngine.Random.Range(0, Animations.Length)];
			_currentlyPlayingAnimation.Play(isIn);
		}

		public override void Stop()
		{
			foreach (var anim in Animations)
				anim.Stop();
		}
	}
}
