﻿namespace Outerminds.UIAnimations
{
	/// <summary>
	/// Play Animations one after the other.
	/// </summary>
	public class AnimationContainerSequence : AnimationUnit
	{
		public AnimationUnit[] Animations;

		int _currentAnimationIndex = -1;
		bool _isIn;
		bool _hasStarted;

		public override bool IsPlaying()
		{
			return (_currentAnimationIndex > -1 && _currentAnimationIndex < Animations.Length && Animations[_currentAnimationIndex].IsPlaying());
		}

		public override void Play(bool isIn = true)
		{
			if (Animations.Length == 0)
				return;

			_hasStarted = true;
			_isIn = isIn;
			_currentAnimationIndex = 0;
			Animations[_currentAnimationIndex].Play(_isIn);
		}

		void Update()
		{
			if (_hasStarted && !IsPlaying())
			{
				_currentAnimationIndex++;
				if (_currentAnimationIndex < Animations.Length)
					Animations[_currentAnimationIndex].Play(_isIn);
				else
					_hasStarted = false;
			}
		}

		public override void Stop()
		{
			foreach (var anim in Animations)
				anim.Stop();

			_hasStarted = false;
			_currentAnimationIndex = -1;
		}
	}
}
