﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public abstract class AnimationUnit : MonoBehaviour
	{
		public abstract void Play(bool isIn = true);

		/// <summary>
		/// Stop animation and reset animated object to default values.
		/// </summary>
		public abstract void Stop();

		public abstract bool IsPlaying();
	}
}
