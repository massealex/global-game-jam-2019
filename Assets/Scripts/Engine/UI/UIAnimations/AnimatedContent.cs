﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimatedContent : MonoBehaviour
	{
		[Header("AnimatedContent")]
		[Tooltip("Animation to be played when the window becomes visible (in transition) and when it becomes not visible (out transition)")]
		public GameObject Content;
		public AnimationUnit TransitionAnimation;

		private bool _isVisible;
		private bool _inited;

		public bool IsVisibleOrAnimating { get { return IsVisible || IsAnimating; } }
		public bool IsAnimating { get { return TransitionAnimation != null && TransitionAnimation.IsPlaying(); } }

		void LateUpdate()
		{
			Content.SetActive(IsVisibleOrAnimating);
		}

		public bool IsVisible
		{
			get { return _isVisible; }
			set
			{
				if (value == IsVisible && _inited)
					return;

				_inited = true;
				_isVisible = value;

				if (value)
				{
					OnShow();
					Content.SetActive(true);

					if (TransitionAnimation != null)
						TransitionAnimation.Play(true);
				}
				else
				{
					OnHide();

					if (TransitionAnimation != null)
						TransitionAnimation.Play(false);
				}
			}
		}

		public virtual void Show() { IsVisible = true; }
		public virtual void Hide() { IsVisible = false; }
		public virtual void Toggle() { IsVisible = !IsVisible; }
		protected virtual void OnShow() { }
		protected virtual void OnHide() { }
	}
}
