﻿using System;
using UnityEngine;
using System.Reflection;

namespace Outerminds.UIAnimations
{
	public class AnimationOnValue : AnimationInOut
	{
		[HideInInspector]
		public TargetValue TargetValue;
		[HideInInspector]
		public ValueAnimationInfo ValueInInfo;
		[HideInInspector]
		public bool UseDifferentValueOutPosition;
		[HideInInspector]
		public ValueAnimationInfo ValueOutInfo;

		object _defaultValue;
		object _startValue;
		object _endValue;

		override public void Init()
		{
			_defaultValue = TargetValue.GetValue();
		}

		public override void OnStart()
		{
			base.OnStart();

			var type = TargetValue.GetTargetValueType();
			if (IsIn || !UseDifferentValueOutPosition)
			{
				_startValue = ValueInInfo.StartValue.UseDefaultValue ? _defaultValue : ValueInInfo.StartValue.GetValue(type);
				_endValue = ValueInInfo.EndValue.UseDefaultValue ? _defaultValue : ValueInInfo.EndValue.GetValue(type);
			}
			else
			{
				_startValue = ValueOutInfo.StartValue.UseDefaultValue ? _defaultValue : ValueOutInfo.StartValue.GetValue(type);
				_endValue = ValueOutInfo.EndValue.UseDefaultValue ? _defaultValue : ValueOutInfo.EndValue.GetValue(type);
			}
		}

		protected override void UpdateAnimation()
		{
			if (_defaultValue is float)
			{
				var start = (float)_startValue;
				var end = (float)_endValue;
				float f = start + AnimationProgress * (end - start);
				TargetValue.SetValue(f);
			}
			else if (_defaultValue is int)
			{
				var start = (int)_startValue;
				var end = (int)_endValue;
				int i = Mathf.FloorToInt(start + AnimationProgress * ((float)end - start));
				TargetValue.SetValue(i);
			}
			else if (_defaultValue is Vector2)
			{
				var start = (Vector2)_startValue;
				var end = (Vector2)_endValue;
				Vector2 vector = new Vector2(
					start.x + AnimationProgress * (end.x - start.x),
					start.y + AnimationProgress * (end.y - start.y));
				TargetValue.SetValue(vector);
			}
			else if (_defaultValue is Vector3)
			{
				var start = (Vector3)_startValue;
				var end = (Vector3)_endValue;
				Vector3 vector = new Vector3(
					start.x + AnimationProgress * (end.x - start.x),
					start.y + AnimationProgress * (end.y - start.y),
					start.z + AnimationProgress * (end.z - start.z));
				TargetValue.SetValue(vector);
			}
			else if (_defaultValue is Rect)
			{
				var start = (Rect)_startValue;
				var end = (Rect)_endValue;
				Rect rect = new Rect(
					start.x + AnimationProgress * (end.x - start.x),
					start.y + AnimationProgress * (end.y - start.y),
					start.width + AnimationProgress * (end.width - start.width),
					start.height + AnimationProgress * (end.height - start.height));
				TargetValue.SetValue(rect);
			}
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			TargetValue.SetValue(_defaultValue);
		}
	}

	[Serializable]
	public class ValueAnimationInfo
	{
		public ValueInfo StartValue = new ValueInfo();
		public ValueInfo EndValue = new ValueInfo();
	}

	[Serializable]
	public class ValueInfo
	{
		public bool UseDefaultValue;

		public float FloatValue;
		public int IntValue;
		public Vector2 Vector2Value;
		public Vector3 Vector3Value;
		public Rect RectValue;

		public object GetValue(Type type)
		{
			if (type == typeof(float))
				return FloatValue;
			else if (type == typeof(int))
				return IntValue;
			else if (type == typeof(Vector2))
				return Vector2Value;
			else if (type == typeof(Vector3))
				return Vector3Value;
			else if (type == typeof(Rect))
				return RectValue;

			return null;
		}
	}

	[Serializable]
	public class TargetValue
	{
		public string EditorDisplayName;
		public Component Component;
		public bool IsProperty;
		public string FieldName;
		public string PropertyName;

		public Type GetTargetValueType()
		{

			if (IsProperty && !string.IsNullOrEmpty(PropertyName))
			{
				PropertyInfo property = Component.GetType().GetProperty(PropertyName);
				if (property != null)
					return property.PropertyType;
			}
			else if(!IsProperty && !string.IsNullOrEmpty(FieldName))
			{
				FieldInfo field = Component.GetType().GetField(FieldName);
				if (field != null)
					return field.FieldType;
			}

			return null;
		}

		public object GetValue()
		{
			if (IsProperty && !string.IsNullOrEmpty(PropertyName))
			{
				PropertyInfo property = Component.GetType().GetProperty(PropertyName);
				if (property != null)
					return property.GetValue(Component);
			}
			else if (!IsProperty && !string.IsNullOrEmpty(FieldName))
			{
				FieldInfo field = Component.GetType().GetField(FieldName);
				if (field != null)
					return field.GetValue(Component);
			}

			return null;
		}

		public void SetValue(object value)
		{
			if (IsProperty && !string.IsNullOrEmpty(PropertyName))
			{
				PropertyInfo property = Component.GetType().GetProperty(PropertyName);
				if (property != null)
					property.SetValue(Component, value);
			}
			else if (!IsProperty && !string.IsNullOrEmpty(FieldName))
			{
				FieldInfo field = Component.GetType().GetField(FieldName);
				if (field != null)
					field.SetValue(Component, value);
			}
		}
	}
}
