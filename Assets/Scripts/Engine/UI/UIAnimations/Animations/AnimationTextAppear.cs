﻿using System.Collections.Generic;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationTextAppear : AnimationPulse
	{
		public enum TextSound { None, CharacterAppear, Interval }

		static char[] _heavyCharacters = { ',', '.', '?', '!' }; // These characters will trigger a longer interval to make a break between sentences
		List<float> _characterProgress = new List<float>();
		List<float> _characterWeights = new List<float>();
		int _invisibleCharacterCount;
		int _lastVisibleCharacterCount;
		float _lastTimeSoundPlayed;

		[HideInInspector]
		public int PunctuationWaitTimeFactor = 10;
		[HideInInspector]
		public TextSound Sound = TextSound.None;
		[HideInInspector]
		public float SpeechSoundInterval = 0.2f;

		public override bool CanEase { get { return false; } }

		public int CharacterCount
		{
			get
			{
				var targetType = (TextTargetTypes)TargetType;
				if (targetType == TextTargetTypes.TextMeshProUI || targetType == TextTargetTypes.TextMeshPro)
				{
					var text = LocalizationManager.Instance.GetString((GetComponent<LocalizedText>().TextID));
					return text.Length;
					//return GetTarget<TMPro.TMP_Text>().text.Length;
				}

				return 0;
			}
		}

		public override void OnStart()
		{
			_lastVisibleCharacterCount = 0;

			var targetType = (TextTargetTypes)TargetType;
			if (targetType == TextTargetTypes.TextMeshProUI || targetType == TextTargetTypes.TextMeshPro)
			{
				var label = GetTarget<TMPro.TMP_Text>();
				label.maxVisibleCharacters = 0;

				int totalWeight = 0;
				
				List<char> heavyCharacters = new List<char>();
				heavyCharacters.AddRange(_heavyCharacters);

				_characterWeights.Clear();
				bool isPartOfTag = false;
				for (int i = 0; i < CharacterCount; i++)
				{
					var text = LocalizationManager.Instance.GetString((GetComponent<LocalizedText>().TextID));
					char character = text[i];
					int weight = heavyCharacters.Contains(character) ? PunctuationWaitTimeFactor : 1;

					// TODO: this will cause a problem if display text has this character
					if (character == '<')
						isPartOfTag = true;

					if (isPartOfTag)
					{
						weight = 0;
						_invisibleCharacterCount++;
					}

					totalWeight += weight;
					_characterWeights.Add(weight);

					if (character == '>')
						isPartOfTag = false;
				}

				_characterProgress = new List<float>();
				float progressPerWeight = 1f / totalWeight;
				float currentProgress = 0;
				for (int i = 0; i < CharacterCount; i++)
				{
					float progress = _characterWeights[i] * progressPerWeight;
					currentProgress += progress;
					_characterProgress.Add(currentProgress);
				}
			}
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();

			var targetType = (TextTargetTypes)TargetType;
			if (targetType == TextTargetTypes.TextMeshProUI || targetType == TextTargetTypes.TextMeshPro)
				GetTarget<TMPro.TMP_Text>().maxVisibleCharacters = CharacterCount;

			//List<float> _characterProgress = new List<float>();
			//List<float> _characterWeights = new List<float>();
			//_invisibleCharacterCount = 0;
			//_lastVisibleCharacterCount = 0;
			//_lastTimeSoundPlayed = 0f;
			//print("WHAT");
		}

		protected override void UpdateAnimation()
		{
			var targetType = (TextTargetTypes)TargetType;
			if (targetType == TextTargetTypes.TextMeshProUI || targetType == TextTargetTypes.TextMeshPro)
			{
				var label = GetTarget<TMPro.TMP_Text>();
				if (AnimationProgress == 0)
				{
					label.maxVisibleCharacters = 0;
					return;
				}
				else if (AnimationProgress == 1)
				{
					label.maxVisibleCharacters = CharacterCount;
					return;
				}

				int currentIndex = 0;
				int invisibleCount = 0;
				for (int i = 0; i < CharacterCount; i++)
				{
					if (_characterWeights[i] == 0)
						invisibleCount++;

					if (_characterProgress[i] > AnimationProgress)
					{
						currentIndex = i + 1;
						break;
					}
				}

				int visibleCharacterCount = currentIndex - invisibleCount;
				label.maxVisibleCharacters = visibleCharacterCount;
				if (Sound == TextSound.CharacterAppear && visibleCharacterCount != _lastVisibleCharacterCount)
					PlayTextSound();

				_lastVisibleCharacterCount = visibleCharacterCount;
			}

			if (Sound == TextSound.Interval && GameTime - _lastTimeSoundPlayed > SpeechSoundInterval)
				PlayTextSound();
		}

		void PlayTextSound()
		{
			//SoundManager.Instance.PlaySound();
			_lastTimeSoundPlayed = GameTime;
		}
	}
}
