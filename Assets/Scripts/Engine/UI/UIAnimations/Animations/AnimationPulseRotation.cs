﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationPulseRotation : AnimationPulse
	{
		[HideInInspector]
		public Vector3 RotationOffset = new Vector3(0, 0, 20f);

		Vector3 _defaultRotation;

		public override void Init()
		{
			_defaultRotation = GetTarget<Transform>().localEulerAngles;
		}

		protected override void UpdateAnimation()
		{
			GetTarget<Transform>().localEulerAngles = new Vector3(GetRotation(_defaultRotation.x, RotationOffset.x), GetRotation(_defaultRotation.y, RotationOffset.y), GetRotation(_defaultRotation.z, RotationOffset.z));
		}

		float GetRotation(float defaultAngle, float angleOffset)
		{
			return defaultAngle + angleOffset * AnimationProgress;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localEulerAngles = _defaultRotation;
		}
	}
}

