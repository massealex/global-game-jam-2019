﻿using UnityEngine;
using UnityEngine.UI;

namespace Outerminds.UIAnimations
{
	public class AnimationPulseAlpha : AnimationPulse
	{
		[HideInInspector]
		public float MinAlpha = 0f;
		[HideInInspector]
		public float MaxAlpha = 1f;

		float _defaultAlpha;

		public override void Init()
		{
			_defaultAlpha = GetAlpha();
		}

		protected override void UpdateAnimation()
		{
			float halfInterval = (MaxAlpha - MinAlpha) / 2f;
			float alpha = MinAlpha + halfInterval + AnimationProgress * (halfInterval);
			SetAlpha(alpha);
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			SetAlpha(_defaultAlpha);
		}

		float GetAlpha()
		{
			var targetType = (AlphaTargetTypes)TargetType;
			if (targetType == AlphaTargetTypes.CanvasGroup)
				return GetTarget<CanvasGroup>().alpha;
			else if (targetType == AlphaTargetTypes.Image)
				return GetTarget<Image>().color.a;
			else if (targetType == AlphaTargetTypes.SpriteRenderer)
				return GetTarget<SpriteRenderer>().color.a;
			else if (targetType == AlphaTargetTypes.TextMeshPro || targetType == AlphaTargetTypes.TextMeshProUI)
				return GetTarget<TMPro.TMP_Text>().color.a;
			else if (targetType == AlphaTargetTypes.Text)
				return GetTarget<Text>().color.a;
			else if (targetType == AlphaTargetTypes.TextMesh)
				return GetTarget<TextMesh>().color.a;

			return 1f;
		}

		void SetAlpha(float alpha)
		{
			var targetType = (AlphaTargetTypes)TargetType;
			if (targetType == AlphaTargetTypes.CanvasGroup)
				(GetTarget<CanvasGroup>()).alpha = alpha;
			else if (targetType == AlphaTargetTypes.Image)
			{
				var image = GetTarget<Image>();
				image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
			}
			else if (targetType == AlphaTargetTypes.SpriteRenderer)
			{
				var spriteRenderer = GetTarget<SpriteRenderer>();
				spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, alpha);
			}
			else if (targetType == AlphaTargetTypes.TextMeshPro || targetType == AlphaTargetTypes.TextMeshProUI)
			{
				var tmpText = GetTarget<TMPro.TMP_Text>();
				tmpText.color = new Color(tmpText.color.r, tmpText.color.g, tmpText.color.b, alpha);
			}
			else if (targetType == AlphaTargetTypes.Text)
			{
				var text = GetTarget<Text>();
				text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
			}
			else if (targetType == AlphaTargetTypes.TextMesh)
			{
				var textMesh = GetTarget<TextMesh>();
				textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, alpha);
			}
		}
	}
}
