﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationShake : AnimationPulse
	{
		[HideInInspector]
		public float ShakeAmplitude = 1f;

		Vector3 _defaultPosition;

		public override bool CanEase { get { return false; } }

		override public void Init()
		{
			_defaultPosition = GetTarget<Transform>().localPosition;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localPosition = _defaultPosition;
		}

		override protected void UpdateAnimation()
		{
			Vector3 position = _defaultPosition;

			if (AnimationProgress > 0 && AnimationProgress < 1)
				position += new Vector3(
					(1f - AnimationProgress) * UnityEngine.Random.Range(-ShakeAmplitude, ShakeAmplitude),
					(1f - AnimationProgress) * UnityEngine.Random.Range(-ShakeAmplitude, ShakeAmplitude));

			GetTarget<Transform>().localPosition = position;
		}
	}
}
