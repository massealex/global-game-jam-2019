﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationPulseScale : AnimationPulse
	{
		[HideInInspector]
		public Vector3 ScaleAmount = new Vector3(0.5f, 0.5f, 0.5f);

		Vector3 _defaultScale;

		public override void Init()
		{
			_defaultScale = GetTarget<Transform>().localScale;
		}

		protected override void UpdateAnimation()
		{
			GetTarget<Transform>().localScale = new Vector3(GetScale(_defaultScale.x, ScaleAmount.x), GetScale(_defaultScale.y, ScaleAmount.y), GetScale(_defaultScale.z, ScaleAmount.z));
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localScale = _defaultScale;
		}

		float GetScale(float baseScale, float scaleAmount)
		{
			return baseScale + scaleAmount * AnimationProgress;
		}
	}
}
