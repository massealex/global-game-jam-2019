﻿using System;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationMorphRectTransform : AnimationInOut
	{
		[HideInInspector]
		public MorphInfo MorphInInfo;
		[HideInInspector]
		public bool UseDifferentMorphOutPosition;
		[HideInInspector]
		public MorphInfo MorphOutInfo;

		RectTransformValues _defaultValues;
		RectTransformValues _currentStartValues;
		RectTransformValues _currentEndValues;

		override public void Init()
		{
			_defaultValues = GetRectTransformValues(GetTarget<RectTransform>());
		}

		public override void OnStart()
		{
			base.OnStart();

			RefreshRectangles(IsIn);
		}

		void RefreshRectangles(bool isIn)
		{
			RectInfo startRect = null;
			RectInfo endRect = null;

			if (isIn || !UseDifferentMorphOutPosition)
			{
				startRect = MorphInInfo.StartRect;
				endRect = MorphInInfo.EndRect;
			}
			else
			{
				startRect = MorphOutInfo.StartRect;
				endRect = MorphOutInfo.EndRect;
			}

			_currentStartValues = GetRectValuesFromRectInfo(startRect);
			_currentEndValues = GetRectValuesFromRectInfo(endRect);
		}

		RectTransformValues GetRectValuesFromRectInfo(RectInfo rectInfo)
		{
			if (rectInfo.Rect == RectInfo.RectType.RectTransform)
				return GetRectTransformValues(rectInfo.TargetRectTransform);
			else
				return _defaultValues;
		}

		protected override void UpdateAnimation()
		{
			SetRectTransformValues(GetTarget<RectTransform>(), _currentStartValues, _currentEndValues, AnimationProgress);
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			SetRectTransformValues(GetTarget<RectTransform>(), _defaultValues, _defaultValues, 0);
		}

		RectTransformValues GetRectTransformValues(RectTransform rectTransform)
		{
			var values = new RectTransformValues();
			values.SizeDelta = rectTransform.sizeDelta;
			values.Position = rectTransform.position;
			values.AnchoredPosition = rectTransform.anchoredPosition;
			values.AnchorMin = rectTransform.anchorMin;
			values.AnchorMax = rectTransform.anchorMax;

			return values;
		}

		void SetRectTransformValues(RectTransform rectTransform, RectTransformValues startValues, RectTransformValues endValues, float progress)
		{
			rectTransform.sizeDelta = startValues.SizeDelta + progress * (endValues.SizeDelta - startValues.SizeDelta);
			rectTransform.position = startValues.Position + progress * (endValues.Position - startValues.Position);
			rectTransform.anchoredPosition = startValues.AnchoredPosition + progress * (endValues.AnchoredPosition - startValues.AnchoredPosition);
			rectTransform.anchorMin = startValues.AnchorMin + progress * (endValues.AnchorMin - startValues.AnchorMin);
			rectTransform.anchorMax = startValues.AnchorMax + progress * (endValues.AnchorMax - startValues.AnchorMax);
		}
	}

	[Serializable]
	public class MorphInfo
	{
		public RectInfo StartRect;
		public RectInfo EndRect;
	}

	[Serializable]
	public class RectInfo
	{
		public enum RectType { RectTransform, DefaultRect }

		public RectType Rect;
		public RectTransform TargetRectTransform;
	}

	[Serializable]
	public class RectTransformValues
	{
		public Vector2 SizeDelta;
		public Vector3 Position;
		public Vector2 AnchoredPosition;
		public Vector2 AnchorMin;
		public Vector2 AnchorMax;
	}
}
