﻿using UnityEngine;

public class AnimationEventSetActive : Outerminds.UIAnimations.AnimationEvent
{
	public enum SetActiveAction { Active, Deactivate, Toggle }

	[HideInInspector]
	public SetActiveAction ActionSetActive;

	public override void ExecuteEvent()
	{
		var targetGameObject = GetTarget<Transform>().gameObject;
		if (ActionSetActive == SetActiveAction.Active)
			targetGameObject.SetActive(true);
		else if (ActionSetActive == SetActiveAction.Deactivate)
			targetGameObject.SetActive(false);
		else if (ActionSetActive == SetActiveAction.Toggle)
			targetGameObject.SetActive(!targetGameObject.activeInHierarchy);
	}
}
