﻿using UnityEngine;
using UnityEngine.UI;

namespace Outerminds.UIAnimations
{
	public class AnimationCurrencyParticle : AnimationPulse
	{
		public Image ImageCurrency;
		[HideInInspector]
		public Vector3 StartPosition;
		[HideInInspector]
		public Vector3 EndPosition;

		[Range(0, 1)]
		public float ExplosionProgressRatio = 0.2f;
		public float ExplosionMinRange;
		public float ExplosionMaxRange;
		public AnimationCurve ExplosionCurve;
		public AnimationCurve DestinationCurve;
		public float MinRotationSpeed;
		public float MaxRotationSpeed;

		Vector3 _explosionPosition;
		float _rotationSpeed;

		public override bool CanEase { get { return false; } }
		public int HeldValue { get; set; }

		public override void OnStart()
		{
			base.OnStart();

			_explosionPosition = StartPosition + (Vector3)UnityEngine.Random.insideUnitCircle.normalized * UnityEngine.Random.Range(ExplosionMinRange, ExplosionMaxRange);
			_rotationSpeed = UnityEngine.Random.Range(MinRotationSpeed, MaxRotationSpeed);
		}

		protected override void UpdateAnimation()
		{
			var target = GetTarget<Transform>();

			if (AnimationProgress < ExplosionProgressRatio)
			{
				// Explosion
				float subRatio = AnimationProgress / ExplosionProgressRatio;
				target.position = StartPosition + ExplosionCurve.Evaluate(subRatio) * (_explosionPosition - StartPosition);
			}
			else
			{
				// Move to destination
				float subRatio = (AnimationProgress - ExplosionProgressRatio) / (1f - ExplosionProgressRatio);
				target.position = _explosionPosition + DestinationCurve.Evaluate(subRatio) * (EndPosition - _explosionPosition);
			}

			ImageCurrency.enabled = AnimationProgress > 0;

			target.Rotate(0, 0, _rotationSpeed * Time.deltaTime);
		}
	}
}
