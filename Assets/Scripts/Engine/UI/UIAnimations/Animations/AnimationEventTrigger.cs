﻿using UnityEngine.Events;

public class AnimationEventTrigger : Outerminds.UIAnimations.AnimationEvent
{
	public UnityEvent Event;

	public override void ExecuteEvent()
	{
		Event.Invoke();
	}
}
