﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationScale : AnimationInOut
	{
		[HideInInspector]
		public Vector3 StartingScale = Vector3.zero;
		[HideInInspector]
		public bool UseDifferentOutScale;
		[HideInInspector]
		public Vector3 ExitScale = Vector3.one;

		Vector3 _endScale;

		public override void Init()
		{
			base.Init();

			_endScale = GetTarget<Transform>().localScale;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localScale = _endScale;
		}

		override protected void UpdateAnimation()
		{
			var startScale = !IsIn && UseDifferentOutScale ? ExitScale : StartingScale;

			GetTarget<Transform>().localScale = startScale + AnimationProgress * (_endScale - startScale);
		}
	}
}
