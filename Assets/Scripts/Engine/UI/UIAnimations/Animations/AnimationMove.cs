﻿using System;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationMove : AnimationInOut
	{
		[HideInInspector]
		public MoveInfo MoveInInfo;
		[HideInInspector]
		public bool UseDifferentMoveOutPosition;
		[HideInInspector]
		public MoveInfo MoveOutInfo;

		Vector3 _defaultLocalPosition;
		Vector3 _defaultPosition;
		Vector3 _currentStartPosition;
		Vector3 _currentEndPosition;
		float _currentLobeHeight;

		override public void Init()
		{
			_defaultLocalPosition = GetTarget<Transform>().localPosition;
			_defaultPosition = GetTarget<Transform>().position;
		}

		public override void OnStart()
		{
			base.OnStart();

			RefreshMovePositions(IsIn);
		}

		void RefreshMovePositions(bool inTransition)
		{
			PositionInfo startPosition;
			PositionInfo endPosition;
			float lobeHeight = 0;

			if (inTransition)
			{
				startPosition = MoveInInfo.StartPosition;
				endPosition = MoveInInfo.EndPosition;
				lobeHeight = MoveInInfo.LobeHeight;
			}
			else
			{
				if (UseDifferentMoveOutPosition)
				{
					startPosition = MoveOutInfo.StartPosition;
					endPosition = MoveOutInfo.EndPosition;
					lobeHeight = MoveOutInfo.LobeHeight;
				}
				else
				{
					startPosition = MoveInInfo.EndPosition;
					endPosition = MoveInInfo.StartPosition;
					lobeHeight = MoveInInfo.LobeHeight;
				}
			}

			_currentLobeHeight = lobeHeight;
			_currentStartPosition = GetLocalPosition(startPosition);
			_currentEndPosition = GetLocalPosition(endPosition);
		}

		Vector3 GetLocalPosition(PositionInfo position)
		{
			if (position.Position == PositionInfo.PositionType.CustomPosition)
				return position.CustomPosition;
			else if (position.Position == PositionInfo.PositionType.Transform)
				return GetTarget<Transform>().InverseTransformPoint(position.TransformPosition.position);
			else 
				return _defaultLocalPosition;
		}

		protected override void UpdateAnimation()
		{
			Vector3 lobe = Vector3.zero;
			if (_currentLobeHeight > 0)
				lobe.y = _currentLobeHeight * AnimationEasing.CalculatePulse(AnimationEasing.PulseType.jump, 1f, AnimationProgress);

			//print(_currentStartPosition + "    " + _currentEndPosition);
			GetTarget<Transform>().localPosition = _currentStartPosition + AnimationProgress * (_currentEndPosition - _currentStartPosition) + lobe;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localPosition = _defaultLocalPosition;
		}
	}

	[Serializable]
	public class MoveInfo
	{
		public PositionInfo StartPosition;
		public PositionInfo EndPosition;
		public float LobeHeight;
	}

	[Serializable]
	public class PositionInfo
	{
		public enum PositionType { Transform, CustomPosition, DefaultPosition }

		public PositionType Position;
		public Transform TransformPosition;
		public Vector3 CustomPosition;
		public bool WorldPosition;
	}
}

