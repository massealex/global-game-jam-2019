﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationEasing : MonoBehaviour
	{

		/// <summary>
		/// The type of easing to use based on Robert Penner's open source easing equations (http://www.robertpenner.com/easing_terms_of_use.html).
		/// </summary>
		public enum EaseType
		{
			linear,
			easeInQuad,
			easeOutQuad,
			easeInOutQuad,
			easeInCubic,
			easeOutCubic,
			easeInOutCubic,
			easeInQuart,
			easeOutQuart,
			easeInOutQuart,
			easeInQuint,
			easeOutQuint,
			easeInOutQuint,
			easeInSine,
			easeOutSine,
			easeInOutSine,
			easeInExpo,
			easeOutExpo,
			easeInOutExpo,
			easeInCirc,
			easeOutCirc,
			easeInOutCirc,
			easeInBounce,
			easeOutBounce,
			easeInOutBounce,
			easeInBack,
			easeOutBack,
			easeInOutBack,
			easeInElastic,
			easeOutElastic,
			easeInOutElastic,
			spring,
			customAnimationCurve = 100
		}

		public enum PulseType
		{
			punch,
			sin,
			cos,
			step,
			jump,
			jumpAndBounce,
			customAnimationCurve = 100
		}

		// Ease types animate a value from 0 to 1
		public static float CalculateEasing(EaseType easeType, float start, float end, float value)
		{
			switch (easeType)
			{
				case EaseType.easeInQuad:
					return easeInQuad(start, end, value);
				case EaseType.easeOutQuad:
					return easeOutQuad(start, end, value);
				case EaseType.easeInOutQuad:
					return easeInOutQuad(start, end, value);
				case EaseType.easeInCubic:
					return easeInCubic(start, end, value);
				case EaseType.easeOutCubic:
					return easeOutCubic(start, end, value);
				case EaseType.easeInOutCubic:
					return easeInOutCubic(start, end, value);
				case EaseType.easeInQuart:
					return easeInQuart(start, end, value);
				case EaseType.easeOutQuart:
					return easeOutQuart(start, end, value);
				case EaseType.easeInOutQuart:
					return easeInOutQuart(start, end, value);
				case EaseType.easeInQuint:
					return easeInQuint(start, end, value);
				case EaseType.easeOutQuint:
					return easeOutQuint(start, end, value);
				case EaseType.easeInOutQuint:
					return easeInOutQuint(start, end, value);
				case EaseType.easeInSine:
					return easeInSine(start, end, value);
				case EaseType.easeOutSine:
					return easeOutSine(start, end, value);
				case EaseType.easeInOutSine:
					return easeInOutSine(start, end, value);
				case EaseType.easeInExpo:
					return easeInExpo(start, end, value);
				case EaseType.easeOutExpo:
					return easeOutExpo(start, end, value);
				case EaseType.easeInOutExpo:
					return easeInOutExpo(start, end, value);
				case EaseType.easeInCirc:
					return easeInCirc(start, end, value);
				case EaseType.easeOutCirc:
					return easeOutCirc(start, end, value);
				case EaseType.easeInOutCirc:
					return easeInOutCirc(start, end, value);
				case EaseType.linear:
					return linear(start, end, value);
				case EaseType.easeInBounce:
					return easeInBounce(start, end, value);
				case EaseType.easeOutBounce:
					return easeOutBounce(start, end, value);
				case EaseType.easeInOutBounce:
					return easeInOutBounce(start, end, value);
				case EaseType.easeInBack:
					return easeInBack(start, end, value);
				case EaseType.easeOutBack:
					return easeOutBack(start, end, value);
				case EaseType.easeInOutBack:
					return easeInOutBack(start, end, value);
				case EaseType.easeInElastic:
					return easeInElastic(start, end, value);
				case EaseType.easeOutElastic:
					return easeOutElastic(start, end, value);
				case EaseType.easeInOutElastic:
					return easeInOutElastic(start, end, value);
				case EaseType.spring:
					return spring(start, end, value);
			}

			return 0;
		}

		// Pulse ease types animate a value from 0 to 0
		public static float CalculatePulse(PulseType punchType, float amplitude, float value)
		{
			switch (punchType)
			{
				case PulseType.punch:
					return punch(amplitude, value);
				case PulseType.sin:
					return sin(amplitude, value);
				case PulseType.cos:
					return cos(amplitude, value);
				case PulseType.step:
					return step(amplitude, value);
				case PulseType.jump:
					return jump(amplitude, value);
				case PulseType.jumpAndBounce:
					return jumpAndBounce(amplitude, value);
			}

			return 0;
		}

		private static float linear(float start, float end, float value)
		{
			return Mathf.Lerp(start, end, value);
		}

		private static float clerp(float start, float end, float value)
		{
			float min = 0.0f;
			float max = 360.0f;
			float half = Mathf.Abs((max - min) * 0.5f);
			float retval = 0.0f;
			float diff = 0.0f;
			if ((end - start) < -half)
			{
				diff = ((max - start) + end) * value;
				retval = start + diff;
			}
			else if ((end - start) > half)
			{
				diff = -((max - end) + start) * value;
				retval = start + diff;
			}
			else retval = start + (end - start) * value;
			return retval;
		}

		private static float easeInQuad(float start, float end, float value)
		{
			end -= start;
			return end * value * value + start;
		}

		private static float easeOutQuad(float start, float end, float value)
		{
			end -= start;
			return -end * value * (value - 2) + start;
		}

		private static float easeInOutQuad(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return end * 0.5f * value * value + start;
			value--;
			return -end * 0.5f * (value * (value - 2) - 1) + start;
		}

		private static float easeInCubic(float start, float end, float value)
		{
			end -= start;
			return end * value * value * value + start;
		}

		private static float easeOutCubic(float start, float end, float value)
		{
			value--;
			end -= start;
			return end * (value * value * value + 1) + start;
		}

		private static float easeInOutCubic(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return end * 0.5f * value * value * value + start;
			value -= 2;
			return end * 0.5f * (value * value * value + 2) + start;
		}

		private static float easeInQuart(float start, float end, float value)
		{
			end -= start;
			return end * value * value * value * value + start;
		}

		private static float easeOutQuart(float start, float end, float value)
		{
			value--;
			end -= start;
			return -end * (value * value * value * value - 1) + start;
		}

		private static float easeInOutQuart(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return end * 0.5f * value * value * value * value + start;
			value -= 2;
			return -end * 0.5f * (value * value * value * value - 2) + start;
		}

		private static float easeInQuint(float start, float end, float value)
		{
			end -= start;
			return end * value * value * value * value * value + start;
		}

		private static float easeOutQuint(float start, float end, float value)
		{
			value--;
			end -= start;
			return end * (value * value * value * value * value + 1) + start;
		}

		private static float easeInOutQuint(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return end * 0.5f * value * value * value * value * value + start;
			value -= 2;
			return end * 0.5f * (value * value * value * value * value + 2) + start;
		}

		private static float easeInSine(float start, float end, float value)
		{
			end -= start;
			return -end * Mathf.Cos(value * (Mathf.PI * 0.5f)) + end + start;
		}

		private static float easeOutSine(float start, float end, float value)
		{
			end -= start;
			return end * Mathf.Sin(value * (Mathf.PI * 0.5f)) + start;
		}

		private static float easeInOutSine(float start, float end, float value)
		{
			end -= start;
			return -end * 0.5f * (Mathf.Cos(Mathf.PI * value) - 1) + start;
		}

		private static float easeInExpo(float start, float end, float value)
		{
			end -= start;
			return end * Mathf.Pow(2, 10 * (value - 1)) + start;
		}

		private static float easeOutExpo(float start, float end, float value)
		{
			end -= start;
			return end * (-Mathf.Pow(2, -10 * value) + 1) + start;
		}

		private static float easeInOutExpo(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return end * 0.5f * Mathf.Pow(2, 10 * (value - 1)) + start;
			value--;
			return end * 0.5f * (-Mathf.Pow(2, -10 * value) + 2) + start;
		}

		private static float easeInCirc(float start, float end, float value)
		{
			end -= start;
			return -end * (Mathf.Sqrt(1 - value * value) - 1) + start;
		}

		private static float easeOutCirc(float start, float end, float value)
		{
			value--;
			end -= start;
			return end * Mathf.Sqrt(1 - value * value) + start;
		}

		private static float easeInOutCirc(float start, float end, float value)
		{
			value /= .5f;
			end -= start;
			if (value < 1) return -end * 0.5f * (Mathf.Sqrt(1 - value * value) - 1) + start;
			value -= 2;
			return end * 0.5f * (Mathf.Sqrt(1 - value * value) + 1) + start;
		}

		private static float easeInBounce(float start, float end, float value)
		{
			end -= start;
			float d = 1f;
			return end - easeOutBounce(0, end, d - value) + start;
		}

		private static float easeOutBounce(float start, float end, float value)
		{
			value /= 1f;
			end -= start;
			if (value < (1 / 2.75f))
			{
				return end * (7.5625f * value * value) + start;
			}
			else if (value < (2 / 2.75f))
			{
				value -= (1.5f / 2.75f);
				return end * (7.5625f * (value) * value + .75f) + start;
			}
			else if (value < (2.5 / 2.75))
			{
				value -= (2.25f / 2.75f);
				return end * (7.5625f * (value) * value + .9375f) + start;
			}
			else
			{
				value -= (2.625f / 2.75f);
				return end * (7.5625f * (value) * value + .984375f) + start;
			}
		}

		private static float easeInOutBounce(float start, float end, float value)
		{
			end -= start;
			float d = 1f;
			if (value < d * 0.5f) return easeInBounce(0, end, value * 2) * 0.5f + start;
			else return easeOutBounce(0, end, value * 2 - d) * 0.5f + end * 0.5f + start;
		}

		private static float easeInBack(float start, float end, float value)
		{
			end -= start;
			value /= 1;
			float s = 1.70158f;
			return end * (value) * value * ((s + 1) * value - s) + start;
		}

		private static float easeOutBack(float start, float end, float value)
		{
			float s = 1.70158f;
			end -= start;
			value = (value) - 1;
			return end * ((value) * value * ((s + 1) * value + s) + 1) + start;
		}

		private static float easeInOutBack(float start, float end, float value)
		{
			float s = 1.70158f;
			end -= start;
			value /= .5f;
			if ((value) < 1)
			{
				s *= (1.525f);
				return end * 0.5f * (value * value * (((s) + 1) * value - s)) + start;
			}
			value -= 2;
			s *= (1.525f);
			return end * 0.5f * ((value) * value * (((s) + 1) * value + s) + 2) + start;
		}

		private static float easeInElastic(float start, float end, float value)
		{
			end -= start;

			float d = 1f;
			float p = d * .3f;
			float s = 0;
			float a = 0;

			if (value == 0) return start;

			if ((value /= d) == 1) return start + end;

			if (a == 0f || a < Mathf.Abs(end))
			{
				a = end;
				s = p / 4;
			}
			else
			{
				s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
			}

			return -(a * Mathf.Pow(2, 10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;
		}

		private static float easeOutElastic(float start, float end, float value)
		{
			end -= start;

			float d = 1f;
			float p = d * .3f;
			float s = 0;
			float a = 0;

			if (value == 0) return start;

			if ((value /= d) == 1) return start + end;

			if (a == 0f || a < Mathf.Abs(end))
			{
				a = end;
				s = p * 0.25f;
			}
			else
			{
				s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
			}

			return (a * Mathf.Pow(2, -10 * value) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) + end + start);
		}

		private static float easeInOutElastic(float start, float end, float value)
		{
			end -= start;

			float d = 1f;
			float p = d * .3f;
			float s = 0;
			float a = 0;

			if (value == 0) return start;

			if ((value /= d * 0.5f) == 2) return start + end;

			if (a == 0f || a < Mathf.Abs(end))
			{
				a = end;
				s = p / 4;
			}
			else
			{
				s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
			}

			if (value < 1) return -0.5f * (a * Mathf.Pow(2, 10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;
			return a * Mathf.Pow(2, -10 * (value -= 1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
		}

		private static float spring(float start, float end, float value)
		{
			value = Mathf.Clamp01(value);
			value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
			return start + (end - start) * value;
		}

		private static float punch(float amplitude, float value)
		{
			if (value == 0 || value == 1)
				return 0;

			float s = 9;
			float period = 1 * 0.3f;
			s = period / (2 * Mathf.PI) * Mathf.Asin(0);
			return (amplitude * Mathf.Pow(2, -10 * value) * Mathf.Sin((value * 1 - s) * (2 * Mathf.PI) / period));
		}

		private static float sin(float amplitude, float value)
		{
			if (value == 0 || value == 1)
				return 0;

			return amplitude * Mathf.Sin(value * (2f * Mathf.PI));
		}

		private static float cos(float amplitude, float value)
		{
			return -1f * sin(amplitude, value);
		}

		private static float step(float amplitude, float value)
		{
			return amplitude * Mathf.Sign(sin(amplitude, value));
		}

		private static float jump(float amplitude, float value)
		{
			return calculateParabola(value, 0f, 1f, amplitude);
		}

		private static float calculateParabola(float currentX, float startX, float endX, float height)
		{
			float progress = (currentX - startX) / (endX - startX);
			progress *= 4f;
			return height * (-0.25f * (progress * progress) + progress);
		}

		private static float jumpAndBounce(float amplitude, float value)
		{
			if (value < 0.58f)
				return calculateParabola(value, 0f, 0.58f, amplitude);
			else if (value < 0.82f)
				return calculateParabola(value, 0.58f, 0.82f, amplitude * 0.37f);
			else
				return calculateParabola(value, 0.82f, 1f, amplitude * 0.2f);
		}
	}
}
