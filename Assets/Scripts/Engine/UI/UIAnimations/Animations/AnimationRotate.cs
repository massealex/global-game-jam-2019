﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationRotate : AnimationInOut
	{
		[HideInInspector]
		public float StartRotationOffset;
		[HideInInspector]
		public bool UseDifferentOutOffset;
		[HideInInspector]
		public float ExitOffset;

		float _endRotation;

		override public void Init()
		{
			_endRotation = GetTarget<Transform>().localEulerAngles.z;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localEulerAngles = new Vector3(0f, 0f, _endRotation);
		}

		protected override void UpdateAnimation()
		{
			float startRotation = !IsIn && UseDifferentOutOffset ? _endRotation + ExitOffset : _endRotation + StartRotationOffset;

			GetTarget<Transform>().localEulerAngles = new Vector3(0, 0, startRotation + AnimationProgress * (_endRotation - startRotation));
		}
	}
}
