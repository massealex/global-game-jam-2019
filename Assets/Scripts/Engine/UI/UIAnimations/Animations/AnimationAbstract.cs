﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public abstract class AnimationAbstract : AnimationUnit
	{
		public enum TimeTypes { Unscaled, Scaled }

		public enum AlphaTargetTypes { CanvasGroup, Image, SpriteRenderer, Text, TextMesh, TextMeshProUI, TextMeshPro }
		public enum ColorTargetTypes { Image, SpriteRenderer, MeshRenderer, Text, TextMesh, TextMeshProUI, TextMeshPro }
		public enum TextureTargetTypes { Image, SpriteRenderer, MeshRenderer }
		public enum TextTargetTypes { Text, TextMesh, TextMeshProUI, TextMeshPro }

		[HideInInspector]
		public int TargetType;
		[HideInInspector]
		public bool UseCustomTarget;
		[HideInInspector]
		public GameObject CustomTarget;
		[HideInInspector]
		public bool Loop;
		[HideInInspector]
		public float LoopDelay;
		[HideInInspector]
		public bool StartOnEnable;
		[HideInInspector]
		public bool StopOnDisable = false;
		[HideInInspector]
		public TimeTypes TimeChannel;

		protected bool _isPlaying;
		protected float _runningTimer;
		protected float _delayTimer;
		protected float _loopDelayTimer;

		bool _cachedTargetSet;
		Component _cachedTarget;

		public bool HasInitiated { get; private set; }
		public virtual float CurrentDuration { get { return 0; } }
		public virtual float Delay { get { return 0; } }
		public virtual bool CanEase { get { return true; } }
		public virtual bool HasDuration { get { return true; } }
		public virtual bool CanStop { get { return true; } }
		public float RuningTimer { get { return _runningTimer; } }
		public float DelayTimer { get { return _delayTimer; } }
		public GameObject TargetObject { get { return UseCustomTarget && CustomTarget != null ? CustomTarget : gameObject; } }
		public bool EditorShowEaseGraph { get; set; }
		public float DeltaTime { get { return TimeChannel == TimeTypes.Unscaled ? Time.unscaledDeltaTime : Time.deltaTime; } }
		public float GameTime { get { return TimeChannel == TimeTypes.Unscaled ? Time.unscaledTime : Time.time; } }

		public T GetTarget<T>() where T : Component
		{
			if (!_cachedTargetSet)
			{
				_cachedTargetSet = Application.isPlaying;
				_cachedTarget = TargetObject.GetComponent<T>();
			}

			return (T)_cachedTarget;
		}

		void Update()
		{
			if (!_isPlaying)
				return;

			if (_delayTimer < Delay)
				_delayTimer += DeltaTime;
			else if (_loopDelayTimer > 0)
				_loopDelayTimer -= DeltaTime;
			else
				_runningTimer += DeltaTime;

			if (_runningTimer > CurrentDuration)
			{
				_runningTimer = CurrentDuration;
				UpdateAnimation();

				if (Loop)
				{
					_loopDelayTimer = LoopDelay;
					_runningTimer = 0;
					OnLoop();
				}
				else
				{
					_isPlaying = false;
					OnEnd();
				}
			}
			else
				UpdateAnimation();
		}

		public virtual void Init() { }
		public virtual void OnStart() { }
		public virtual void OnEnd() { }
		public virtual void OnLoop() { }
		public virtual void ResetToDefaultState() { }

		protected void StartAnimation()
		{
			_runningTimer = 0;
			_delayTimer = 0;
			_isPlaying = true;

			if (!HasInitiated)
			{
				HasInitiated = true;
				Init();
			}

			OnStart();

			UpdateAnimation();
		}

		protected abstract void UpdateAnimation();

		public override void Play(bool isInTransition = true) { }

		public override void Stop()
		{
			_isPlaying = false;

			if (HasInitiated)
				ResetToDefaultState();
		}

		public override bool IsPlaying()
		{
			return _isPlaying && gameObject.activeInHierarchy;
		}

		void OnEnable()
		{
			if (StartOnEnable)
				Play(true);
		}

		void OnDisable()
		{
			if (StopOnDisable)
				Stop();
		}
	}

	public abstract class AnimationPulse : AnimationAbstract
	{
		[HideInInspector]
		public float Duration = 1f;
		[HideInInspector]
		public float StartDelay = 0f;
		[HideInInspector]
		public int EaseType;
		[HideInInspector]
		public AnimationCurve CustomCurve;

		public override float CurrentDuration { get { return Duration; } }
		public override float Delay { get { return StartDelay; } }
		public AnimationEasing.PulseType CurrentEaseType { get { return (AnimationEasing.PulseType)EaseType; } }

		public float AnimationProgress
		{
			get
			{
				float progress = _runningTimer / CurrentDuration;
				if (!CanEase)
					return progress;

				if (CurrentDuration <= 0f)
					progress = 1f;

				if (CurrentEaseType == AnimationEasing.PulseType.customAnimationCurve)
					return CustomCurve.Evaluate(progress);
				else
					return AnimationEasing.CalculatePulse(CurrentEaseType, 1, progress);
			}
		}

		public override void Play(bool isIn = true)
		{
			if (isIn)
				StartAnimation();
		}
	}

	public abstract class AnimationInOut : AnimationAbstract
	{
		[HideInInspector]
		public float DurationIn = 1f;
		[HideInInspector]
		public float StartDelayIn = 0f;
		[HideInInspector]
		public int EaseTypeIn;
		[HideInInspector]
		public AnimationCurve CustomCurveEaseIn;
		[HideInInspector]
		public float DurationOut = 1f;
		[HideInInspector]
		public float StartDelayOut = 0f;
		[HideInInspector]
		public int EaseTypeOut;
		[HideInInspector]
		public AnimationCurve CustomCurveEaseOut;

		override public float CurrentDuration { get { return IsIn ? DurationIn : DurationOut; } }
		public AnimationEasing.EaseType CurrentEaseType { get { return IsIn ? (AnimationEasing.EaseType)EaseTypeIn : (AnimationEasing.EaseType)EaseTypeOut; } }
		public AnimationCurve CurrentCustomEaseCurve { get { return IsIn ? CustomCurveEaseIn : CustomCurveEaseOut; } }
		override public float Delay { get { return IsIn ? StartDelayIn : StartDelayOut; } }
		public bool IsIn { get; private set; }

		public float AnimationProgress
		{
			get
			{
				float progress = _runningTimer / CurrentDuration;
				if (CurrentDuration <= 0f)
					progress = 1f;

				if (!IsIn)
					progress = (1 - progress);

				if (CurrentEaseType == AnimationEasing.EaseType.customAnimationCurve)
					return CurrentCustomEaseCurve.Evaluate(progress);
				else
					return AnimationEasing.CalculateEasing(CurrentEaseType, 0, 1, progress);
			}
		}

		public override void Play(bool isIn = true)
		{
			IsIn = isIn;
			StartAnimation();
		}
	}

	public abstract class AnimationEvent : AnimationAbstract
	{
		[HideInInspector]
		public float StartDelay = 0f;

		public override float Delay { get { return StartDelay; } }
		public override bool CanEase { get { return false; } }
		public override bool HasDuration { get { return false; } }
		public override bool CanStop { get { return false; } }

		public override void Play(bool isIn = true)
		{
			if (isIn)
				StartAnimation();
		}

		public override void OnEnd()
		{
			ExecuteEvent();
		}

		public override void OnLoop()
		{
			ExecuteEvent();
		}

		public abstract void ExecuteEvent();

		protected override void UpdateAnimation() { }
	}
}
