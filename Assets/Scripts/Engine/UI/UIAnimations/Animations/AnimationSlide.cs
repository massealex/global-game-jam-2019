﻿using System;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationSlide : AnimationInOut
	{
		[HideInInspector]
		public SlideInfo SlideInInfo;
		[HideInInspector]
		public bool UseDifferentSlideOutPosition;
		[HideInInspector]
		public SlideInfo SlideOutInfo;

		Vector3 _defaultPosition;
		Vector3 _startPositionIn;
		Vector3 _startPositionOut;
		Vector3 _leftOffscreenPosition;
		Vector3 _topOffscreenPosition;
		Vector3 _rightOffscreenPosition;
		Vector3 _bottomOffscreenPosition;

		override public void Init()
		{
			_defaultPosition = GetTarget<Transform>().localPosition;

			RefreshOffScreenPositions();
		}

		void RefreshOffScreenPositions()
		{
			var targetTransform = GetTarget<Transform>();
			var canvas = targetTransform.GetComponentInParent<Canvas>();
			if (canvas == null)
				return;

			var canvasRectTransform = canvas.GetComponent<RectTransform>();
			var targetRectTransform = targetTransform.GetComponent<RectTransform>();

			Vector3[] canvasCorners = new Vector3[4];
			canvasRectTransform.GetWorldCorners(canvasCorners);
			var canvasRect = new Rect(canvasCorners[0].x, canvasCorners[0].y, canvasCorners[2].x - canvasCorners[1].x, canvasCorners[1].y - canvasCorners[0].y);

			Vector3[] targetCorners = new Vector3[4];
			targetRectTransform.GetWorldCorners(targetCorners);
			var targetRect = new Rect(targetCorners[0].x, targetCorners[0].y, targetCorners[2].x - targetCorners[1].x, targetCorners[1].y - targetCorners[0].y);

			_rightOffscreenPosition = canvas.transform.InverseTransformPoint(targetTransform.position + new Vector3(canvasRect.x + canvasRect.width - targetRect.x, 0));
			_leftOffscreenPosition = canvas.transform.InverseTransformPoint(targetTransform.position + new Vector3(-(targetRect.x - canvasRect.x) - targetRect.width, 0));
			_topOffscreenPosition = canvas.transform.InverseTransformPoint(targetTransform.position + new Vector3(0, canvasRect.y + canvasRect.height - targetRect.y));
			_bottomOffscreenPosition = canvas.transform.InverseTransformPoint(targetTransform.position + new Vector3(0, -(targetRect.y - canvasRect.y) - targetRect.height));
		}

		public override void OnStart()
		{
			base.OnStart();

			RefreshSlidePosition(IsIn);
		}

		void RefreshSlidePosition(bool inTransition)
		{
			Vector3 localPosition = Vector3.zero;

			var slideInfo = (inTransition ? SlideInInfo : SlideOutInfo);
			if (slideInfo.SlideDirection == SlideInfo.SlideType.Custom)
				localPosition = _defaultPosition + slideInfo.Offset;
			else if (slideInfo.SlideDirection == SlideInfo.SlideType.Left)
				localPosition = _leftOffscreenPosition;
			else if (slideInfo.SlideDirection == SlideInfo.SlideType.Right)
				localPosition = _rightOffscreenPosition;
			else if (slideInfo.SlideDirection == SlideInfo.SlideType.Bottom)
				localPosition = _bottomOffscreenPosition;
			else if (slideInfo.SlideDirection == SlideInfo.SlideType.Top)
				localPosition = _topOffscreenPosition;

			if (inTransition)
				_startPositionIn = localPosition;
			else
				_startPositionOut = localPosition;
		}

		protected override void UpdateAnimation()
		{
			Vector3 startPosition = (IsIn || !UseDifferentSlideOutPosition) ? _startPositionIn : _startPositionOut;
			Vector3 endPosition = _defaultPosition;

			var slideInfo = (IsIn ? SlideInInfo : SlideOutInfo);
			if (slideInfo.Invert)
			{
				endPosition = startPosition;
				startPosition = _defaultPosition;
			}

			GetTarget<Transform>().transform.localPosition = startPosition + AnimationProgress * (endPosition - startPosition);
		}

		public void SetDefaultPosition(Vector3 position)
		{
			GetTarget<Transform>().localPosition = position;
			_defaultPosition = position;
			RefreshOffScreenPositions();
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localPosition = _defaultPosition;
		}
	}

	[Serializable]
	public class SlideInfo
	{
		public enum SlideType { Custom, Left, Right, Top, Bottom }

		public SlideType SlideDirection;
		public Vector3 Offset;
		public bool Invert;
	}
}
