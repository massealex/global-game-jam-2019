﻿using UnityEngine;

namespace Outerminds.UIAnimations
{
	public class AnimationPulsePosition : AnimationPulse
	{
		[HideInInspector]
		public Vector3 PulseOffset;

		Vector3 _defaultPosition;

		public override void Init()
		{
			_defaultPosition = GetTarget<Transform>().localPosition;
		}

		protected override void UpdateAnimation()
		{
			GetTarget<Transform>().localPosition = _defaultPosition + AnimationProgress * PulseOffset;
		}

		public override void ResetToDefaultState()
		{
			base.ResetToDefaultState();
			GetTarget<Transform>().localPosition = _defaultPosition;
		}
	}
}
