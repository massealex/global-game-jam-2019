﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationSpriteSequence))]
	public class AnimationSpriteSequenceEditor : AnimationPulseEditor
	{
		List<SpriteListItem> _oldList = new List<SpriteListItem>();
		ReorderableList _spriteList;
		bool _showFrameArray = true;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;

			DrawHeader("Sprite Sequence Animation");

			animation.FrameInterval = EditorGUILayout.FloatField("Frame Interval", animation.FrameInterval);
			DrawSpriteList(animation);
			DropSpriteArea();

			DrawCustomTargetSection();
			animation.TargetType = (int)(AnimationAbstract.TextureTargetTypes)EditorGUILayout.EnumPopup("Target Type", (AnimationAbstract.TextureTargetTypes)animation.TargetType);
			ValidateTextureTarget();
		}

		void DrawSpriteList(AnimationSpriteSequence animation)
		{
			_showFrameArray = EditorGUILayout.Foldout(_showFrameArray, "Frames");
			if (!_showFrameArray)
				return;

			float frameInterval = animation.FrameInterval;
			int spriteCount = EditorGUILayout.IntField("Size", animation.FrameCount);

			if (spriteCount < 0)
				spriteCount = 0;
			while (animation.FrameCount < spriteCount)
				animation.Sprites.Add(animation.FrameCount < _oldList.Count ? _oldList[animation.FrameCount] : new SpriteListItem(null));
			while (animation.FrameCount > spriteCount)
				animation.Sprites.RemoveAt(animation.FrameCount - 1);

			animation.FrameInterval = frameInterval;

			_spriteList.DoLayoutList();
		}

		public void DropSpriteArea()
		{
			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;

			Event e = Event.current;
			switch (e.type)
			{
				case EventType.DragUpdated:
				case EventType.DragPerform:
					DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
					if (e.type == EventType.DragPerform)
					{
						DragAndDrop.AcceptDrag();

						float frameInterval = animation.FrameInterval;
						for (int i = 0; i < DragAndDrop.objectReferences.Length; i++)
							if (DragAndDrop.objectReferences[i] is Texture2D)
								animation.Sprites.Add(new SpriteListItem(AssetDatabase.LoadAssetAtPath<Sprite>(DragAndDrop.paths[i])));
						animation.FrameInterval = frameInterval;
					}
					break;
			}
		}

		protected override void OnEnable()
		{
			base.OnEnable();

			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;

			_oldList.Clear();
			foreach (var item in animation.Sprites)
				_oldList.Add(item);

			_spriteList = new ReorderableList(animation.Sprites, typeof(SpriteListItem), true, true, true, true);

			_spriteList.drawHeaderCallback += DrawSpriteListHeader;
			_spriteList.drawElementCallback += DrawElement;

			_spriteList.onAddCallback += AddItem;
			_spriteList.onRemoveCallback += RemoveItem;
		}

		protected override void OnDisable()
		{
			base.OnDisable();

			_spriteList.drawHeaderCallback -= DrawSpriteListHeader;
			_spriteList.drawElementCallback -= DrawElement;

			_spriteList.onAddCallback -= AddItem;
			_spriteList.onRemoveCallback -= RemoveItem;
		}

		void DrawSpriteListHeader(Rect rect)
		{
			GUI.Label(rect, "Sprite Frames");
		}

		void DrawElement(Rect rect, int index, bool active, bool focused)
		{
			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;
			SpriteListItem item = animation.Sprites[index];

			EditorGUI.BeginChangeCheck();
			item.Sprite = (Sprite)EditorGUI.ObjectField(new Rect(rect.x, rect.y, rect.width, rect.height - 5), "Sprite " + index, item.Sprite, typeof(Sprite), false);
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(target);
			}
		}

		void AddItem(ReorderableList list)
		{
			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;

			float frameInterval = animation.FrameInterval;
			animation.Sprites.Add(new SpriteListItem(null));
			animation.FrameInterval = frameInterval;

			EditorUtility.SetDirty(target);
		}

		void RemoveItem(ReorderableList list)
		{
			AnimationSpriteSequence animation = (AnimationSpriteSequence)target;

			float frameInterval = animation.FrameInterval;
			animation.Sprites.RemoveAt(list.index);
			animation.FrameInterval = frameInterval;

			EditorUtility.SetDirty(target);
		}
	}
}

