﻿using UnityEngine;
using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationPulse), true)]
	public class AnimationPulseEditor : AnimationAbstractEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationPulse animation = (AnimationPulse)target;

			DrawAnimationDetails("Details", ref animation.Duration, ref animation.StartDelay, ref animation.EaseType, ref animation.CustomCurve);
			DrawAnimationSettings(ref animation.Loop, ref animation.LoopDelay, ref animation.StartOnEnable, ref animation.StopOnDisable);
		}
	}
}
