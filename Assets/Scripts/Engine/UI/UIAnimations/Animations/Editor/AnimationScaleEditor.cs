﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationScale))]
	public class AnimationScaleEditor : AnimationInOutEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationScale scaleAnimation = (AnimationScale)target;

			DrawHeader("Scale Animation");

			scaleAnimation.StartingScale = EditorGUILayout.Vector3Field("Start Scale", scaleAnimation.StartingScale);
			scaleAnimation.UseDifferentOutScale = EditorGUILayout.Toggle("Different Out Scale", scaleAnimation.UseDifferentOutScale);
			if (scaleAnimation.UseDifferentOutScale)
				scaleAnimation.ExitScale = EditorGUILayout.Vector3Field("Out Scale", scaleAnimation.ExitScale);

			DrawCustomTargetSection();
		}
	}
}
