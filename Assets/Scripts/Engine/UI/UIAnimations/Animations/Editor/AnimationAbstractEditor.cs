﻿using UnityEngine;
using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationAbstract))]
	public class AnimationAbstractEditor : Editor
	{
		Material _timelineMaterial;
		GUIStyle _timerLabelStyle;
		GUIStyle _startDelayLabelStyle;

		Color green = new Color(39f / 255f, 137f / 255f, 38f / 255f);
		Color darkGreen = new Color(22f / 255f, 71f / 255f, 21f / 255f);
		Color darkGray = new Color(94f / 255f, 94f / 255f, 94f / 255f);
		Color lineColor { get { return ((AnimationAbstract)target).IsPlaying() ? green : Color.black; } }
		Color backgroundColor { get { return ((AnimationAbstract)target).IsPlaying() ? darkGreen : darkGray; } }

		protected virtual void OnEnable()
		{
			var shader = Shader.Find("Hidden/Internal-Colored");
			_timelineMaterial = new Material(shader);

			_timerLabelStyle = new GUIStyle();
			_timerLabelStyle.alignment = TextAnchor.MiddleRight;
			_timerLabelStyle.normal.textColor = Color.white;
			_timerLabelStyle.fontStyle = FontStyle.Bold;
			_timerLabelStyle.padding = new RectOffset(3, 3, 1, 2);

			_startDelayLabelStyle = new GUIStyle();
			_startDelayLabelStyle.alignment = TextAnchor.MiddleLeft;
			_startDelayLabelStyle.normal.textColor = new Color(255f / 255f, 200f / 255f, 200f / 255f);
			_startDelayLabelStyle.fontStyle = FontStyle.Bold;
			_startDelayLabelStyle.padding = new RectOffset(3, 3, 1, 2);

			EditorApplication.update += Update;
		}

		protected virtual void OnDisable()
		{
			DestroyImmediate(_timelineMaterial);

			EditorApplication.update -= Update;
		}

		void Update()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			if (animation != null && animation.IsPlaying())
				Repaint();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}

		protected void DrawHeader(string text)
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField(text, EditorStyles.boldLabel);
		}

		protected void DrawAnimationDetails(string title, ref float duration, ref float startDelay, ref int easing, ref AnimationCurve customCurve, bool displayTimelineCursor = true)
		{
			AnimationAbstract animation = (AnimationAbstract)target;

			DrawHeader(title);

			if (Application.isPlaying)
				DrawTimeline(duration, startDelay, displayTimelineCursor);

			if (animation.HasDuration)
				duration = Mathf.Max(0f, EditorGUILayout.FloatField("Duration", duration));
			startDelay = Mathf.Max(0f, EditorGUILayout.FloatField("Start Delay", startDelay));

			if (animation.CanEase)
			{
				EditorGUILayout.BeginHorizontal();

				bool useCustomCurve;
				if (animation is AnimationInOut)
				{
					easing = (int)(AnimationEasing.EaseType)EditorGUILayout.EnumPopup("Easing", (AnimationEasing.EaseType)easing);
					useCustomCurve = (AnimationEasing.EaseType)easing == AnimationEasing.EaseType.customAnimationCurve;
				}
				else
				{
					useCustomCurve = (AnimationEasing.PulseType)easing == AnimationEasing.PulseType.customAnimationCurve;
					easing = (int)(AnimationEasing.PulseType)EditorGUILayout.EnumPopup("Easing", (AnimationEasing.PulseType)easing);
				}

				if (GUILayout.Button(animation.EditorShowEaseGraph ? "X" : "?", GUILayout.Width(20), GUILayout.Height(14)))
					animation.EditorShowEaseGraph = !animation.EditorShowEaseGraph;

				EditorGUILayout.EndHorizontal();

				if (useCustomCurve)
				{
					if (customCurve == null || customCurve.keys.Length == 0)
						customCurve = AnimationCurve.Linear(0, 0, 1, 1);

					EditorGUI.BeginChangeCheck();
					var curve = EditorGUILayout.CurveField("Easing Curve", customCurve);
					if (EditorGUI.EndChangeCheck())
						customCurve = curve;
				}

				if (animation.EditorShowEaseGraph)
					DrawEaseGraph(duration, startDelay, displayTimelineCursor, easing, customCurve);
			}
		}

		protected void DrawAnimationSettings(ref bool loop, ref float loopDelay, ref bool startOnEnable, ref bool stopOnDisable)
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var textAnchor = EditorStyles.label.alignment;

			if (loop)
			{
				EditorGUILayout.BeginHorizontal();
				loop = EditorGUILayout.Toggle("Loop", loop);

				EditorStyles.label.alignment = TextAnchor.MiddleRight;
				loopDelay = EditorGUILayout.FloatField("Loop Delay ", loopDelay);
				EditorGUILayout.EndHorizontal();
				EditorStyles.label.alignment = textAnchor;
			}
			else
				loop = EditorGUILayout.Toggle("Loop", loop);

			EditorGUILayout.BeginHorizontal();
			startOnEnable = EditorGUILayout.Toggle("Start On Enable", startOnEnable);
			EditorStyles.label.alignment = TextAnchor.MiddleRight;
			if (animation.CanStop)
				stopOnDisable = EditorGUILayout.Toggle("Stop On Disable ", stopOnDisable);
			EditorGUILayout.EndHorizontal();
			EditorStyles.label.alignment = textAnchor;

			animation.TimeChannel = (AnimationAbstract.TimeTypes)EditorGUILayout.EnumPopup("Time Channel", animation.TimeChannel);
		}

		protected void DrawTimeline(float duration, float startDelay, bool displayTimelineCursor)
		{
			AnimationAbstract animation = (AnimationAbstract)target;

			bool isPlaying = animation.IsPlaying() && displayTimelineCursor;

			var lastRect = GUILayoutUtility.GetLastRect();
			float width = lastRect.width;
			float height = 16;
			Rect rect = GUILayoutUtility.GetRect(0, 2000, height, height);



			if (Event.current.type == EventType.Repaint)
			{
				EditorGUI.DrawRect(rect, backgroundColor);
				GUI.BeginClip(rect);
				//GL.PushMatrix();
				GL.Clear(true, false, Color.black);
				_timelineMaterial.SetPass(0);

				int deciSeconds = Mathf.RoundToInt(duration / 0.1f);
				for (int i = 0; i < deciSeconds; i++)
				{
					float x = (i * 0.1f / duration) * width;
					float h = i % 10 == 0 ? height : height / 2f;
					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(x, 0, 0);
					GL.Vertex3(x, h, 0);
					GL.End();
				}

				// Draw border retangle
				{
					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, 0, 0);
					GL.Vertex3(0, height, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(width, 0, 0);
					GL.Vertex3(width, height, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, 0, 0);
					GL.Vertex3(width, 0, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, height, 0);
					GL.Vertex3(width, height, 0);
					GL.End();
				}

				//Draw play cursor
				if (animation.IsPlaying() && displayTimelineCursor)
				{
					float cursorX = width * animation.RuningTimer / animation.CurrentDuration;
					for (int i = -1; i <= 1; i++)
					{
						GL.Begin(GL.LINES);
						GL.Color(Color.red);
						GL.Vertex3(cursorX + i, 0, 0);
						GL.Vertex3(cursorX + i, height - 1, 0);
						GL.End();
					}
				}

				GUI.EndClip();
			}

			// Draw labels
			if (isPlaying)
			{
				lastRect = GUILayoutUtility.GetLastRect();

				if (animation.DelayTimer > 0 && animation.DelayTimer < animation.Delay)
				{
					System.TimeSpan timerDelay = System.TimeSpan.FromSeconds(animation.Delay - animation.DelayTimer);
					string strTimerDelay = string.Format("{0:D1}:{1:D2}", timerDelay.Seconds, timerDelay.Milliseconds.ToString().PadLeft(2, '0').Substring(0, 2));
					EditorGUI.LabelField(lastRect, strTimerDelay, _startDelayLabelStyle);
				}

				if (animation.HasDuration)
				{
					System.TimeSpan timer = System.TimeSpan.FromSeconds(animation.RuningTimer);
					string strTimer = string.Format("{0:D1}:{1:D2}", timer.Seconds, timer.Milliseconds.ToString().PadLeft(2, '0').Substring(0, 2));
					EditorGUI.LabelField(lastRect, strTimer, _timerLabelStyle);
				}
			}
		}

		protected void DrawEaseGraph(float duration, float startDelay, bool displayCursor, int easing, AnimationCurve customCurve)
		{
			AnimationAbstract animation = (AnimationAbstract)target;

			float width = EditorGUIUtility.currentViewWidth - 34f;// Screen.width - 32;
			float height = width;
			Rect rect = GUILayoutUtility.GetRect(0, 2000, height, height);

			if (Event.current.type == EventType.Repaint)
			{
				EditorGUI.DrawRect(rect, backgroundColor);
				GUI.BeginClip(rect);
				//GL.PushMatrix();
				GL.Clear(true, false, Color.black);
				_timelineMaterial.SetPass(0);

				// Draw X axis
				GL.Begin(GL.LINES);
				GL.Color(lineColor);
				GL.Vertex3(width / 2f, 0, 0);
				GL.Vertex3(width / 2f, height, 0);
				GL.End();

				// Draw Y axis
				GL.Begin(GL.LINES);
				GL.Color(lineColor);
				GL.Vertex3(0, height / 2f, 0);
				GL.Vertex3(width, height / 2f, 0);
				GL.End();

				// Draw border retangle
				{
					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, 0, 0);
					GL.Vertex3(0, height, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(width, 0, 0);
					GL.Vertex3(width, height, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, 0, 0);
					GL.Vertex3(width, 0, 0);
					GL.End();

					GL.Begin(GL.LINES);
					GL.Color(lineColor);
					GL.Vertex3(0, height, 0);
					GL.Vertex3(width, height, 0);
					GL.End();
				}

				float demiHeight = height / 2f;
				float demiWidth = width / 2f;
				// Draw curve
				{
					// TODO : work for in, out, simple, animation curve or code easing
					float increment = 0.01f;
					for (float i = 0f; i < 1f - increment; i += increment)
					{
						float startX = i;
						float startY = EvaluateEasing(animation, startX, easing, customCurve);
						float endX = i + increment;
						float endY = EvaluateEasing(animation, endX, easing, customCurve);

						GL.Begin(GL.LINES);
						GL.Color(Color.white);
						GL.Vertex3(demiWidth + demiWidth * startX, height - (demiHeight + startY * demiHeight), 0);
						GL.Vertex3(demiWidth + demiWidth * endX, height - (demiHeight + endY * demiHeight), 0);
						GL.End();
					}
				}

				// Draw play cursor
				if (animation.IsPlaying() && displayCursor)
				{
					int cursorRadius = 3;
					for (int i = -cursorRadius; i <= cursorRadius; i++)
					{
						float x = animation.RuningTimer / animation.CurrentDuration;
						float y = EvaluateEasing(animation, x, easing, customCurve);

						GL.Begin(GL.LINES);
						GL.Color(Color.red);
						GL.Vertex3(i + demiWidth + demiWidth * x, height - (demiHeight + y * demiHeight) + cursorRadius, 0);
						GL.Vertex3(i + demiWidth + demiWidth * x, height - (demiHeight + y * demiHeight) - cursorRadius, 0);
						GL.End();
					}
				}

				GUI.EndClip();
			}
		}

		float EvaluateEasing(AnimationAbstract animation, float progress, int easing, AnimationCurve curve)
		{
			if ((animation is AnimationInOut && easing == (int)AnimationEasing.EaseType.customAnimationCurve) ||
				(animation is AnimationPulse && easing == (int)AnimationEasing.PulseType.customAnimationCurve))
			{
				return curve.Evaluate(progress);
			}
			else if (animation is AnimationInOut)
				return AnimationEasing.CalculateEasing((AnimationEasing.EaseType)easing, 0f, 1f, progress);
			else if (animation is AnimationPulse)
				return AnimationEasing.CalculatePulse((AnimationEasing.PulseType)easing, 1f, progress);

			return 0f;
		}

		protected bool DrawCustomTargetSection()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var lastUseCustomTarget = animation.UseCustomTarget;
			var lastCustomTarget = animation.CustomTarget;

			DrawHeader("Target");

			EditorGUILayout.BeginHorizontal();
			animation.UseCustomTarget = EditorGUILayout.Toggle("Custom Target", animation.UseCustomTarget);
			if (animation.UseCustomTarget)
				animation.CustomTarget = (GameObject)EditorGUILayout.ObjectField(animation.CustomTarget, typeof(GameObject), true);
			else
				GUILayout.Label("(This game object by default)");
			EditorGUILayout.EndHorizontal();

			return (lastUseCustomTarget != animation.UseCustomTarget || lastCustomTarget != animation.CustomTarget);
		}

		void DisplayValidateTargetWarning(string type)
		{
			EditorGUILayout.HelpBox("Missing " + type + " component on Target.", MessageType.Error, true);
		}

		protected void ValidateAlphaTarget()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var targetType = (AnimationAbstract.AlphaTargetTypes)animation.TargetType;
			if ((targetType == AnimationAbstract.AlphaTargetTypes.CanvasGroup && animation.GetTarget<CanvasGroup>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.Image && animation.GetTarget<UnityEngine.UI.Image>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.SpriteRenderer && animation.GetTarget<SpriteRenderer>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.Text && animation.GetTarget<UnityEngine.UI.Text>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.TextMesh && animation.GetTarget<TextMesh>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.TextMeshPro && animation.GetTarget<TMPro.TextMeshPro>() == null) ||
				(targetType == AnimationAbstract.AlphaTargetTypes.TextMeshProUI && animation.GetTarget<TMPro.TextMeshProUGUI>() == null))
			{
				if (animation.GetTarget<CanvasGroup>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.CanvasGroup;
				else if (animation.GetTarget<UnityEngine.UI.Image>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.Image;
				else if (animation.GetTarget<SpriteRenderer>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.SpriteRenderer;
				else if (animation.GetTarget<UnityEngine.UI.Text>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.Text;
				else if (animation.GetTarget<TextMesh>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.TextMesh;
				else if (animation.GetTarget<TMPro.TextMeshPro>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.TextMeshPro;
				else if (animation.GetTarget<TMPro.TextMeshProUGUI>() != null)
					animation.TargetType = (int)AnimationAbstract.AlphaTargetTypes.TextMeshProUI;
				else
					DisplayValidateTargetWarning(targetType.ToString());
			}
		}

		protected void ValidateTextureTarget()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var targetType = (AnimationAbstract.TextureTargetTypes)animation.TargetType;
			if ((targetType == AnimationAbstract.TextureTargetTypes.Image && animation.GetTarget<UnityEngine.UI.Image>() == null) ||
				(targetType == AnimationAbstract.TextureTargetTypes.SpriteRenderer && animation.GetTarget<SpriteRenderer>() == null) ||
				(targetType == AnimationAbstract.TextureTargetTypes.MeshRenderer && animation.GetTarget<MeshRenderer>() == null))
			{
				if (animation.GetTarget<UnityEngine.UI.Image>() != null)
					animation.TargetType = (int)AnimationAbstract.TextureTargetTypes.Image;
				else if (animation.GetTarget<SpriteRenderer>() != null)
					animation.TargetType = (int)AnimationAbstract.TextureTargetTypes.SpriteRenderer;
				else if (animation.GetTarget<MeshRenderer>() != null)
					animation.TargetType = (int)AnimationAbstract.TextureTargetTypes.MeshRenderer;
				else
					DisplayValidateTargetWarning(targetType.ToString());
			}
		}

		protected void ValidateColorTarget()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var targetType = (AnimationAbstract.ColorTargetTypes)animation.TargetType;
			if ((targetType == AnimationAbstract.ColorTargetTypes.Image && animation.GetTarget<UnityEngine.UI.Image>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.SpriteRenderer && animation.GetTarget<SpriteRenderer>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.MeshRenderer && animation.GetTarget<MeshRenderer>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.Text && animation.GetTarget<UnityEngine.UI.Text>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.TextMesh && animation.GetTarget<TextMesh>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.TextMeshPro && animation.GetTarget<TMPro.TextMeshPro>() == null) ||
				(targetType == AnimationAbstract.ColorTargetTypes.TextMeshProUI && animation.GetTarget<TMPro.TextMeshProUGUI>() == null))
			{

				if (animation.GetTarget<UnityEngine.UI.Image>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.Image;
				else if (animation.GetTarget<SpriteRenderer>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.SpriteRenderer;
				else if (animation.GetTarget<MeshRenderer>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.MeshRenderer;
				else if (animation.GetTarget<UnityEngine.UI.Text>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.Text;
				else if (animation.GetTarget<TextMesh>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.TextMesh;
				else if (animation.GetTarget<TMPro.TextMeshPro>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.TextMeshPro;
				else if (animation.GetTarget<TMPro.TextMeshProUGUI>() != null)
					animation.TargetType = (int)AnimationAbstract.ColorTargetTypes.TextMeshProUI;
				else
					DisplayValidateTargetWarning(targetType.ToString());
			}
		}

		protected void ValidateTextTarget()
		{
			AnimationAbstract animation = (AnimationAbstract)target;
			var targetType = (AnimationAbstract.TextTargetTypes)animation.TargetType;
			if ((targetType == AnimationAbstract.TextTargetTypes.Text && animation.GetTarget<UnityEngine.UI.Text>() == null) ||
				(targetType == AnimationAbstract.TextTargetTypes.TextMesh && animation.GetTarget<TextMesh>() == null) ||
				(targetType == AnimationAbstract.TextTargetTypes.TextMeshPro && animation.GetTarget<TMPro.TextMeshPro>() == null) ||
				(targetType == AnimationAbstract.TextTargetTypes.TextMeshProUI && animation.GetTarget<TMPro.TextMeshProUGUI>() == null))
			{
				if (animation.GetTarget<UnityEngine.UI.Text>() != null)
					animation.TargetType = (int)AnimationAbstract.TextTargetTypes.Text;
				else if (animation.GetTarget<TextMesh>() != null)
					animation.TargetType = (int)AnimationAbstract.TextTargetTypes.TextMesh;
				else if (animation.GetTarget<TMPro.TextMeshPro>() != null)
					animation.TargetType = (int)AnimationAbstract.TextTargetTypes.TextMeshPro;
				else if (animation.GetTarget<TMPro.TextMeshProUGUI>() != null)
					animation.TargetType = (int)AnimationAbstract.TextTargetTypes.TextMeshProUI;
				else
					DisplayValidateTargetWarning(targetType.ToString());
			}
		}

		protected void SaveChanges(Object target, GameObject targetGameObject)
		{
			if (GUI.changed && !Application.isPlaying)
			{
				EditorUtility.SetDirty(target);
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
			}
		}
	}
}
