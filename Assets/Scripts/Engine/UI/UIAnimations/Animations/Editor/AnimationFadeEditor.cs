﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationFade))]
	public class AnimationFadeEditor : AnimationInOutEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationFade fadeAnimation = (AnimationFade)target;

			DrawHeader("Fade Animation");

			fadeAnimation.FromAlpha = EditorGUILayout.Slider("Start Alpha", fadeAnimation.FromAlpha, 0f, 1f);
			fadeAnimation.ToAlpha = EditorGUILayout.Slider("End Alpha", fadeAnimation.ToAlpha, 0f, 1f);
			fadeAnimation.UseDifferentOutAlpha = EditorGUILayout.Toggle("Different Out Alpha", fadeAnimation.UseDifferentOutAlpha);
			if (fadeAnimation.UseDifferentOutAlpha)
				fadeAnimation.ExitAlpha = EditorGUILayout.Slider("Out Alpha", fadeAnimation.ExitAlpha, 0f, 1f);

			DrawCustomTargetSection();
			fadeAnimation.TargetType = (int)(AnimationAbstract.AlphaTargetTypes)EditorGUILayout.EnumPopup("Target Type", (AnimationAbstract.AlphaTargetTypes)fadeAnimation.TargetType);
			ValidateAlphaTarget();
		}
	}
}
