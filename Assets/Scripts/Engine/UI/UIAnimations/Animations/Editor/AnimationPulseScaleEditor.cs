﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationPulseScale))]
	public class AnimationPulseScaleEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationPulseScale animation = (AnimationPulseScale)target;

			DrawHeader("Pulse Scale Animation");

			animation.ScaleAmount = EditorGUILayout.Vector3Field("Scale Increment Amount", animation.ScaleAmount);

			DrawCustomTargetSection();
		}
	}
}
