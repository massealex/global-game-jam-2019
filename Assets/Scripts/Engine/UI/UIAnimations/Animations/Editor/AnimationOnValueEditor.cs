﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationOnValue))]
	public class AnimationOnValueEditor : AnimationInOutEditor
	{
		bool showInInfo = true;
		bool showOutInfo = true;
		TargetValue[] _values = new TargetValue[0];

		protected override void OnEnable()
		{
			base.OnEnable();

			LoadFieldNames();
		}

		void LoadFieldNames()
		{
			AnimationOnValue valueAnimation = (AnimationOnValue)target;
			var targetGameObject = valueAnimation.TargetObject;

			List<TargetValue> values = new List<TargetValue>();
			foreach (var component in targetGameObject.GetComponents<Component>())
			{
				if (component.GetType() == valueAnimation.GetType())
					continue;

				var strType = component.GetType().ToString();
				strType = strType.Substring(strType.LastIndexOf(".") + 1);

				FieldInfo[] fields = component.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
				foreach (var field in fields)
				{
					var strFieldType = field.FieldType.ToString();
					strFieldType = strFieldType.Substring(strFieldType.LastIndexOf(".") + 1);
					var path = strType + "/" + field.Name + " (" + strFieldType + ")";
					values.Add(new TargetValue { EditorDisplayName = path, Component = component, FieldName = field.Name });
				}

				PropertyInfo[] properties = component.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
				foreach (var property in properties)
				{
					if (!property.CanWrite || !property.CanRead)
						continue;

					// Get and set must be public
					MethodInfo getMethod = property.GetGetMethod(false);
					MethodInfo setMethod = property.GetSetMethod(false);
					if (getMethod == null || setMethod == null)
						continue;

					var strPropertyType = property.PropertyType.ToString();
					strPropertyType = strPropertyType.Substring(strPropertyType.LastIndexOf(".") + 1);
					var path = strType + "/" + property.Name + " (" + strPropertyType + ")";
					values.Add(new TargetValue { EditorDisplayName = path, Component = component, PropertyName = property.Name, IsProperty = true });
				}
			}

			_values = values.ToArray();
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationOnValue valueAnimation = (AnimationOnValue)target;

			DrawHeader("Value Animation");


			EditorGUI.BeginChangeCheck();
			int valueIndex = -1;
			for (int i = 0; i < _values.Length; i++)
			{
				var v = _values[i];
				if (v.Component == valueAnimation.TargetValue.Component && (
						(!string.IsNullOrEmpty(valueAnimation.TargetValue.FieldName) && v.FieldName == valueAnimation.TargetValue.FieldName) ||
						(!string.IsNullOrEmpty(valueAnimation.TargetValue.PropertyName) && v.PropertyName == valueAnimation.TargetValue.PropertyName)
					))
				{
					valueIndex = i;
					break;
				}
			}

			string[] paths = new string[_values.Length];
			for (int i = 0; i < _values.Length; i++)
				paths[i] = _values[i].EditorDisplayName;
			valueIndex = EditorGUILayout.Popup(valueIndex, paths);
			if (EditorGUI.EndChangeCheck() && valueIndex > -1 && valueIndex < _values.Length)
			{
				valueAnimation.TargetValue = _values[valueIndex];
				if (!Application.isPlaying)
					EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
			}


			if (!string.IsNullOrEmpty(valueAnimation.TargetValue.FieldName) || !string.IsNullOrEmpty(valueAnimation.TargetValue.PropertyName))
			{
				showInInfo = EditorGUILayout.Foldout(showInInfo, "Value In Info");
				if (showInInfo)
					DrawAnimationValueInfo(valueAnimation.ValueInInfo);

				valueAnimation.UseDifferentValueOutPosition = EditorGUILayout.Toggle("Different Value Out Position", valueAnimation.UseDifferentValueOutPosition);
				if (valueAnimation.UseDifferentValueOutPosition)
				{
					showOutInfo = EditorGUILayout.Foldout(showOutInfo, "Value Out Info");
					if (showOutInfo)
						DrawAnimationValueInfo(valueAnimation.ValueOutInfo);
				}
			}

			bool customTargetChanged = DrawCustomTargetSection();
			LoadFieldNames();
		}

		void DrawAnimationValueInfo(ValueAnimationInfo valueAnimationInfo)
		{
			EditorGUI.indentLevel++;

			DrawValueInfo(valueAnimationInfo.StartValue, "Start Value");
			DrawValueInfo(valueAnimationInfo.EndValue, "End Value");

			EditorGUI.indentLevel--;
		}

		void DrawValueInfo(ValueInfo valueInfo, string label)
		{
			AnimationOnValue valueAnimation = (AnimationOnValue)target;

			EditorGUI.BeginChangeCheck();

			valueInfo.UseDefaultValue = EditorGUILayout.Toggle(label + ": Use Default", valueInfo.UseDefaultValue);
			if (!valueInfo.UseDefaultValue)
			{
				EditorGUI.indentLevel++;
				var t = valueAnimation.TargetValue.GetTargetValueType();
				if (t == typeof(float))
					valueInfo.FloatValue = EditorGUILayout.FloatField(valueInfo.FloatValue);
				else if (t == typeof(int))
					valueInfo.IntValue = EditorGUILayout.IntField(valueInfo.IntValue);
				else if (t == typeof(Vector2))
					valueInfo.Vector2Value = EditorGUILayout.Vector2Field("", valueInfo.Vector2Value);
				else if (t == typeof(Vector3))
					valueInfo.Vector3Value = EditorGUILayout.Vector3Field("", valueInfo.Vector3Value);
				else if (t == typeof(Rect))
					valueInfo.RectValue = EditorGUILayout.RectField(valueInfo.RectValue);
				EditorGUI.indentLevel--;
			}

			if (EditorGUI.EndChangeCheck() && !Application.isPlaying)
				EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
		}
	}
}
