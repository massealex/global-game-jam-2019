﻿using UnityEngine;
using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationEvent), true)]
	public class AnimationEventEditor : AnimationAbstractEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationEvent animation = (AnimationEvent)target;

			animation.StartDelay = Mathf.Max(0f, EditorGUILayout.FloatField("Start Delay", animation.StartDelay));
			if (Application.isPlaying)
				DrawTimeline(0, animation.StartDelay, true);

			DrawAnimationSettings(ref animation.Loop, ref animation.LoopDelay, ref animation.StartOnEnable, ref animation.StopOnDisable);
		}
	}
}

