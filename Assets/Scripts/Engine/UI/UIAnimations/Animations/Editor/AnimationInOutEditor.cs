﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationInOut))]
	public class AnimationInOutEditor : AnimationAbstractEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationInOut transitionAnimation = (AnimationInOut)target;

			DrawAnimationDetails("In", ref transitionAnimation.DurationIn, ref transitionAnimation.StartDelayIn, ref transitionAnimation.EaseTypeIn, ref transitionAnimation.CustomCurveEaseIn, transitionAnimation.IsIn);
			DrawAnimationDetails("Out", ref transitionAnimation.DurationOut, ref transitionAnimation.StartDelayOut, ref transitionAnimation.EaseTypeOut, ref transitionAnimation.CustomCurveEaseOut, !transitionAnimation.IsIn);

			DrawHeader("Settings");
			DrawAnimationSettings(ref transitionAnimation.Loop, ref transitionAnimation.LoopDelay, ref transitionAnimation.StartOnEnable, ref transitionAnimation.StopOnDisable);

			SaveChanges(transitionAnimation, transitionAnimation.gameObject);
		}
	}
}
