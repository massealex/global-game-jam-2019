﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationRotate))]
	public class AnimationRotateEditor : AnimationInOutEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationRotate rotateAnimation = (AnimationRotate)target;

			DrawHeader("Rotate Animation");

			rotateAnimation.StartRotationOffset = EditorGUILayout.FloatField("Start Rotation Offset", rotateAnimation.StartRotationOffset);
			rotateAnimation.UseDifferentOutOffset = EditorGUILayout.Toggle("Different Out Rotation", rotateAnimation.UseDifferentOutOffset);
			if (rotateAnimation.UseDifferentOutOffset)
				rotateAnimation.ExitOffset = EditorGUILayout.FloatField("Out Rotation Offset", rotateAnimation.ExitOffset);

			DrawCustomTargetSection();
		}
	}
}
