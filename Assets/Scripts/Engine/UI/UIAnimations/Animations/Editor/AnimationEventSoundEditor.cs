﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationEventSound), true)]
	public class AnimationEventSoundEditor : AnimationEventEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationEventSound animationSound = (AnimationEventSound)target;

			DrawHeader("Audio Clip");
			EditorGUI.BeginChangeCheck();
			animationSound.AudioClip = (AudioClip)EditorGUILayout.ObjectField("Audio Clip", animationSound.AudioClip, typeof(AudioClip), false);
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(animationSound);
				EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
			}
		}
	}
}

