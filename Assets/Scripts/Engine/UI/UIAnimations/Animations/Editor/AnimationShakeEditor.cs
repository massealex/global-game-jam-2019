﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationShake))]
	public class AnimationShakeEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationShake shakeAnimation = (AnimationShake)target;

			DrawHeader("Shake Animation");

			shakeAnimation.ShakeAmplitude = EditorGUILayout.FloatField("Shake Amplitude", shakeAnimation.ShakeAmplitude);

			DrawCustomTargetSection();
		}
	}
}
