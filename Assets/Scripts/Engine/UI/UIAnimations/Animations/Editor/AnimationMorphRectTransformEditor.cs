﻿using UnityEditor;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationMorphRectTransform))]
	public class AnimationMorphRectTransformEditor : AnimationInOutEditor
	{
		bool showInInfo = true;
		bool showOutInfo = true;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationMorphRectTransform morphAnimation = (AnimationMorphRectTransform)target;

			DrawHeader("Morph Animation");

			showInInfo = EditorGUILayout.Foldout(showInInfo, "Morph In Info");
			if (showInInfo)
				DrawMorphInfo(morphAnimation.MorphInInfo);

			morphAnimation.UseDifferentMorphOutPosition = EditorGUILayout.Toggle("Different Morph Out Position", morphAnimation.UseDifferentMorphOutPosition);
			if (morphAnimation.UseDifferentMorphOutPosition)
			{
				showOutInfo = EditorGUILayout.Foldout(showOutInfo, "Morph Out Info");
				if (showOutInfo)
					DrawMorphInfo(morphAnimation.MorphOutInfo);
			}

			DrawCustomTargetSection();
		}

		void DrawMorphInfo(MorphInfo morphInfo)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			DrawRectInfo(morphInfo.StartRect, "Start Rect");
			DrawRectInfo(morphInfo.EndRect, "End Rect");
			EditorGUILayout.EndVertical();
		}

		void DrawRectInfo(RectInfo rectInfo, string label)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);

			rectInfo.Rect = (RectInfo.RectType)EditorGUILayout.EnumPopup(label, rectInfo.Rect);

			EditorGUI.indentLevel++;
			if (rectInfo.Rect == RectInfo.RectType.RectTransform)
				rectInfo.TargetRectTransform = (RectTransform)EditorGUILayout.ObjectField("Target RectTransform", rectInfo.TargetRectTransform, typeof(RectTransform), true);
			EditorGUI.indentLevel--;

			EditorGUILayout.EndVertical();
		}
	}
}


