﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationSlide))]
	public class AnimationSlideEditor : AnimationInOutEditor
	{
		bool showInInfo = true;
		bool showOutInfo = true;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationSlide slideAnimation = (AnimationSlide)target;

			DrawHeader("Slide Animation");

			showInInfo = EditorGUILayout.Foldout(showInInfo, "Slide In Info");
			if (showInInfo)
				DrawSlideInfo(slideAnimation.SlideInInfo, "Start Offset");

			slideAnimation.UseDifferentSlideOutPosition = EditorGUILayout.Toggle("Different Slide Out Position", slideAnimation.UseDifferentSlideOutPosition);
			if (slideAnimation.UseDifferentSlideOutPosition)
			{
				showOutInfo = EditorGUILayout.Foldout(showOutInfo, "Slide Out Info");
				if (showOutInfo)
					DrawSlideInfo(slideAnimation.SlideOutInfo, "End Offset");
			}

			DrawCustomTargetSection();
		}

		void DrawSlideInfo(SlideInfo slideInfo, string labelOffset)
		{
			EditorGUI.indentLevel++;
			slideInfo.SlideDirection = (SlideInfo.SlideType)EditorGUILayout.EnumPopup("Slide Direction", slideInfo.SlideDirection);
			if (slideInfo.SlideDirection == SlideInfo.SlideType.Custom)
				slideInfo.Offset = EditorGUILayout.Vector3Field(labelOffset, slideInfo.Offset);
			slideInfo.Invert = EditorGUILayout.Toggle("Invert Direction", slideInfo.Invert);
			EditorGUI.indentLevel--;
		}
	}
}
