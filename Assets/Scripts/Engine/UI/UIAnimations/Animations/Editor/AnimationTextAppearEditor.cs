﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationTextAppear))]
	public class AnimationTextAppearEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationTextAppear textAppearAnimation = (AnimationTextAppear)target;

			DrawHeader("Text Appear Animation");
			textAppearAnimation.PunctuationWaitTimeFactor = EditorGUILayout.IntSlider("Punctuation Wait Factor", textAppearAnimation.PunctuationWaitTimeFactor, 1, 50);
			textAppearAnimation.Sound = (AnimationTextAppear.TextSound)EditorGUILayout.EnumPopup("Text Sound", textAppearAnimation.Sound);

			if (textAppearAnimation.Sound == AnimationTextAppear.TextSound.Interval)
				textAppearAnimation.SpeechSoundInterval = EditorGUILayout.FloatField("Speech Sound Interval", textAppearAnimation.SpeechSoundInterval);

			DrawCustomTargetSection();
			textAppearAnimation.TargetType = (int)(AnimationAbstract.TextTargetTypes)EditorGUILayout.EnumPopup("Target Type", (AnimationAbstract.TextTargetTypes)textAppearAnimation.TargetType);
			ValidateTextTarget();
		}
	}
}

