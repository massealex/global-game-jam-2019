﻿using System;
using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationEventSetActive), true)]
	public class AnimationEventSetActiveEditor : AnimationEventEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationEventSetActive animationSetActive = (AnimationEventSetActive)target;

			DrawHeader("Set Is Active");
			animationSetActive.ActionSetActive = (AnimationEventSetActive.SetActiveAction)EditorGUILayout.EnumPopup("Set Active Action", animationSetActive.ActionSetActive);

			DrawCustomTargetSection();
		}
	}
}

