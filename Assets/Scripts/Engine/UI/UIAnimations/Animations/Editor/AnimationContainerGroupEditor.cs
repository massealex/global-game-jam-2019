﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationContainerGroup))]
	public class AnimationContainerGroupEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationContainerGroup groupAnimation = (AnimationContainerGroup)target;

			EditorGUILayout.Space();

			if (GUILayout.Button("Auto Fill With Child Animations", GUILayout.Height(35)))
			{
				List<AnimationAbstract> animations = new List<AnimationAbstract>();
				FindAnimations(groupAnimation.transform, animations);
				groupAnimation.Animations = animations.ToArray();

				EditorUtility.SetDirty(groupAnimation);
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(groupAnimation.gameObject.scene);
			}
		}

		void FindAnimations(Transform currentSubject, List<AnimationAbstract> animations)
		{
			foreach (var anim in currentSubject.gameObject.GetComponents<AnimationAbstract>())
				animations.Add(anim);

			foreach (Transform child in currentSubject)
				if (child.GetComponent<AnimationContainerGroup>() == null)
					FindAnimations(child, animations);
		}
	}
}

