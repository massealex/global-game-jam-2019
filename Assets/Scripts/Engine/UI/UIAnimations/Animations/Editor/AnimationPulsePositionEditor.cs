﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationPulsePosition))]
	public class AnimationPulsePositionEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationPulsePosition animation = (AnimationPulsePosition)target;

			DrawHeader("Pulse Position Animation");

			animation.PulseOffset = EditorGUILayout.Vector3Field("Pulse Offset", animation.PulseOffset);

			DrawCustomTargetSection();
		}
	}
}
