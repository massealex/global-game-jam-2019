﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationPulseAlpha))]
	public class AnimationPulseAlphaEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationPulseAlpha animation = (AnimationPulseAlpha)target;

			DrawHeader("Pulse Alpha Animation");

			animation.MinAlpha = EditorGUILayout.Slider("Min Alpha", animation.MinAlpha, 0, 1);
			animation.MaxAlpha = EditorGUILayout.Slider("Max Alpha", animation.MaxAlpha, 0, 1);

			DrawCustomTargetSection();
			animation.TargetType = (int)(AnimationAbstract.AlphaTargetTypes)EditorGUILayout.EnumPopup("Target Type", (AnimationAbstract.AlphaTargetTypes)animation.TargetType);
			ValidateAlphaTarget();
		}
	}
}
