﻿using UnityEditor;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationPulseRotation))]
	public class AnimationPulseRotationEditor : AnimationPulseEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationPulseRotation animation = (AnimationPulseRotation)target;

			DrawHeader("Pulse Rotation Animation");

			animation.RotationOffset = EditorGUILayout.Vector3Field("Rotation Offset", animation.RotationOffset);

			DrawCustomTargetSection();
		}
	}
}

