﻿using UnityEditor;
using UnityEngine;

namespace Outerminds.UIAnimations
{
	[CustomEditor(typeof(AnimationMove))]
	public class AnimationMoveEditor : AnimationInOutEditor
	{
		bool showInInfo = true;
		bool showOutInfo = true;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			AnimationMove moveAnimation = (AnimationMove)target;

			DrawHeader("Move Animation");

			showInInfo = EditorGUILayout.Foldout(showInInfo, "Move In Info");
			if (showInInfo)
				DrawMoveInfo(moveAnimation.MoveInInfo);

			moveAnimation.UseDifferentMoveOutPosition = EditorGUILayout.Toggle("Different Move Out Position", moveAnimation.UseDifferentMoveOutPosition);
			if (moveAnimation.UseDifferentMoveOutPosition)
			{
				showOutInfo = EditorGUILayout.Foldout(showOutInfo, "Move Out Info");
				if (showOutInfo)
					DrawMoveInfo(moveAnimation.MoveOutInfo);
			}

			DrawCustomTargetSection();
		}

		void DrawMoveInfo(MoveInfo moveInfo)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			DrawPositionInfo(moveInfo.StartPosition, "Start Position");
			DrawPositionInfo(moveInfo.EndPosition, "End Position");
			moveInfo.LobeHeight = EditorGUILayout.FloatField("Lobe Height", moveInfo.LobeHeight);
			EditorGUILayout.EndVertical();
		}

		void DrawPositionInfo(PositionInfo positionInfo, string label)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);

			positionInfo.Position = (PositionInfo.PositionType)EditorGUILayout.EnumPopup(label + " Type", positionInfo.Position);

			EditorGUI.indentLevel++;
			if (positionInfo.Position == PositionInfo.PositionType.CustomPosition)
			{
				positionInfo.WorldPosition = EditorGUILayout.Toggle("Is World Position", positionInfo.WorldPosition);
				positionInfo.CustomPosition = EditorGUILayout.Vector3Field((positionInfo.WorldPosition ? "World" : "Local") + " Position", positionInfo.CustomPosition);
			}
			else if (positionInfo.Position == PositionInfo.PositionType.Transform)
				positionInfo.TransformPosition = (Transform)EditorGUILayout.ObjectField("Transform", positionInfo.TransformPosition, typeof(Transform), true);
			EditorGUI.indentLevel--;
			EditorGUILayout.EndVertical();
		}
	}
}

