﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Outerminds.UIAnimations
{
	public class AnimationSpriteSequence : AnimationPulse
	{
		[HideInInspector]
		public List<SpriteListItem> Sprites = new List<SpriteListItem>();

		public override bool CanEase { get { return false; } }

		public float FrameInterval
		{
			get { return Duration / Mathf.Max(1f, (float)FrameCount); }
			set { if (value == 0) return; Duration = value * Mathf.Max(1f, (float)FrameCount); }
		}

		public int FrameCount
		{
			get { return Sprites.Count; }
		}

		protected override void UpdateAnimation()
		{
			int frameCount = FrameCount;
			if (frameCount == 0)
				return;

			int frameIndex = Mathf.FloorToInt(AnimationProgress * frameCount) % frameCount;
			if (AnimationProgress == 1f)
				frameIndex = frameCount - 1;

			SetFrame(frameIndex);
		}

		void SetFrame(int index)
		{
			if (FrameCount == 0)
				return;

			if (index > FrameCount)
				index = FrameCount - 1;
			else if (index < 0)
				index = 0;

			var targetType = (TextureTargetTypes)TargetType;
			if (targetType == TextureTargetTypes.Image)
				GetTarget<Image>().sprite = Sprites[index].Sprite;
			else if (targetType == TextureTargetTypes.MeshRenderer)
				GetTarget<MeshRenderer>().material.mainTexture = Sprites[index].Sprite.texture;
			else if (targetType == TextureTargetTypes.SpriteRenderer)
				GetTarget<SpriteRenderer>().sprite = Sprites[index].Sprite;
		}
	}

	[System.Serializable]
	public class SpriteListItem
	{
		public Sprite Sprite;

		public SpriteListItem(Sprite sprite)
		{
			Sprite = sprite;
		}
	}
}
