﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Set the same UIWindowGroup for UI screens among which only one can be shown a time (ie tabs)
/// </summary>
public class UIWindowGroup : MonoBehaviour
{
	bool _isWindowsInGroupSet;
	UIWindow[] _windowsInGroup;

	public UIWindow[] WindowsInGroup
	{
		get
		{
			if (!_isWindowsInGroupSet)
			{
				List<UIWindow> windows = new List<UIWindow>();
				windows.AddRange(GetComponentsInChildren<UIWindow>(true));
				for (int i = 0; i < windows.Count; i++)
				{
					if (windows[i].UIWindowGroup == this)
						continue;

					windows.RemoveAt(i);
					i--;
				}

				_windowsInGroup = windows.ToArray();
				_isWindowsInGroupSet = true;
			}

			return _windowsInGroup;
		}
	}

	public UIWindow CurrentOpenedWindow()
	{
		foreach (var window in WindowsInGroup)
			if (window.IsVisible)
				return window;
		return null;
	}

	public void CloseAllWindows()
	{
		foreach (var window in WindowsInGroup)
			window.Hide();
	}
}
