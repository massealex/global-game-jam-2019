﻿using UnityEngine;

public class UIWindow : MonoBehaviour
{
	[Header("UI Window")]

	[Tooltip("Set the same group for UI screens among which only one screen can be shown a time (ie tabs)")]
	public UIWindowGroup UIWindowGroup;

	[Tooltip("Animation to be played when the window becomes visible (in transition) and when it becomes not visible (out transition)")]
	public Outerminds.UIAnimations.AnimationUnit TransitionAnimation; 

	private bool _isVisible;
	private bool _inited;

	public bool IsAnimating { get { return TransitionAnimation != null && TransitionAnimation.IsPlaying(); } }

	protected GameObject _content;
	protected bool _contentIsSet;
	protected virtual GameObject Content
	{
		get { if (!_contentIsSet) { _contentIsSet = true; _content = transform.GetChild(0).gameObject; } return _content; }
	}

	protected virtual void LateUpdate()
	{
		bool active = Content.activeInHierarchy;
		Content.SetActive(_isVisible || IsAnimating);
		if (active && !Content.activeInHierarchy)
			OnHidden();
	}

	public bool IsVisible
	{
		get { return _isVisible; }
		set
		{
			if (value == IsVisible && _inited)
				return;

			_inited = true;
			_isVisible = value;

			if (value)
			{
				UIManager.Instance.RegisterActiveWindow(this);
				OnShow();
				Content.SetActive(true);

				if (TransitionAnimation != null)
					TransitionAnimation.Play(true);
			}
			else
			{
				UIManager.Instance.UnregisterActiveWindow(this);
				OnHide();

				if (TransitionAnimation != null)
					TransitionAnimation.Play(false);
			}
		}
	}

	public virtual void Show() { IsVisible = true; }
	public virtual void Hide() { IsVisible = false; }
	public virtual void Toggle() { IsVisible = !IsVisible; }
	public virtual void Back() { }
	protected virtual void OnShow() { }
	protected virtual void OnHide() { }
	protected virtual void OnHidden() { }
}
