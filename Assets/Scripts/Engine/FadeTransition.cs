﻿using Outerminds.UIAnimations;
using System;
using UnityEngine;

public class FadeTransition : MonoBehaviour
{
	public AnimationFade AnimationFade;

	bool _out = false;

	public Action Action { get; set; }

	void Start()
	{
		DontDestroyOnLoad(gameObject);
		AnimationFade.Play();
	}

	public void Init(Action callBackAction)
	{
		Action = callBackAction;
	}

	void Update()
	{
		if (!AnimationFade.IsPlaying() && !_out)
		{
			_out = true;
			AnimationFade.Play(false);

			if (Action != null)
				Action();
		}
		else if (!AnimationFade.IsPlaying() && _out)
		{
			Destroy(gameObject);
		}
	}
}
