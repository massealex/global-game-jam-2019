﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
	public AudioClip AudioButtonClick;

	Button _button;

	void Awake()
	{
		if (_button == null)
			_button = GetComponent<Button>();
	}

	void OnEnable()
	{
		if (_button != null)
			_button.onClick.AddListener(PlayButtonSound);
	}

	void OnDisable()
	{
		if (_button != null)
			_button.onClick.RemoveListener(PlayButtonSound);
	}

	public void PlayButtonSound()
	{
		SoundManager.Instance.PlaySound(AudioButtonClick);
	}
}
