﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
	[Header("Music Tracks")]
	public MusicManagerItem DayAmbiance;
	public MusicManagerItem NightAmbiance;
	public MusicManagerItem MenuMusic;
    public MusicManagerItem SneakyMusic;
    public MusicManagerItem BlimpRide;
    public MusicManagerItem SpaceAmbiance;
	public MusicManagerItem RaceMusic;

	MusicManagerItem[] _items;

	static MusicManager _instance;
	public static MusicManager Instance { get { if (_instance == null) _instance = FindObjectOfType<MusicManager>(); return _instance; } }

	void Awake ()
	{
		_items = GetComponentsInChildren<MusicManagerItem>(true);
	}

	public void StartMusic(MusicManagerItem musicItemToPlay)
	{
		foreach (var item in _items)
		{
			if (item == musicItemToPlay)
				item.Play();
			else
				item.Stop();
		}
	}

	public void StopAllMusic()
	{
		foreach (var item in _items)
			item.Stop();
	}
}
