﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManagerItem : MonoBehaviour {

	AudioSource _audioSource;
	float _maxVolume;
	bool _isPlaying;
	float _lastSettingsVolume = -1f;

	void Awake ()
	{
		_audioSource = GetComponent<AudioSource>();
		_maxVolume = _audioSource.volume;
		_audioSource.volume = 0;
	}

	public void Play()
	{
		_isPlaying = true;
	}

	public void Stop()
	{
		_isPlaying = false;
	}

	public bool IsPlaying
	{
		get { return _isPlaying; }
		set
		{
			_isPlaying = value;
		}
	}

	void Update ()
	{
		float settingsVolume = Settings.Music;
		float wantedVolume = _isPlaying ? _maxVolume * settingsVolume : 0;
		float lerpSpeed = settingsVolume == _lastSettingsVolume ? (_isPlaying ? 0.5f : 4f) : 1000f;

		_audioSource.volume = Mathf.MoveTowards(_audioSource.volume, wantedVolume, Time.unscaledDeltaTime * lerpSpeed);
		if (_audioSource.isPlaying && _audioSource.volume == 0)
			_audioSource.Stop();
		else if (!_audioSource.isPlaying && _audioSource.volume > 0)
			_audioSource.Play();

		_lastSettingsVolume = settingsVolume;
	}
}
