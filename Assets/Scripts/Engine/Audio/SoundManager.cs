﻿using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	static SoundManager _instance;
	public static SoundManager Instance { get { if (_instance == null) _instance = GameObject.FindObjectOfType<SoundManager>(); return _instance; } }

	Transform _cameraTransform;

	void Start()
	{
		_cameraTransform = Camera.main.transform;
	}

	public void PlaySound(string pResourceName, float volume = 1f)
	{
		AudioClip soundClip = Load(pResourceName);
		if (soundClip == null)
			Debug.LogError("SoundManager : Couldn't find audio resource (" + pResourceName + ")");
		else
			ExecuteSound(soundClip, false, Vector3.zero, volume);
	}

	public float PlayPitchAudio(string pResourceName, float volume = 1f)
	{
		AudioClip soundClip = Load(pResourceName);
		GameObject soundGameObject = PlayClipAt(soundClip, Vector3.zero, volume);
		soundGameObject.transform.parent = _cameraTransform;
		soundGameObject.transform.localPosition = Vector3.zero;
		soundGameObject.GetComponent<AudioSource>().pitch = UnityEngine.Random.Range(0.8f, 1.2f);
		soundGameObject.GetComponent<AudioSource>().volume = 0.5f;
		return 0;
	}

	private IEnumerator PlayLoadedAudioWhenReady(AudioClip pClip, float volume = 1f)
	{
		while (pClip.loadState == AudioDataLoadState.Loaded)
			yield return null;

		ExecuteSound(pClip, false, Vector3.zero, volume);
	}

	public void PlaySound(AudioClip pSoundClip, float volume = 1f)
	{
		ExecuteSound(pSoundClip, false, Vector3.zero, volume);
	}

	public void PlaySoundAt(string pResourceName, Vector3 pSoundPosition, float volume = 1)
	{
		AudioClip soundClip = Load(pResourceName);
		if (soundClip == null)
			Debug.LogError("SoundManager : Couldn't find audio resource (" + pResourceName + ")");
		else
			ExecuteSound(soundClip, true, pSoundPosition, volume);
	}

	public void PlaySoundAt(AudioClip pSoundClip, Vector3 pSoundPosition, float volume = 1f)
	{
		ExecuteSound(pSoundClip, true, pSoundPosition, volume);
	}

	private void ExecuteSound(AudioClip pSoundClip, bool pIs3DSound, Vector3 p3DSoundPosition, float volume)
	{
		if (Settings.Sound <= 0f)
			return;

		if (pIs3DSound)
			PlayClipAt(pSoundClip, p3DSoundPosition, volume);
		else
		{
			GameObject soundGameObject = PlayClipAt(pSoundClip, Vector3.zero, volume);
			soundGameObject.transform.parent = _cameraTransform;
			soundGameObject.transform.localPosition = Vector3.zero;
		}
	}

	// Instantiate a game obect with an AudioSource component playing the audio file. The game object is destroyed when audip has finshied playing.
	private GameObject PlayClipAt(AudioClip clip, Vector3 pos, float volume)
	{
		GameObject tempGO = new GameObject("TempAudio");
		tempGO.transform.position = pos;
		AudioSource aSource = tempGO.AddComponent<AudioSource>();
		aSource.clip = clip;
		aSource.volume = volume * Settings.Sound;

		aSource.minDistance = 10;

		aSource.Play();
		Destroy(tempGO, clip.length * 6f); // Destroy object once clip has finished playing
		LastOneShotAudioGameObject = tempGO;
		return tempGO;
	}

	private AudioClip Load(string content)
	{
		AudioClip obj = null;

		if (obj == null)
			obj = Resources.Load(content, typeof(AudioClip)) as AudioClip;

		if (obj == null && content != content.ToLower()) // Try again with lower case
			obj = Resources.Load(content.ToLower(), typeof(AudioClip)) as AudioClip;

		if (obj == null)
			Debug.LogWarning("Loading AudioClip failed : " + content);

		return obj;
	}

	public GameObject LastOneShotAudioGameObject
	{
		get; private set;
	}

	public AudioSource LastOneShotAudio
	{
		get { return LastOneShotAudioGameObject.GetComponent<AudioSource>(); }
	}
}