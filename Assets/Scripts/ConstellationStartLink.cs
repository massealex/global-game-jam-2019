﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstellationStartLink : MonoBehaviour
{
	public Transform Star1;
	public Transform Star2;
	public float LineTickness;

	void Awake()
	{
		transform.position = Vector3.Lerp(Star1.position, Star2.position, 0.5f);
		transform.localEulerAngles = new Vector3(0f, 0f, (Star2.position - Star1.position).ToDegreeAngle());
		transform.localEulerAngles = new Vector3(0f, 0f, (Star2.position - Star1.position).ToDegreeAngle());
		transform.localScale = new Vector3(Vector3.Distance(Star2.position, Star1.position), LineTickness, 1f);
	}
}
