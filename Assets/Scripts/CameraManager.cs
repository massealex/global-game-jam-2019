﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
	public CinemachineCameraActivation MainMenuCamera;
	public CinemachineCameraActivation IntroCamera;
	public CinemachineCameraActivation Level1Camera;
	public CinemachineCameraActivation EndingCamera;
	public CinemachineCameraActivation CinemachineCamRaceStart1;

	static CameraManager _instance;
	public static CameraManager Instance { get { if (_instance == null) _instance = FindObjectOfType<CameraManager>(); return _instance; } }
}
