﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorativeCloud : MonoBehaviour
{
    private Transform   _transform;
    private Vector3     _baseScale;
    private float       _scalerRatio;
    private float       _lastValue;
    private float       _scaleTimer;
    private float       _variation;
    private bool        _growing;

    // TestingCommit
    void Start()
    {
        _transform = transform;
        _baseScale = _transform.localScale;
        _scalerRatio = 1.0f;
        _lastValue = 1.0f;
        _variation = Random.Range(0.8f, 1.1f);
        _growing = Random.Range(0, 2) == 1;
    }

    void Update()
    {
        if(_scaleTimer < 2f)
        {
            _scaleTimer += Time.deltaTime * _variation;
            if(_growing)
                _scalerRatio = _lastValue  + 0.1f * _scaleTimer / 2.0f;
            else
                _scalerRatio = _lastValue - 0.1f * _scaleTimer / 2.0f;
            
            _transform.localScale = _baseScale * _scalerRatio;
        }
        else
        {
            _scaleTimer = 0.0f;
            _growing = !_growing;
            _lastValue = _scalerRatio;
        }
    }
}
